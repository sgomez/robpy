Camera Calibration Tutorial
===========================

In this short tutorial we show how to calibrate a set of cameras using a bright green 
led in a dark room. The idea of this procedure is to estimate a set of projection matrices 
that are required for stereo vision.

Capture Images and 3D Point Correspondences
-------------------------------------------

First we need a set of corresponding points in 2 and 3 dimensions. In our case, we assume we use
the Barrett WAM robot arm to gather 3D information. Therefore, we attach a bright green led
to the robot end effector as shown in the following image.

.. image:: img/robot_led.jpg
    :height: 300px
    :align: center

Then we need to collect images from all the cameras, and corresponding 3D positions with the
robot forward kinematics.

The procedure to collect images will off course depend on the particular setup being used.
Therefore, we do not provide code to collect images. Rather, we assume you can take pictures
in some standard format like JPG with your particular setup and store them in the hard drive
with a name formated as ``<camera_name>_<image_number>.<extension>``. The extension is by 
default ``jpg``, and the image number should be zero padded to have
exactly 8 digits. For example, lets suppose that we have only two cameras, named ``cam1``
and ``cam2``. And that we take 2 pictures per camera. The images should be named::

  cam1_00000001.jpg
  cam2_00000001.jpg
  cam1_00000002.jpg
  cam2_00000002.jpg

In our case, we used a script that takes one picture from every camera after the user
press a key. To collect the 3D data using SL on the robot, you can use an script like
the one located in ``examples/vision/cap3d_calib.py``. This example script produces a 
JSON file with the position of the end effector of the robot every time the user presses 
Enter. To finish type exit followed by enter.

In all the example scripts provided for this guide you can use the command ``--help`` to get 
some help of how to use the script. For example, using::

  python examples/vision/cap3d_calib.py --help


Should produce something like::

  usage: cap3d_calib.py [-h] [--host HOST] [--port PORT] [--out OUT]
                        [--start START]

  Record the state of the robot after receiving user input

  optional arguments:
    -h, --help     show this help message and exit
    --host HOST    Name of the host to connect to
    --port PORT    Port of the host to connect to
    --out OUT      File name where the data is stored
    --start START  First index of the recorded data

The recommended procedure to collect the data is:

1) Attach the super bright LED to the robot end effector
2) Run the program to collect camera images and the ``cap3d_calib.py`` script
3) Repeat about 30 times with the room dark:

  a) Place the robot in some position where the led is visible by at least 2 cameras
  b) Take pictures with cameras and collect the 3D position of the led at the same time.

4) Detach the led from the robot and collect more pictures where at least 3 cameras
   can see the led. Ideally, the led should be visible by as many cameras as possible.

For the rest of the tutorial, we will assume that the cameras are called:

1) cam111781
2) cam21127
3) cam21126
4) cam21119

And that the order matters. The set of image names should therefore look like this::

  cam111781_00000001.jpg cam111781_00000002.jpg ...
  cam21127_00000001.jpg cam21127_00000002.jpg ...
  cam21126_00000001.jpg cam21126_00000002.jpg ...
  cam21119_00000001.jpg cam21119_00000002.jpg ...  


The JSON file generated with the ``cap3d_calib.py`` script should look like this::

  [
    {
    "index": 1, 
    "pos": [0.029, -0.759, -0.855], 
    "joints": [-0.01, 0.21, -0.04, 1.63, -0.02, -0.31, 0.04]
    }, 
    {
    "index": 2, 
    "pos": [-0.467, -0.687, -0.846], 
    "joints": [0.35, 0.36, 0.34, 1.26, -0.05, 0.09, -0.33]
    },
    ...
  ]

Where the ``index`` field represents the image number, ``pos`` the 3D position of the 
led, and ``joints`` the joint state of the robot when the picture was taken. In the 
previous example, the images

* cam111781_00000001.jpg
* cam21127_00000001.jpg
* cam21126_00000001.jpg
* cam21119_00000001.jpg 

are all images of the led located in the position [0.029, -0.759, -0.855] in
cartesian coordinates taken by the different cameras.

**Recomputing Kinematics** 

The previous procedure produces both joint angles and the position of the
end effector when the used pressed the key. However, it is common that the
LED will not be located exactly at the center of the racket. For instance,
in our experiments the LED was located 4.5 cm away from the racket center
in the direction of the normal of the racket. We can fix this problem like
this::

   python examples/vision/recomp_kinematics.py --input <original_file> 
     --out <out_file> --offset 0 0.045 0


Finding the Position of the LED
-------------------------------

The next step is to find the position of the LED in all the images. For this
purpose you can use the script ``examples/vision/find_green_led.py``.

When you call the script, you should pass with ``--path`` the path to the image folder,
with ``--cam_prefixes`` a list with the camera names, and with ``--num_pics`` the number
of pictures per camera. For instance, if we took 150 images per camera in the previous step,
the script can be called as::

  python find_green_led.py --path /path/to/images --cam_prefixes cam111781 cam21127 cam21126 cam21119 --num_pics 150

As a result, the script should create a file named ``obs2d.json`` in the same folder
where the images where stored. The produced file should look like this::

  {"obs2d": [[null, null,...],
             [[100,200.5],null,...],
             [[300,333.3],[400,500],...],
             [[150,178.2],[234,543],...]
            ]
  }

The numbers in the previous example will change for you. The numbers basically represent the position of the
center of the LED in pixels and the ``null`` entries represent that the LED was not detected in that image. The
reason why in the previous example there are 4 list entries is because in this example we are assuming there
are 4 cameras.

Estimating the Camera Calibration Matrices
------------------------------------------

With everything in place, we can proceed to estimate the camera calibration matrices. To do this use
the script ``examples/vision/calibrate.py``. Please use ``--help`` to understand the arguments. When
you call this script you should provide at least the following arguments:

* ``--obs2d path/to/obs2d.json``: This file is the ``obs2d.json`` file produced by the 
  ``find_green_led.py`` script
* ``--obs3d path/to/obs3d.json``: This file is the one produced by the ``cap3d_calib.py`` script
  containing the 3D coordinates of some of the points
* ``--outfile path/to/outfile.txt``: In this file is where the calibration matrices are stored.

The output file will look like this::

  -567.809367102 -360.372229432 -104.226373333 -317.667653748 
  -15.5633652289 168.134962156 -600.202702151 457.219320536 
  0.221020632931 -0.425901023371 -0.117382563005 1.0 
  -643.040167286 72.9754261518 52.7754524205 961.178072072 
  -20.2898565655 194.268986886 -541.525095165 498.645529979 
  -0.151576532282 -0.37100980433 -0.141525495431 1.0 
  326.955021459 -20.0276321357 13.0550700753 272.122788837 
  5.1859041884 -121.51853107 -283.699691296 -202.735349865 
  0.0755878851827 0.194674108496 -0.0837612886946 1.0 
  227.933609092 157.559497712 -98.3880925606 347.053104629 
  -28.5016562618 -117.478288235 -262.750348224 -115.160136887 
  -0.109374188892 0.167496312818 -0.0697851761116 1.0 

Where the first 3 lines correspond to the first camera (In this example ``cam111781``),
the next 3 lines to the second camera and so on. This script should also produce some
metrics like the pixel reprojection error per camera. It should be less than 3 pixels.
