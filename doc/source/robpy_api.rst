*******************
RobPy API Reference
*******************

The RobPy package consists of a set of utilities for robotics in Python. Part of
the functionality is written in Python and other part in C++.

======================
Robot Vision Utilities
======================

The module `robpy.vision` contains a set computer vision utilities like camera 
calibration and stereo vision.

.. automodule:: robpy.vision
   :members:

