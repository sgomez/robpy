
import matplotlib.pyplot as plt
import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.layers.wrappers import TimeDistributed
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau
from keras.utils import np_utils

def sim_ball_traj(init_pos=np.zeros(3), init_vel=np.array([-1.3,4.5,2.2]), air_drag=np.array([0.5,0.5,0.1]), 
        quad_air_drag=0.0, bounce_fac=np.array([0.9,0.9,0.8]), model_noise=None, 
        obs_noise=None, deltaT=0.005, T=120, max_bounces=None):
    pos = init_pos
    vel = init_vel
    obs = []
    time = []
    noise_add = lambda x, noise: np.random.multivariate_normal(x, noise) if noise is not None else x
    is_bounce = []
    bounces_left = max_bounces if max_bounces is not None else 1000000
    for i in xrange(T):
        obs.append( noise_add(pos, obs_noise) )
        time.append(deltaT*i)
        a = -air_drag*vel - quad_air_drag*np.linalg.norm(vel)*vel + np.array([0,0,-9.8])
        pos = pos + vel*deltaT + 0.5*(deltaT**2)*a
        vel = vel + deltaT*a
        if pos[2] < 0.0 and bounces_left > 0:
            is_bounce.append(True)
            pos[2] *= -1.0
            vel[2] *= -1.0
            vel = vel*bounce_fac
            bounces_left -= 1
        else:
            is_bounce.append(False)
    return np.array(time), np.array(obs), is_bounce


def sim_data(N=100, T=120, 
        init_state = {
            'pos': {'mean': np.array([0.0,0.0,0.3]), 'cov': np.diag(np.array([0.5,1,0.01])**2)},
            'vel': {'mean': np.array([-1.4,4.5,2.3]), 'cov': np.eye(3)}
        }, deltaT=0.005, max_bounces=None, bounce_fac=np.array([0.9,0.9,0.8]),
        air_drag=np.array([0,0,0]), quad_air_drag=0.15):
    ans = []
    times = []
    is_bounce = []
    s = lambda x: np.random.multivariate_normal(x['mean'], x['cov'])
    for i in xrange(N):
        time, x, is_bounce_n = sim_ball_traj(init_pos=s(init_state['pos']), 
                init_vel=s(init_state['vel']), 
                air_drag=air_drag, 
                quad_air_drag=quad_air_drag,
                bounce_fac=bounce_fac, 
                model_noise=None, obs_noise=None,#0.02**2*np.eye(3), 
                deltaT=deltaT, T=T, max_bounces=max_bounces)
        ans.append(x)
        times.append(time)
        is_bounce.append(is_bounce_n)
    return np.array(times), np.array(ans), is_bounce

def predict_rtsp(model, obs, T):
    """ Predict the next T time steps of the time series
    """
    s = obs.shape
    x = np.concatenate((obs,np.zeros((s[0],T,s[2]))), axis = 1)
    for t in xrange(T):
        px = model.predict(x[:,0:s[1]+t,:])
        x[:,s[1]+t,:] = px[:,-1,:]
    return x

def predict_sim_data(obs, T, air_drag=np.array([0.5,0.5,0.1]), bounce_fac=np.array([0.9,0.9,0.8]), deltaT=0.016):
    """ Toy prediction function (To compare the classifier with)
    """
    s = obs.shape
    x = np.concatenate((obs,np.zeros((s[0],T,s[2]))), axis = 1)
    for t in xrange(T):
        vel = x[:,s[1]+t-1,:] - x[:,s[1]+t-2,:]
        pos = x[:,s[1]+t-1,:]
        a = -air_drag*vel + np.array([0,0,-9.8])
        pos = pos + vel*deltaT + 0.5*(deltaT**2)*a
        vel = vel + deltaT*a
        if pos[:,2] < 0.0:
            pos[:,2] *= -1.0
            vel[:,2] *= -1.0
            vel = vel*bounce_fac

        x[:,s[1]+t,:] = pos
    return x


def train_rtsp(time_series):
    """ Train a recursive time series predictor
    """
    X = time_series[:,0:-1,:]
    Y = time_series[:,1:,:]
    mask = np.ones((X.shape[0], X.shape[1]))
    mask[:,0:10] = 0

    model = Sequential()
    model.add(LSTM(256, return_sequences=True, input_dim = X.shape[2]))
    model.add(LSTM(256, return_sequences=True))
    model.add(LSTM(256, return_sequences=True))
    #model.add(Dropout(0.2))
    #model.add(Dense(32), activation='softmax')
    model.add(TimeDistributed(Dense(Y.shape[2])))
    model.compile(loss='mse', optimizer='adam', sample_weight_mode="temporal")
    #TODO: Remember to look later again the sample_weight mode to avoid checking errors with few
    #ball observations
    # define the checkpoint
    filepath="weights-improvement-{epoch:02d}-{loss:.4f}.hdf5"
    checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
    #reduce learning learning rate on plateau
    reduce_lr = ReduceLROnPlateau(monitor='loss', factor=0.8, patience=10, verbose=1, min_lr=1e-5)
    #callback list
    callbacks_list = [reduce_lr]#[checkpoint]
    # fit the model
    history = model.fit(X, Y, epochs=500, batch_size=8, callbacks=callbacks_list, sample_weight=mask)
    plt.plot(np.log10(history.history['loss']))
    plt.show()
    return model

import tensorflow as tf

def get_cnn_minibatch(time_series, batch_size=32, k=32, num_future_pred=32):
    inputs = []
    outputs = []
    for n in range(batch_size):
        i = np.random.randint(len(time_series))
        curr_ts = time_series[i]
        start_seed = np.random.randint(0,(len(curr_ts)/2)-k-num_future_pred)
        inputs.append( curr_ts[start_seed:start_seed+k] )
        out_indices = start_seed+k+np.random.permutation(len(curr_ts)-start_seed-k)[0:num_future_pred]
        outputs.append( curr_ts[out_indices] )
    return np.array(inputs), np.array(outputs)


def cnn_feature_extractor(inputs, filters, kernel_size, activation, strides, is_training, name):
    assert(len(filters)==len(strides))
    layer_input = inputs
    for layer_id, filter_size in enumerate(filters):
        conv_layer = tf.layers.conv1d(inputs=layer_input,
                filters=filter_size,
                kernel_size=kernel_size,
                activation=activation,
                strides=strides[layer_id],
                padding="same",
                name="{}/conv{}".format(name,layer_id))
        c1norm = tf.layers.batch_normalization(conv_layer, training=is_training)
        layer_input = c1norm
    return tf.contrib.layers.flatten(layer_input)


def train_cnn(time_series, k=32, num_iter=100, val_data=None):
    """ Train a CNN time series predictor
    """
    N = len(time_series)
    _,d = time_series[0].shape
    tf.reset_default_graph()
    input_state = tf.placeholder(tf.float32, shape=[None,k,d], name="input_state")
    pred_time = tf.placeholder(tf.float32, shape=[None,1], name="pred_time")
    outputs = tf.placeholder(tf.float32, shape=[None,d-1], name="outputs")
    is_training = tf.placeholder(tf.bool, name="is_training")
    encoded_state_norm = cnn_feature_extractor(inputs=input_state,
            filters=[8,12,16,20,32], 
            strides=[2,2,2,2,2], 
            kernel_size=2,
            activation=tf.nn.relu,
            is_training=is_training,
            name="feature_extractor")

    state_with_time = tf.concat([pred_time, encoded_state_norm], 1)
    fully_1 = tf.layers.dense(inputs=state_with_time,
            units=128,
            activation=tf.nn.tanh,
            name="fully_1")
    predictions = tf.layers.dense(inputs=fully_1,
            units=d-1,
            name="predictions")
    tf.add_to_collection("predictions", predictions)
    loss = tf.losses.mean_squared_error(labels=outputs,
            predictions=predictions)
    tf.add_to_collection('loss', loss)
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    with tf.control_dependencies(update_ops):
        train_step = tf.train.AdamOptimizer(1e-4).minimize(loss, name="train_step")

    nparams=np.sum([np.product([xi.value for xi in x.get_shape()]) for x in tf.all_variables()])
    print("Number of parameters: {}".format(nparams))

    saver = tf.train.Saver()
    model = {'input_state': input_state,'pred_time':pred_time,'is_training':is_training,'predictions/BiasAdd':predictions}
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        train_batch_size = 512
        train_num_future_pred = 1
        for opt_iter in range(num_iter):
            mb_in, mb_out_shaped = get_cnn_minibatch(time_series, batch_size=train_batch_size, k=k, num_future_pred=train_num_future_pred)
            mb_out = np.reshape(mb_out_shaped, [train_batch_size,-1]) # Only useful if there is one single prediction to make
            mb_in_norm = np.copy(mb_in)
            curr_times = mb_in[:,-1,0] #t0: For every training instance, the time of the last of the k_th obs
            mb_in_norm[:,:,0] = mb_in[:,:,0] - np.reshape(curr_times,[-1,1]) # Subtract t0 for input
            mb_out_norm = np.zeros(mb_out.shape)
            mb_out_norm[:,0] = mb_out[:,0] - curr_times # Subtract t0 also from output
            mb_out_norm[:,1:] = mb_out[:,1:] - mb_in[:,-1,1:] # And subtract the last position from output
            batch_pred_times = mb_out_norm[:,0]
            batch_outputs = mb_out_norm[:,1:]
            batch_pred_times_reshaped = np.reshape(batch_pred_times, [train_batch_size,train_num_future_pred])
            #if opt_iter%2000==0:
            #    print("Input: ", mb_in)
            #    print("batch_pred_times: ", batch_pred_times_reshaped)
            #    print("outputs: ", batch_outputs)
            feed_dict = {input_state: mb_in_norm, 
                    pred_time: batch_pred_times_reshaped, 
                    outputs: batch_outputs, 
                    is_training: False}
            if opt_iter%100 == 0:
                batch_loss = loss.eval(feed_dict=feed_dict)
                #batch_pred = predictions.eval(feed_dict=feed_dict)
                #batch_diff = batch_outputs - batch_pred
                #batch_loss = np.mean(batch_diff*batch_diff)
                print("iter={}, batch_loss={}, avg_batch_err={}".format(opt_iter,batch_loss,np.sqrt(batch_loss)))
                #print(batch_loss)
            if opt_iter%2000==0:
                my_loss = lambda x: np.mean(x*x, axis=1)
                err = cnn_predict_error(sess, model, time_series, k) 
                avg_loss = cnn_predict_error(sess, model, time_series, k, loss_fn=my_loss)
                batch_ts = np.concatenate((mb_in,mb_out_shaped),axis=1)
                batch_loss = cnn_predict_error(sess, model, batch_ts, k, loss_fn=my_loss)
                print("Total training error: {}, avg_loss: {}, batch_loss: {}".format(np.mean(err), np.mean(avg_loss),
                    batch_loss))
                if val_data is not None:
                    val_err = cnn_predict_error(sess, model, val_data, k)
                    print("Total validation error: {}".format(np.mean(val_err)))
                saver.save(sess, '/tmp/cnn_ball_model/cnn_ball_model', global_step=opt_iter)
            feed_dict[is_training] = True
            train_step.run(feed_dict=feed_dict)
        saver.save(sess, '/tmp/cnn_ball_model/cnn_ball_model')
    return model


def load_cnn(sess, path):
    model_tensors = ['input_state','pred_time','is_training','predictions/BiasAdd']
    ans = {}
    new_saver = tf.train.import_meta_graph('{}.meta'.format(path))
    new_saver.restore(sess, path)
    graph = tf.get_default_graph()
    for tn in model_tensors:
        var = graph.get_tensor_by_name('{}:0'.format(tn))
        ans[tn] = var
    return ans

def predict_cnn(sess, model, input_state, pred_times):
    mb_in = np.copy(input_state)
    curr_times = input_state[:,-1,0] #t0: For every training instance, the time of the last of the k_th obs
    mb_in[:,:,0] = mb_in[:,:,0] - np.reshape(curr_times,[-1,1]) # Subtract t0 for input
    pred_times_reshaped = np.reshape(pred_times - curr_times, [-1,1])
    curr_pred = model['predictions/BiasAdd'].eval(feed_dict={
        model['input_state']: mb_in, 
        model['pred_time']: pred_times_reshaped, 
        model['is_training']: False})
    curr_pred += input_state[:,-1,1:]
    return curr_pred

def cnn_predict_many(sess, model, input_state, pred_times):
    N,K = np.shape(pred_times)
    D = input_state.shape[2]
    predictions = np.zeros((N,K,D-1))
    for k in range(K):
        predictions[:,k] = predict_cnn(sess, model, input_state, pred_times[:,k])
    return predictions

def cnn_predict_error(sess, model, time_series, k=32, loss_fn=lambda x: np.linalg.norm(x,axis=1)):
    N,K,D = np.shape(time_series)
    input_state = time_series[:,0:k,:]
    ans = []
    for i in range(k,K):
        predictions = predict_cnn(sess, model, input_state, pred_times=time_series[:,i,0])
        diff = time_series[:,i,1:] - predictions
        loss = loss_fn(diff)
        avg_loss = np.mean(loss)
        ans.append(avg_loss)
    return ans
    
def create_seq_dataset(time_series, k=1):
    inputs = time_series[:,:-k,:]
    outputs = time_series[:,k:,:]
    out_diffs = outputs - inputs

