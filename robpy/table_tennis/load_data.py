""" Code to load table tennis data

This file contains code to load the captured table tennis data stored
in a set of files. It also segments the data in desired ways if the
meta data to do so is provided.
"""

import json
import bisect
import math
import numpy as np
import robpy.slpp as slpp
import robpy.time_series as rts
import logging

def rm_indices(v, indices):
    """ Remove multiple indices from a list

    Given a sorted non-repeated list of indices, this algorithm
    returns a new list removing all of those indices from the given vector.
    """
    nv = []
    j = 0
    for i,x in enumerate(v):
        if j<len(indices) and indices[j]==i:
            j += 1
        else:
            nv.append(x)
    return nv

def rm_repeated_obs(v):
    nv = [v[0]]
    for x in v:
        xo = np.array(x["obs"])
        xl = np.array(nv[-1]["obs"])
        if np.linalg.norm(xl-xo)>1e-6:
            nv.append(x)
    return nv

def ensure_dt(v, **params):
    X = []
    Xavg = []
    times = []
    hidden = []
    i = 0
    while i<len(v):
        j = i+1
        x = [np.array(v[i]["obs"])]
        while j<len(v) and math.fabs(v[i]["time"] - v[j]["time"])<1e-6: 
            x.append( np.array(v[j]["obs"]) )
            j+=1
        x_avg = sum(x) / (j-i)
        if len(times)!=0: 
            dt = v[i]["time"] - times[-1]
            assert(dt>0)
            if dt > params["maxDeltaT"]:
                n = int(math.ceil(dt / params["deltaT"]))
                ndt = dt / n
                for k in xrange(n-1):
                    times.append(times[-1] + ndt)
                    X.append(X[-1])
                    Xavg.append(Xavg[-1])
                    hidden.append(True)
            assert(times[-1] < v[i]["time"])
        times.append(v[i]["time"])
        hidden.append(False)
        X.append(x)
        Xavg.append(x_avg)
        i = j
    Xavg = np.array(Xavg)
    return (X, Xavg, np.array(times), hidden)        

def fw_ball_traj(data_and_meta):
    """ Builds a data set of forward ball trajectories

    This method builds a data set for forward ball trajectories with a similar
    time difference between samples, so that it is easy to use for the Kalman
    filter implementation. Several observations at the same time are averaged
    and missing observations are added as Zero vectors and the corresponding
    hidden flag is set.
    """
    X = []
    Xavg = []
    U = []
    Hidden = []
    Times = []
    state = 0
    for inst in data_and_meta:
        meta = inst['meta']
        if not meta['obs']: 
            continue
        data = inst['data']
        outliers = {}
        inliers = []
        for sensor in meta["obs"]:
            outliers[sensor["id"]] = sensor["outliers"]
        for sensor in data["obs"]:
            if not sensor["id"] in outliers: continue #ignore 2d data
            curr_inliers = rm_indices(sensor["values"], outliers[sensor["id"]])
            curr_inliers = rm_repeated_obs(curr_inliers)
            inliers.extend(curr_inliers)
        inliers = sorted(inliers, key=lambda x: x["time"])
        stimes = map(lambda x: x["time"], inliers)
        assert( all(stimes[i] <= stimes[i+1] for i in xrange(len(stimes)-1)) )

        for segm in meta["segments"]:
            stime = segm["start_time"]
            etime = segm["ball_fw_time"]
            a = bisect.bisect_left(stimes, stime) + 1
            b = bisect.bisect_right(stimes, etime)
            assert(a<b)
            assert( all(inliers[i]["time"] <= inliers[i+1]["time"] for i in xrange(a,b)) )
            x, xavg, times, hidden = ensure_dt(inliers[a:b], deltaT=0.016, maxDeltaT=0.02)
            X.append(x)
            Xavg.append(xavg)
            Hidden.append(hidden)
            assert( all(times[i] <= times[i+1] for i in xrange(len(times)-1)) )
            Times.append(times)
            U.append(np.ones((len(x),1)))
    return X, Xavg, U, Hidden, Times

def strike_trajectories(data_and_meta):
    """ Builds a data set of strike robot trajectories
    """
    data = []
    for instance in data_and_meta:
        if not "meta" in instance: continue
        meta = instance["meta"]
        if not meta["segments"]: continue
        for segm in meta["segments"]:
            if not "strike" in segm: continue
            strike = segm["strike"]
            a = strike["start_ix"]
            b = strike["end_ix"]
            elem = instance["data"]["joints"][a:b]
            data.append(elem)
    return data

class DatasetBuilder:
    """ Builds table tennis data sets in memory from loaded formatted data

    This class can be used to load data stored in JSON format from different
    files. To be able to use the segmentation abilities, the meta-data files
    should also be provided.
    """

    def __init__(self):
        self.data = []

    def load_json(self, file_name, **params):
        """ Loads JSON formatted file into memory

        You can pass the meta data file name as the optional parameter "meta".
        """
        f = open(file_name, "r")
        instance = {"data": json.load(f)}
        f.close()

        if "meta" in params:
            f = open(params["meta"], "r")
            meta = json.load(f)
            f.close()
            instance.update({"meta": meta})

        self.data.append(instance)

    def strike_trajectories(self, **params):
        """ Builds a data set of strike robot trajectories
        """
        return strike_trajectories(self.data)


    def fw_ball_traj(self):
        """ Builds a data set of forward ball trajectories

        This method builds a data set for forward ball trajectories with a similar
        time difference between samples, so that it is easy to use for the Kalman
        filter implementation. Several observations at the same time are averaged
        and missing observations are added as Zero vectors and the corresponding
        hidden flag is set.
        """
        return fw_ball_traj(self.data)

class PlayDatasetBuilder:
    """ Builds table tennis data sets in memory from loaded formatted data

    This class can be used to load data stored in JSON format from different
    files. To be able to use the segmentation abilities, the meta-data files
    should also be provided.
    """

    def __init__(self):
        self.data = []

    def load_json(self, file_name, **params):
        """ Loads JSON formatted file into memory

        You can pass the meta data file name as the optional parameter "meta".
        """
        f = open(file_name, "r")
        instance = {"data": json.load(f)}
        f.close()

        if "meta" in params:
            f = open(params["meta"], "r")
            meta = json.load(f)
            f.close()
            instance.update({"meta": meta})

        self.data.append(instance)

    def strike_trajectories(self, player=1, split_joint=0, zero_tol=0.01):
        """ Builds a data set of strike robot trajectories
        """
        strikes = []
        logging.debug('Extracting the strike trajectories')
        for inst in self.data:
            if not "meta" in inst: continue
            games = inst["meta"]
            if games is None:
                logging.warning("The meta-data for some particular data-set was null. Ignoring.")
                continue
            q,qd,t = slpp.joints_to_numpy(inst["data"]["joints"])
            for game in games:
                if not "trials" in game or game['trials'] is None: 
                    logging.warning("The meta-data for game {} does not contain any trials, ignoring this game".format(game['game_id']))
                    continue
                for trial in game["trials"]:
                    if not trial["valid"]: break
                    logging.debug('Valid trial on game {}, trial {}, player {}'.format(game['game_id'], trial['trial_id'], trial['player']))
                    if player != trial["player"]:
                        continue
                    should_use = trial["opponent_success"] and "success" in trial and trial["success"]
                    if not should_use:
                        logging.debug('Trial {} was not successful, ommiting')
                        continue
                    hit_time = trial["ball_fw_time"]
                    j_hit_ix = bisect.bisect_left(t, hit_time)
                    # Maybe consider in the future to run zcv in task space (y coordinate)
                    a,b = rts.zero_cross_vel(qd, split_joint, zero_tol, seed_ix=j_hit_ix)
                    logging.debug("Using successful striking movement of player {},"
                            " from index {} to {} and hitting time {}".format(trial["player"], a, b, hit_time))
                    strike = inst["data"]["joints"][a:b]
                    strikes.append(strike)
        logging.info("Finding a total of {} successful striking movements".format(len(strikes)))
        return strikes

    def fw_ball_traj(self, player=1):
        """ Builds a data set of forward ball trajectories

        This method builds a data set for forward ball trajectories with a similar
        time difference between samples, so that it is easy to use for the Kalman
        filter implementation. Several observations at the same time are averaged
        and missing observations are added as Zero vectors and the corresponding
        hidden flag is set.
        """
        X = []
        Times = []
        state = 0
        for inst in self.data:
            meta = inst['meta']
            data = inst['data']
            obs_times,obs_positions = slpp.obs_to_numpy(data["obs"])
            if meta is None:
                logging.warning("The meta-data for some particular data-set was null. Ignoring.")
                continue
            for game in meta:
                if not "trials" in game or game['trials'] is None: 
                    logging.warning("The meta-data for game {} does not contain any trials, ignoring this game".format(game['game_id']))
                    continue
                t = np.array( obs_times[1][game["game_start_ix"]:game["game_end_ix"]] )
                x = np.array( obs_positions[1][game["game_start_ix"]:game["game_end_ix"],:] )
                for trial in game["trials"]:
                    if not trial["valid"]: break
                    logging.debug('Valid trial on game {}, trial {}'.format(game['game_id'], trial['trial_id']))
                    if player != trial["player"]:
                        continue
                    should_use = trial["opponent_success"]
                    if not should_use: continue
                    incoming_ball_ix = trial["incoming_ball_ix"]
                    tt = t[incoming_ball_ix]
                    tx = x[incoming_ball_ix]
                    X.append(tx)
                    Times.append(tt)
        return Times, X


import matplotlib.pyplot as plt

'''
class CleanerDataNorm:

    def __init__(self, with_delta_t=True):
        self.__norm = preproc.RobustScaler()
        self.__with_delta_t = with_delta_t

    def fit(X):
        if self.__with_delta_t:
'''

def load_cleaner_dataset(f_name):
    ans = {}
    with open(f_name,'r') as f:
        data = np.load(f)
        for k in data: ans[k] = data[k]
    return ans

def split_time_cleaner_dataset(data, split_time=0.1, min_obs_traj=100):
    instances = []
    split_ix = [ix for ix in range(1,len(data['ball'])) if (data['ball'][ix][3] - data['ball'][ix-1][3]) > split_time]
    split_ix.insert(0,0)
    split_ix.append(len(data['ball']))
    keys = ['ball', 'ball_cut', 'ball_bounce', 'ball_direction', 'ball_reliable']
    for i in range(1,len(split_ix)):
        split_range = (split_ix[i-1], split_ix[i])
        if (split_ix[i] - split_ix[i-1]) < min_obs_traj: continue
        instance = {}
        for k in keys:
            instance[k] = data[k][split_range[0]:split_range[1]]
        instances.append(instance)
    return instances

def plot_time_cleaner_instance(instance):
    ball_marker = ['bo', 'go', 'yo', 'rx']
    ball = instance['ball']
    ball_dir = instance['ball_direction']
    fig, axis = plt.subplots(3, 1, squeeze=False)
    X = [[] for i in range(4)]
    for i,x in enumerate(ball):
        X[ball_dir[i]].append(x)
        if 'ball_reliable' in instance and instance['ball_reliable'][i] == 0:
            X[3].append(x)
        for d in range(3):
            if 'ball_cut' in instance and instance['ball_cut'][i] == 1:
                axis[d][0].axvline(x=x[3],color='black')
            if 'ball_bounce' in instance and instance['ball_bounce'][i] == 1:
                axis[d][0].axvline(x=x[3],color='red')
    for i in range(4):
        if len(X[i]) == 0: continue
        x = np.array(X[i])
        for d in range(3):
            axis[d][0].plot(x[:,3],x[:,d],ball_marker[i])
    plt.show()


