

import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import scipy.linalg
import robpy.kalman as kalman
import robpy.full_promp as promp
import robpy.utils as utils
import json

class Ball_A_Mat:

    def __init__(self, airDrag, bounceVec, deltaT, bounceZ):
        self.deltaT = deltaT
        self.bounceZ = bounceZ
        tableWidth = 1.526
        tableLenght = 2.74
        self.Xrange = [-0.5*tableWidth,0.5*tableWidth]
        self.Yrange = [-3.5,-3.5+tableLenght]
        self.__params = np.array(airDrag + bounceVec)
        self.optional = {}

    def flyMat(self, state, params=None, **opt_par):
        params = params if params is not None else self.params
        deltaT = self.deltaT if not 'deltaT' in opt_par else opt_par['deltaT']
        beta = params[0:3]
        Adim = []
        for d in xrange(3):
            Adim.append( np.array([[1, deltaT - 0.5*beta[d]*deltaT**2],[0,1-beta[d]*deltaT]]) )
        Afly = scipy.linalg.block_diag(Adim[0],Adim[1],Adim[2])
        return Afly

    def bounceMat(self, state, params=None, **opt_par):
        params = params if params is not None else self.params
        deltaT = self.deltaT if not 'deltaT' in opt_par else opt_par['deltaT']
        beta = params[0:3]
        bounceFac = params[3:6]
        Abounce = np.array([[1,deltaT - 0.5*beta[0]*deltaT**2,0,0,0,0],[0,bounceFac[0],0,0,0,0], \
            [0,0,1,deltaT - 0.5*beta[1]*deltaT**2,0,0], [0,0,0,bounceFac[1],0,0], \
            #[0,0,0,0,1,0], [0,0,0,0,0,-bounceFac[2]]])
            [0,0,0,0,-bounceFac[2],-bounceFac[2]*deltaT],[0,0,0,0,0,-bounceFac[2]]])
        return Abounce

    def isBounce(self, state):
        if 'isBounce' in self.optional and state['n']<len(self.optional['isBounce']) and state['t']<len(self.optional['isBounce'][state['n']]):
            return self.optional['isBounce'][state['n']][state['t']]
        #print("Dangerous code being executed")
        Afly = self.flyMat(state)
        Bfly = state['B'].flyMat(state)
        next_state = np.dot(Afly, state['prev_z']) + np.dot(Bfly, state['action'])
        next_z = next_state[4]
        if next_z <= self.bounceZ:
            next_x = next_state[0]
            next_y = next_state[2]
            return True #next_x>=self.Xrange[0] and next_x<=self.Xrange[1] and next_y>=self.Yrange[0] and next_y<=self.Yrange[1]
        return False

    def mat(self, state, params=None, **opt_par):
        if self.isBounce(state):
            return self.bounceMat(state, params, **opt_par)
        return self.flyMat(state,params,**opt_par)

    def n_params(self):
        return len(self.__params)

    def shape(self):
        return 6,6

    @property
    def params(self):
        return self.__params

    @params.setter
    def params(self, params):
        self.__params = params

    def deriv(self, state, param_id, params=None, **opt_par):
        params = params if params is not None else self.params
        deltaT = self.deltaT if not 'deltaT' in opt_par else opt_par['deltaT']
        beta = params[0:3]
        bounceFac = params[3:6]
        ans = np.zeros((6,6))
        if self.isBounce(state):
            if param_id==0: #beta[0]
                ans[0,1] = -0.5*deltaT**2
            elif param_id==1: #beta[1]
                ans[2,3] = -0.5*deltaT**2
            elif param_id==2: #beta[2]
                pass #derivative is zero
            elif param_id==3: #bounceFac[0]
                ans[1,1] = 1
            elif param_id==4: #bounceFac[1]
                ans[3,3] = 1
            elif param_id==5: #bounceFac[2]
                ans[4,4] = -1
                ans[4,5] = -deltaT
                #ans[5,5] = -1
            else:
                raise NotImplementedError
        else:
            if param_id >= 0 and param_id < 3:
                ans[2*param_id,2*param_id + 1] = -0.5*deltaT**2
                ans[2*param_id+1,2*param_id+1] = -deltaT
        return ans

class Ball_B_Mat:
    def __init__(self, deltaT, bounceZ, bounceFac):
        self.deltaT = deltaT
        self.gravety = -9.8
        self.bounceZ = bounceZ
        self.bounceFac = bounceFac
        self.params = 0 #don't optimize anything here for the moment

    def flyMat(self, state, params=None, **opt_par):
        deltaT = self.deltaT if not 'deltaT' in opt_par else opt_par['deltaT']
        Bfly = np.array([[0],[0],[0],[0],[0.5*deltaT**2*self.gravety],[deltaT*self.gravety]])
        return Bfly

    def bounceMat(self, state, params=None, **opt_par):
        bounceZ = self.bounceZ
        bfac = self.bounceFac #state['A'].params[5]
        Bbounce = np.array([[0],[0],[0],[0],[bounceZ*(1+bfac)],[0]])
        #Bbounce = np.array([[0],[0],[0],[0],[0],[0]])
        return Bbounce

    def mat(self, state, params=None, **opt_par):
        if (state['A'].isBounce(state)):
            return self.bounceMat(state, params, **opt_par)
        return self.flyMat(state, params, **opt_par)

    def n_params(self):
        return len(self.__params)

    def shape(self):
        return 6,1

    @property
    def params(self):
        return self.__params

    @params.setter
    def params(self, params):
        self.__params = params

    def deriv(self, state, param_id, params, **opt_par):
        raise NotImplementedError


def save_ball_model(file_name, deltaT, kf, A, **params):
    f = file(file_name, "w")
    ball_model = {'mu_0': kf.mu0.tolist(), 'fly_obs_noise': kf.Sigma.tolist(),
            'bounce_obs_noise': kf.Sigma.tolist(), 'fly_trans_noise': kf.Gamma.tolist(),
            'bounce_trans_noise': kf.Gamma.tolist(), 'gravety': -9.8,
            'airDrag': A.params[0:3].tolist(), 'bounceFac': A.params[3:6].tolist(),
            'deltaT': deltaT, 'model': 'LGBM1', 'P0': kf.P0.tolist()}
    #Note: Fixing the following values is not too nice because they are in the matrix A as well
    table = {"width": 1.526, "lenght": 2.74, "center": [0.0, -2.13, A.bounceZ], "net_height": 0.1525}
    data = {"table": table, "ball_model": ball_model}
    json.dump(data, f)
    f.close()

def load_ball_model(file_name, **params):
    params.setdefault('a_mat', 'linear')
    f = file(file_name, 'r')
    data = json.load(f)
    f.close()
    model = data['ball_model']
    deltaT = model['deltaT']
    bounceZ = data["table"]["center"][2]
    if params['a_mat'] == 'linear':
        beta = model['airDrag']
        bounceFac = model['bounceFac']
        A = Ball_A_Mat(beta,bounceFac,deltaT,bounceZ)
    else: raise NotImplementedError
    B = Ball_B_Mat(deltaT, bounceZ, bounceFac[2])
    C = kalman.ConstMat(np.array([[1,0,0,0,0,0],[0,0,1,0,0,0],[0,0,0,0,1,0]]))
    kf = kalman.LinearKF(A, C, B=B)
    kf.mu0 = model['mu_0']
    kf.P0 = model['P0']
    kf.Sigma = model['fly_obs_noise']
    kf.Gamma = model['fly_trans_noise']
    return kf, deltaT


def load_ball_data(file_path, real_set):
    #dropSet = set([17,20,22,23,24,29,56]) #some datasamples are no good
    #real_set = [i for i in range(15,66) if not i in dropSet]
    data = []
    for i in real_set:
        mat = np.loadtxt(file_path + "unifyData" + str(i) + ".txt")
        vision = mat[:,0:11]
        cam1 = []
        cam2 = []
        time1 = []
        time2 = []
        for t in xrange(np.shape(vision)[0]):
            time = vision[t][-1]
            if vision[t][1]==1:
                pos = vision[t][2:5]
                if len(cam1)==0 or not np.allclose(cam1[-1],pos):
                    cam1.append(pos)
                    time1.append(time)
            if vision[t][6]==1:
                pos = vision[t][7:10]
                if len(cam2)==0 or not np.allclose(cam2[-1],pos):
                    cam2.append(pos)
                    time2.append(time)
        data.append({'file_num': i, 'cam1': np.array(cam1), 'time1': np.array(time1), \
            'cam2': np.array(cam2), 'time2': np.array(time2)})
    return data

def segment_ball_trajectories(data, split_pts):
    split_data = []
    segm_data = {}
    for i in xrange(np.shape(split_pts)[0]):
        segm_data[split_pts[i][0]] = split_pts[i][1:]
    for instance in data:
        segm_inst = segm_data[instance['file_num']]
        pre_bounce_pos = []
        pre_bounce_time = []
        post_bounce_pos = []
        post_bounce_time = []
        cam1_ix = 0
        cam2_ix = 0
        while cam1_ix < np.shape(instance['cam1'])[0] and cam2_ix < np.shape(instance['cam2'])[0]:
            if instance['time1'][cam1_ix] == instance['time2'][cam2_ix]:
                ctime = instance['time1'][cam1_ix]
                cpos = (instance['cam1'][cam1_ix,:] + instance['cam2'][cam2_ix,:]) / 2.0
                cam1_ix += 1
                cam2_ix += 1
            elif instance['time1'][cam1_ix] < instance['time2'][cam2_ix]:
                ctime = instance['time1'][cam1_ix]
                cpos = instance['cam1'][cam1_ix,:]
                cam1_ix += 1
            else:
                ctime = instance['time2'][cam2_ix]
                cpos = instance['cam2'][cam2_ix,:]
                cam2_ix += 1
            if ctime > segm_inst[0] and ctime < segm_inst[1]:
                pre_bounce_pos.append(cpos)
                pre_bounce_time.append(ctime)
            elif ctime >= segm_inst[1] and ctime < segm_inst[2]:
                post_bounce_pos.append(cpos)
                post_bounce_time.append(ctime)
        while cam1_ix < np.shape(instance['cam1'])[0]:
            ctime = instance['time1'][cam1_ix]
            cpos = instance['cam1'][cam1_ix,:]
            cam1_ix += 1
            if ctime > segm_inst[0] and ctime < segm_inst[1]:
                pre_bounce_pos.append(cpos)
                pre_bounce_time.append(ctime)
            elif ctime >= segm_inst[1] and ctime < segm_inst[2]:
                post_bounce_pos.append(cpos)
                post_bounce_time.append(ctime)
        while cam2_ix < np.shape(instance['cam2'])[0]:
            ctime = instance['time2'][cam2_ix]
            cpos = instance['cam2'][cam2_ix,:]
            cam2_ix += 1
            if ctime > segm_inst[0] and ctime < segm_inst[1]:
                pre_bounce_pos.append(cpos)
                pre_bounce_time.append(ctime)
            elif ctime >= segm_inst[1] and ctime < segm_inst[2]:
                post_bounce_pos.append(cpos)
                post_bounce_time.append(ctime)
        split_data.append({"pre_bounce_time": np.array(pre_bounce_time),\
            "pre_bounce_pos": np.array(pre_bounce_pos),\
            "post_bounce_time": np.array(post_bounce_time),\
            "post_bounce_pos": np.array(post_bounce_pos), "file_num": instance['file_num']})
    return split_data

def estimate_deltaT(times):
    dsum = 0.0
    ssum = 0.0
    nval = 0
    for time in times:
        for i in xrange(len(time)-1):
            val = time[i+1] - time[i]
            dsum += val
            ssum += val**2
            nval += 1
    deltaT = dsum / nval
    #print "deltaT=",deltaT," dev=",np.sqrt((ssum/nval) - deltaT**2)
    return deltaT

def plot_ball_trajectory(instance):
    plt.figure(1)
    axis = ['X','Y','Z']
    for dim in xrange(3):
        plt.subplot(3,1,dim+1)
        plt.plot(instance['time1'],instance['cam1'][:,dim],'rx')
        plt.plot(instance['time2'],instance['cam2'][:,dim],'bx')
        plt.ylabel(axis[dim])
    plt.xlabel('Time')
    plt.show()

def plot_segm_ball_trajectory(instance):
    plt.figure(1)
    axis = ['X','Y','Z']
    for dim in xrange(3):
        plt.subplot(3,1,dim+1)
        plt.plot(instance['pre_bounce_time'],instance['pre_bounce_pos'][:,dim],'rx')
        plt.plot(instance['post_bounce_time'],instance['post_bounce_pos'][:,dim],'bx')
        plt.ylabel(axis[dim])
    plt.xlabel('Time')
    plt.show()

def extend_ball_traj(times, positions, duration, use_nobs=36, deltaT=1.0/180.0,
        airDrag=0.3, gravety=np.array([0,0,-9.8])):
    basis = {
            'conf': [{"type": "poly", "nparams": 0, "conf": {"order": 2}}],
            'params': []
            }
    basis_fun = lambda t,params: promp.comb_basis(t, params, conf=basis["conf"])
    bf_lambdas = promp.get_bfun_lambdas(basis['params'], basis_fun, True, True)
    poly_promp = promp.FullProMP(basis=basis, num_joints=3,
            model={'mu_w': np.zeros(9), 'Sigma_w': np.eye(9)*100, 'Sigma_y':np.eye(3)*1e-5})
    if len(times) < use_nobs: 
        raise Exception("Given a trajectory of {} obs, but minumum is {} obs".format(len(times), use_nobs))
    #1) Find initial position and velocity
    time_cut = times[-use_nobs]
    poly_time = [x - time_cut for x in times[-use_nobs:]]
    poly_time.append(1.0 + poly_time[0]) #Do not time scale
    poly_obs = positions[-use_nobs:]
    #print poly_time
    #print poly_obs
    e_step = poly_promp.E_step([poly_time], [poly_obs])
    #print e_step
    w = np.random.multivariate_normal(e_step['w_means'][0], e_step['w_covs'][0])
    phi_t = promp.comp_Phi_t(poly_time[-2], 1.0, num_joints=3, **bf_lambdas)
    init_state = np.dot(phi_t, w)
    #print init_state
    x0 = init_state[0:3]
    v0 = init_state[3:6]
    #print("Using w={} to extend the ball trajectory".format(w))
    #print("Initial position={} and velocity={}".format(x0,v0))

    #2) Extend to the desired duration
    curr_time = times[-1]
    curr_pos = x0
    curr_vel = v0
    while (curr_time - times[0]) < duration:
        acc = -airDrag*curr_vel + gravety
        curr_pos = curr_pos + deltaT*curr_vel + 0.5*deltaT**2*acc
        curr_vel = curr_vel + deltaT*acc
        curr_time += deltaT
        times.append(curr_time)
        positions.append(curr_pos)

def build_ball_promp_training_set(fw_ball_traj, duration, min_z=-1e100, augment=False, min_duration=0.8):
    #X, Xavg, U, Hidden, Times = fw_ball_traj
    Times, X = fw_ball_traj
    times = []
    x = []
    for n,t in enumerate(Times):
        for start_ix in range(0,len(t)/2,5):
            d = t[-1] - t[start_ix]
            if d < min_duration: continue
            curr_time = []
            curr_pos = []
            for i in range(start_ix, len(t)):
                c_t = t[i]
                if c_t - t[start_ix] > duration: break
                if X[n][i][2] > min_z:
                    curr_time.append(c_t - t[start_ix])
                    curr_pos.append(X[n][i])
            extend_ball_traj(curr_time, curr_pos, duration)
            curr_time[-1] = duration
            times.append(np.array(curr_time))
            x.append(np.array(curr_pos))
            if not augment: break
    return times, x

def train_ball_promp(fw_ball_traj, duration, min_z=-1e100, min_duration=0.8, 
        full_basis = {
            'conf': [
                    {"type": "sqexp", "nparams": 3, "conf": {"dim": 2}},
                    {"type": "sqexp", "nparams": 5, "conf": {"dim": 4}},
                    {"type": "poly", "nparams": 0, "conf": {"order": 1}}
                ],
            'params': [np.log(0.4),0.25,0.75, np.log(0.2), 0.1, 0.4, 0.55, 0.9]
            }):
    dim_basis_fun = promp.dim_comb_basis(**full_basis)
    inv_whis_mean = lambda v, Sigma: utils.make_block_diag(Sigma, 3) + 1e-4*np.eye(dim_basis_fun*3)
    args = {
            #'prior_mu_w': {"m0": np.zeros(3*dim_basis_fun), "k0": 1},
            'prior_Sigma_w': {'v':dim_basis_fun*3, 'mean_cov_mle': inv_whis_mean},
            'opt_basis_pars': False,
            'print_lowerbound': True,
            'max_iter': 30
            }
    model = promp.FullProMP(basis=full_basis)
    times, pos = build_ball_promp_training_set(fw_ball_traj, duration, min_z=min_z, augment=False, min_duration=min_duration)
    model.train(times=times, q=pos, **args)
    #print "Mean Weights:\n", model.mu_w
    #print "Stdev Weights:\n", np.sqrt(np.diag(model.Sigma_w))
    #print "Noise stdev:\n", np.sqrt(np.diag(model.Sigma_y))
    #print "Basis function params: ", model.get_basis_pars()
    return model
