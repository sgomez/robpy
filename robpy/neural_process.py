
import tensorflow as tf
import numpy as np

class RandomBatchSampler:

    def __init__(self, batch_size):
        self.bs = batch_size

    def __call__(self, inputs, outputs, i):
        N,T,D = inputs.shape
        start_ix = (self.bs * i) % N
        end_ix = min(N, start_ix + self.bs)
        full_in = inputs[start_ix:end_ix,:,:]
        full_out = outputs[start_ix:end_ix,:,:]
        css = np.random.randint(1,T)
        cs_ix = np.random.permutation(T)[0:css]
        cs_in = full_in[:,cs_ix,:]
        cs_out = full_out[:,cs_ix,:]
        cond_set = np.concatenate((cs_in,cs_out), axis=2)
        return full_in, full_out, cond_set

def feature_mlp(inputs, is_training, hidden_units, hidden_activation, batch_norm=False):
    """ Multiple hidden layers with the same activation function

    Useful to represent all the layers but the last in generic neural networks. All the
    layers are using the same activation function passed as a parameter.
    """
    prev_layer = inputs
    for n, n_units in enumerate(hidden_units):
        if isinstance(hidden_activation, (list)):
            layer = tf.layers.dense(prev_layer, units=n_units, activation=hidden_activation[n])
        else:
            layer = tf.layers.dense(prev_layer, units=n_units, activation=hidden_activation)
        prev_layer = layer
        if batch_norm:
            bn = tf.layers.batch_normalization(prev_layer, training = is_training)
            prev_layer = bn
    return prev_layer

def reg_mlp(inputs, is_training, hidden_units, out_dims, hidden_activation=tf.nn.relu, batch_norm=False):
    """ Generic regression function for neural networks
    """
    prev_layer = feature_mlp(inputs, is_training, hidden_units, hidden_activation, batch_norm)
    out_layer = tf.layers.dense(prev_layer, units=out_dims)
    return out_layer

def create_mlp_regressor(hidden_units, out_dims, hidden_activation=tf.nn.relu, batch_norm=False):
    return lambda x, is_training: reg_mlp(x, is_training, hidden_units, out_dims, hidden_activation, batch_norm)

class MLPReg:

    def __init__(self, hidden_units, in_dims, out_dims, hidden_activation=tf.nn.relu, batch_norm=True, pred_std=False):
        self.input = tf.placeholder(tf.float32, shape=[None,in_dims])
        self.output = tf.placeholder(tf.float32, shape=[None, out_dims])
        self.is_training = tf.placeholder(tf.bool, name="is_training")
        self.learning_rate = tf.placeholder(tf.float32, name="learning_rate")
        self.should_pred_std = pred_std
        if not pred_std:
            # Normal regression (Not Heteroscedastic)
            self.predictions = reg_mlp(self.input, self.is_training, hidden_units, out_dims, hidden_activation, batch_norm)
            self.loss = tf.losses.mean_squared_error(labels=self.output, predictions=self.predictions)
        else:
            # Predict Gaussian uncertainty (Heteroscedastic model)
            func_output = reg_mlp(self.input, self.is_training, hidden_units, 2*out_dims, hidden_activation, batch_norm)
            self.predictions = func_output[:,0:out_dims] # Predicted mean
            self.pred_std = tf.exp( func_output[:,out_dims:] ) # Predicted standard deviation
            self.pred_dist = tf.distributions.Normal(loc=self.predictions, scale=self.pred_std)
            llhood = self.pred_dist.log_prob(self.output, name="log_likelihood")
            self.loss = tf.reduce_mean(-llhood)
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            self.train_step = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss, name="train_step")

    def predict(self, sess, inputs):
        if self.should_pred_std:
            ans = sess.run([self.predictions, self.pred_std], feed_dict={self.input: inputs, self.is_training: False})
            return ans[0], ans[1]
        else:
            return sess.run(self.predictions, feed_dict={self.input: inputs, self.is_training: False})

    def eval_loss(self, sess, inputs, outputs):
        return sess.run(self.loss, feed_dict={self.input: inputs, self.output: outputs, self.is_training: False})

    def fit(self, sess, inputs, outputs, batch_size=32, epochs=200, lr=1e-3, decay=0.5, decay_epochs=25):
        epoch_iters = 1 + (len(inputs)-(batch_size/2))/batch_size
        losses = []
        learning_rate = lr
        for i in range(epochs):
            bs_loss = []
            if ((i+1) % decay_epochs) == 0: learning_rate *= decay
            for j in range(epoch_iters):
                start_ix = j*batch_size
                end_ix = min(len(inputs), start_ix+batch_size)
                feed_dict={self.input: inputs[start_ix:end_ix,:], 
                        self.output: outputs[start_ix:end_ix,:],
                        self.is_training: True,
                        self.learning_rate: learning_rate}
                bs_loss.append(self.eval_loss(sess, feed_dict[self.input], feed_dict[self.output]))
                sess.run(self.train_step, feed_dict)
            losses.append( np.mean(bs_loss) )
        return {'loss': np.array(losses)}


class CondNeuralProcess:
    """ Conditional Neural Process Implementation

    Functions conditioned on a given conditioning set with known input - output pairs. We assume that
    the conditioning set is given as a concatenation of the input-output pairs (x_i,y_i).

    * cond_set: Set of conditioning (x,y) pairs. Shape [batch_size, cond_set_size, in_dim + out_dim]
    * pred_set: Set of inputs x for which we want predictions. Shape [batch_size, pred_set_size, in_dim]
    * encoder: A function that takes the conditioning set and returns a set of encodes r_i for
      each pair (x,y). The shape of the output must be [batch_size, cond_set_size, encode_dim] 
    * set_encodes: Returns the aggregation of all encodes of a set [batch_size, 1, encode_dim]
    * predictions: Predictions of the model. Shape [batch_size, pred_set_size, out_dims]
    * outputs: Placeholder where the actual outputs can be stored for training
    """

    @staticmethod
    def build_full_encoder(cond_set, encoder, config, is_training):
        all_encodes = encoder(cond_set, is_training)
        set_encode = tf.reduce_mean(all_encodes, axis=1, keepdims=True)
        return set_encode

    @staticmethod
    def build_predictor(set_encodes, inputs, decoder, is_training):
        rep_encodes = tf.tile(set_encodes,[1,tf.shape(inputs)[1],1])
        ext_inputs = tf.concat([rep_encodes, inputs], axis=2)
        pred = decoder(ext_inputs, is_training)
        return pred

    def predict(self, sess, cond_set, inputs):
        pred = sess.run(self.predictions, feed_dict = {self.cond_set: cond_set, 
            self.pred_set: inputs, self.is_training: False})
        return pred

    def eval_loss(self, sess, cond_set, inputs, outputs):
        loss = sess.run(self.loss, feed_dict = {self.cond_set: cond_set, self.pred_set: inputs, 
            self.outputs: outputs, self.is_training: False})
        return loss
    
    def __init__(self, config, encoder, decoder):
        self.is_training = tf.placeholder(tf.bool, name="is_training")
        self.cond_set = tf.placeholder(tf.float32, shape=(None, None, config['in_dim'] + config['out_dim']))
        self.pred_set = tf.placeholder(tf.float32, shape=(None, None, config['in_dim']))
        self.outputs = tf.placeholder(tf.float32, shape=(None, None, config['out_dim']))
        self.learning_rate = tf.placeholder(tf.float32, name="learning_rate")
        self.set_encodes = self.build_full_encoder(self.cond_set, encoder, config, self.is_training)
        self.predictions = self.build_predictor(self.set_encodes, self.pred_set, decoder, self.is_training)
        self.loss = tf.losses.mean_squared_error(labels=self.outputs, predictions=self.predictions)
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            self.train_step = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss, name="train_step")

    def fit(self, sess, inputs, outputs, batch_sampler, iterations, lr=1e-3, decay=0.5, decay_epochs=25):
        error = []
        learning_rate = lr
        for i in range(iterations):
            if ((i+1) % decay_epochs) == 0: 
                learning_rate *= decay
                print(error[1])
            in_set, out_set, cond_set = batch_sampler(inputs, outputs, i)
            feed_dict = {self.cond_set: cond_set, self.pred_set: in_set, self.outputs: out_set,
                    self.is_training: True, self.learning_rate: learning_rate}
            error.append(sess.run(self.loss, feed_dict))
            sess.run(self.train_step, feed_dict)
        return {'loss': np.array(error)}
