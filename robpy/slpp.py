
import numpy as np
import zmq
import json
import time
import datetime
import os
import subprocess
import threading

class SLPP:
    """ The SL++ network client object.

    This object is used to communicate in JSON with the SL++ server application. It uses ZMQ
    as communication protocol and you can specify the URL of the server as an optional parameter
    to the constructor. By default objects of this class open a network socket to the localhost in
    the port 7648.

    """
    _context = None
    _sock = None

    def __init__(self, **params):
        params.setdefault("host", "localhost")
        params.setdefault("port", "7648")
        params.setdefault("url", "tcp://{0}:{1}".format(params["host"], params["port"]))
        self._context = zmq.Context()
        self._sock = self._context.socket(zmq.REQ)
        self._sock.connect(params["url"])
        self.__sock_lock = threading.Lock()

    def query(self, method, body):
        with self.__sock_lock:
            q = json.dumps({'method': method, 'body': body})
            self._sock.send(q)
            res = json.loads( self._sock.recv() )
        if (res != None and 'error' in res):
            msg = "Error on remote method: {0}\nBody:\n{1}\nError: {2}".format(
                    method, body, res['error'])
            raise Exception(msg)
        return res

    def get_time(self):
        body = {}
        method = 'time/get_time'
        res = self.query(method, body)
        return res['time']

    def go_to(self, t0, T, qdes, qddes):
        body = {'t0': t0, 'T': T, 'qdes': qdes, 'qddes': qddes}
        method = 'mov_prim/add_go_to'
        res = self.query(method, body)
        return res['id']

    def get_joint_obs(self):
        method = 'joints/flush'
        body = {}
        return self.query(method, body)

    def clear_sensor_obs(self, **params):
        method = 'obs/clear'
        return self.query(method, params)

    def flush_sensor_obs(self, **params):
        method = 'obs/flush'
        return self.query(method, params)

    def clear_joint_obs(self, **params):
        method = 'joints/clear'
        return self.query(method, params)

def joints_to_numpy(joints):
    q = []
    qd = []
    t = []
    for x in joints:
        q.append(x['q'])
        qd.append(x['qd'])
        t.append(x['t'])
    if len(t) == 0:
        return None, None, None
    else:
        return np.array(q), np.array(qd), np.array(t)

def obs_to_numpy(obs):
    t = {}
    x = {}
    if obs != None:
        for sensor in obs:
            t_i = []
            x_i = []
            for elem in sensor['values']:
                t_i.append(elem['time'])
                x_i.append(elem['obs'])
            t[sensor['id']] = t_i
            x[sensor['id']] = np.array(x_i)
    return t, x

def load_json(f_name):
    """Loads data from a JSON file and returns it as a Python object
    """
    f = file(f_name, 'r')
    data = json.load(f)
    f.close()
    return data

def save_json(f_name, data):
    """Saves a Python object in JSON format
    """
    f = file(f_name, "w")
    json.dump(data, f)
    f.close()


class Recorder:
    """ Records sensor and joint information from SL++ tasks
    This class records the sensor and joint state information from the
    SL++ net tasks bringing only new information every time the method
    update() is called.
    """

    def __init__(self, client, **params):
        params.setdefault('sensor_ids',[0,1,10,11,12,13])
        params.setdefault('no_joints', False)
        params.setdefault('joints_after_obs', False)
        params.setdefault('get_tt_logs', None)
        self.__client = client
        self.__path = params["path"]
        self.__params = params
        self.sensor_callbacks = []
        self.joint_callbacks = []
        self.log_callbacks = []
        self.clear()

    @property
    def is_recording(self):
        return self.__recording

    def clear_internal_state(self):
        self.__joints = []
        self.__logs = []
        self.__obs = {}
        for sid in self.__params['sensor_ids']:
            self.__obs[sid] = []
        self.__last_obs_time = None

    def clear(self):
        self.__client.clear_sensor_obs()
        self.__client.clear_joint_obs()
        self.clear_internal_state()
        self.__recording = False

    def start_recording(self):
        if not self.is_recording:
            self.clear()
            self.__recording = True

    def time_since_last_obs(self):
        if not self.__last_obs_time:
            return 0.0
        else:
            return time.time() - self.__last_obs_time

    def update(self):
        if self.is_recording:
            obs = self.__client.flush_sensor_obs(ids=self.__params['sensor_ids'])
            for sensor in obs:
                if not 'values' in sensor: continue
                prev_obs = self.__obs[sensor['id']]
                new_obs = sensor['values']
                if len(prev_obs)>0 and len(new_obs)>0 and \
                        np.allclose(prev_obs[-1]['obs'], new_obs[0]['obs']):
                    new_obs.pop(0)
                if len(new_obs)>0:
                    do_append = True
                    for obs_cb in self.sensor_callbacks:
                        do_append = do_append and obs_cb(self, sensor['id'], new_obs)
                    if do_append:
                        prev_obs.extend(new_obs)
                        self.__last_obs_time = time.time()

            if self.__params['no_joints'] or \
                    (self.__params['joints_after_obs'] and not self.__last_obs_time):
                joints = None
            else:
                joints = self.__client.get_joint_obs()
            if joints is not None: 
                do_append = True
                for joint_cb in self.joint_callbacks:
                    do_append = do_append and joint_cb(self, joints)
                self.__joints.extend(joints)
            
            if self.__params['get_tt_logs'] is not None:
                log = self.__client.query(self.__params['get_tt_logs'],{})
                if log:
                    print "Getting a log"
                    do_append = True
                    for log_cb in self.log_callbacks:
                        do_append = do_append and log_cb(self, log)
                    if do_append: self.__logs.append(log)

    def to_stream(self, **params):
        obs = [{'id': key, 'values': value} for key, value in self.__obs.items()]
        data = {'joints': self.__joints, 'obs': obs}
        if self.__params['get_tt_logs']: data['logs'] = self.__logs
        return data

    def save(self, **additional_data):
        t_stamp = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
        fname = os.path.join(self.__path, "rec_{0}.json".format(t_stamp))
        data = self.to_stream()
        data.update(additional_data)
        save_json(fname, data)

    def stop_recording(self, **params):
        params.setdefault("do_save", True)
        if self.is_recording:
            self.update()
            if params["do_save"]: self.save()
            self.__recording = False
            self.clear_internal_state()

    def segment_recording(self):
        if self.is_recording:
            self.update()
            self.save()
            self.clear_internal_state()
        else:
            self.start_recording()


class TrajCleaner:
    """ Class to do trajectory cleaning and classification
    """

    def __init__(self, port=7640, process="trial_clean_srv",
            buff_size=65536, segm_type="play"):
        srv_uri = "tcp://{0}:{1}".format("*",port)
        cmd = [process, "--bs", str(buff_size), "--uri", srv_uri, "--type", segm_type]
        self.srv_process = subprocess.Popen(cmd)

        cli_uri = "tcp://{0}:{1}".format("localhost",port)
        ctx = zmq.Context()
        self.socket = ctx.socket(zmq.REQ)
        self.socket.connect(cli_uri)

    def query(self, method, body):
        q = json.dumps({'method': method, 'body': body})
        self.socket.send(q)
        res = json.loads( self.socket.recv() )
        if (res != None and 'error' in res):
            msg = "Error on remote method: {0}\nBody:\n{1}\nError: {2}".format(
                    method, body, res['error'])
            raise Exception(msg)
        return res

    def process_trial(self, trial):
        ans = self.query("process_trial", trial)
        return ans

    def set_config(self, config):
        ans = self.query("config", config)
        return ans

    def close(self):
        self.query("close",None)
        print "Waiting for child process to die..."
        self.srv_process.wait()

