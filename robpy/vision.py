
""" Computer Vision Utilities for Robotics

This module contains vision utilities for robotics. This utilities
include:

* Camera calibration
* Stereo vision


"""

import lib_robotics
import numpy as np
import itertools
import pystan
import os
import cPickle as pickle
import cv2

def project_points(pts3d, proj_mat):
    """Projects a set of 3D points in the image plane with the given projection matrix.

    """
    pts2d = []
    for pt3d in pts3d:
        x = pt3d if isinstance(pt3d,list) else pt3d.tolist()
        p2dh = np.dot(proj_mat, np.array(x + [1]))
        pts2d.append(p2dh[0:2] / p2dh[2])
    return pts2d


def lin_calib_cam(pts2d, pts3d):
    """Computes the camera calibration matrix with a simple linear method

    Given a set of corresponding 2D and 3D points estimates the projection
    matrix P that maps the 3D points in homogeneous coordinates to the
    2D points also in homogeneous coordinates. It simply uses least squares.

    """
    return lib_robotics.calibrate_camera(pts2d, pts3d)

def reproj_error(pts2d, pts3d, proj_mats):
    """Computes the reprojection error with the given projection matrices

    This method computes the re-projection of the 3D points in the image plane
    for every camera along with the errors with respect to the ground truth.
    For every camera this method returns a dictionary. The key 'proj' contains
    the re-projection of the 3D points in the 2D plane in pixels. The key 'indices'
    contains the indices for which the ground truth is known. The key 'errors' contains
    the distance in pixels between ground truth and the re-projected points.
    Finally, the key 'avg_error' contain the average re-projection error in pixels.
    """
    ans = []
    lin_multicam_fill_3d(pts2d, pts3d, proj_mats)
    for ix, pmat in enumerate(proj_mats):
        proj = project_points(pts3d, pmat)
        not_none = np.nonzero(map(lambda x: x is not None, pts2d[ix]))
        exist_pts2d = [x for x in pts2d[ix] if x is not None]
        err_vec = np.array(exist_pts2d) - np.array(proj)[not_none]
        errors = np.linalg.norm(err_vec, axis=1)
        ans.append( {'proj': proj, 'errors': errors, 'indices': not_none, 'avg_error': np.average(errors)} )
    return ans

def stereo_vision(proj_mat, obs2d):
    """Computes a 3D point from 2d observations
    The projection matrices are the camera calibration matrices. This method takes
    2D observations obtained from cameras and produce a 3D estimate using stereo vision.
    """
    assert(len(proj_mat)>=2 and len(proj_mat)==len(obs2d))
    _proj_mat = lib_robotics.list_mat()
    _obs2d = []
    for ix,mat in enumerate(proj_mat):
        if obs2d[ix] is not None and mat is not None:
            _proj_mat.push_back(np.asfortranarray(mat))
            _obs2d.append(obs2d[ix])
    return lib_robotics.stereo_vision(_proj_mat, _obs2d)

def multi_stereo_vision(obs2d, proj_mat, indices):
    """Computes a set of 3D points from 2D projections

    * proj_mat: A list of projection matrices whose length is equal to the number
    of cameras
    * obs2d: A two dimensional list of 2D points. The first dimension should be
    equal to the number of cameras, and the second dimension correspond to the
    different observations of the 3D points projected on each camera. If a camera
    can not see a 3D point pass None instead.
    * indices: The indices for which the 3D estimation is desired.
    """
    ans = []
    for ix in indices:
        pts2d = []
        for camobs in obs2d:
            pts2d.append(camobs[ix])
        ans.append(stereo_vision(proj_mat, pts2d))
    return ans


def __split_multicam_data(obs2d, obs3d):
    """Returns separate 2D and 3D corresponding points with the available data

    """
    n_cams = len(obs2d)
    N = len(obs3d)
    pts2d = [[] for i in xrange(n_cams)]
    pts3d = [[] for i in xrange(n_cams)]
    for i in xrange(N):
        if obs3d[i] is not None:
            for j in xrange(n_cams):
                if obs2d[j][i] is not None:
                    pts3d[j].append(obs3d[i])
                    pts2d[j].append(obs2d[j][i])
    return pts2d, pts3d

def __lin_multicam_proj_mat(pts2d, pts3d, **params):
    proj_mats = []
    n_cams = len(pts2d)
    for i in xrange(n_cams):
        if len(pts3d[i]) > params['min_pts_calib']:
            proj_mats.append(lin_calib_cam(pts2d[i], pts3d[i]))
        else:
            proj_mats.append(None)
    return proj_mats

def lin_multicam_fill_3d(obs2d, obs3d, proj_mats):
    n_obs = len(obs3d)
    n_cams = len(obs2d)
    for i in xrange(n_obs):
        if obs3d[i] is None:
            pmat = []
            o2d = []
            for j in xrange(n_cams):
                if obs2d[j][i] is not None and proj_mats[j] is not None:
                    pmat.append(proj_mats[j])
                    o2d.append(obs2d[j][i])
            if len(pmat)>=2:
                o3d = stereo_vision(pmat, o2d)
                obs3d[i] = list(o3d)

def good_calib_data(obs2d, obs3d, **params):
    params.setdefault('min_cam_obs', 3)
    params.setdefault('min_cam_obs_3d', 2)
    keep = []
    n_cams = len(obs2d)
    good_ix = []
    for ix in xrange(len(obs3d)):
        n_cam_obs = 0
        for cam_id in xrange(n_cams):
            if obs2d[cam_id][ix] is not None: n_cam_obs += 1
        min_cam_obs = params['min_cam_obs_3d'] if obs3d[ix] is not None else params['min_cam_obs']
        if n_cam_obs >= min_cam_obs:
            good_ix.append(ix)
    _obs3d = [obs3d[i] for i in good_ix]
    _obs2d = [[cam_obs[i] for i in good_ix] for cam_obs in obs2d]
    return {'good_ix': good_ix, 'obs2d': _obs2d, 'obs3d': _obs3d}

def lin_multicam_calib(obs2d, obs3d, **params):
    params.setdefault('min_pts_calib', 12) #Minumum number of points to consider calibration accurate
    params.setdefault('mod_inplace', False) #Allow modifications to the received parameters?
    n_cams = len(obs2d)
    _obs2d = obs2d
    _obs3d = list(obs3d) if not params['mod_inplace'] else obs3d
    n_nones = n_cams
    proj_mats = [None for i in xrange(n_cams)]
    while True:
        pts2d, pts3d = __split_multicam_data(_obs2d, _obs3d)
        proj_mats = __lin_multicam_proj_mat(pts2d, pts3d, min_pts_calib=params['min_pts_calib'])
        new_n_nones = sum(map(lambda x: x is None, proj_mats))
        if new_n_nones >= n_nones: break
        n_nones = new_n_nones
        #Predict missing 3D with available projection matrices
        lin_multicam_fill_3d(_obs2d, _obs3d, proj_mats)
    return proj_mats

def bayesian_multicam_cam_calib(pts2d, pts3d, **params):
    params.setdefault('prior_sigma_iw', 1.0*np.eye(2))
    params.setdefault('prior_sigma_dof', 3)
    params.setdefault('prior_error_3d_iw', 1e-4*np.eye(3))
    params.setdefault('prior_error_3d_dof', 20)
    params.setdefault('stan_model', os.path.join(os.path.dirname(__file__), 'config/multicam_calib.stan'))
    params.setdefault('stan_compiled', 'stan_multicam_calib.out')
    params.setdefault('mcmc_iters', 1000)
    params.setdefault('mcmc_chains', 4)
    params.setdefault('print_fit', False)
    full_3d = list(pts3d)
    n_cams = len(pts2d)
    init_proj_mat = lin_multicam_calib(pts2d, full_3d, mod_inplace=True, **params)
    _pts3d = []
    obs3d = []
    is_obs3d = []
    init_hid_3d = []
    obs2d = []
    is_obs2d = []
    for ix, x in enumerate(full_3d):
        if x is not None:
            assert(isinstance(x, list))
            if pts3d[ix] is not None:
                obs3d.append(x)
                is_obs3d.append(1)
            else:
                obs3d.append([0,0,0])
                is_obs3d.append(0)
            init_hid_3d.append(x)
            obs2d.append([])
            is_obs2d.append([])
            for cam_id in xrange(n_cams):
                if pts2d[cam_id][ix] is not None:
                    obs2d[-1].append(pts2d[cam_id][ix])
                    is_obs2d[-1].append(1)
                else:
                    obs2d[-1].append([0,0])
                    is_obs2d[-1].append(0)
    #obs3d_homo = map(lambda x: x + [1], obs3d)
    proj_ix = list(itertools.product(range(1,4), range(1,5)))[0:11]
    data = {'N': len(obs3d), 'n_cams': n_cams, 'is_obs_3d': is_obs3d, 'obs3d': obs3d,
        'obs2d': obs2d, 'is_obs_2d': is_obs2d, 'proj_ix': proj_ix,
        'prior_sigma_iw': params['prior_sigma_iw'], 'prior_sigma_dof': params['prior_sigma_dof'],
        'prior_error_3d_dof': params['prior_error_3d_dof'], 'prior_error_3d_iw': params['prior_error_3d_iw']}
    init_proj = map(lambda x: x.flatten()[0:11], init_proj_mat)
    init_pars = lambda: {"sigma_2d": params['prior_sigma_iw'], "proj_params": init_proj,
        "hid3d": init_hid_3d, "sigma_3d": params['prior_error_3d_iw']}
    if os.path.isfile(params['stan_compiled']):
        sm = pickle.load(open(params['stan_compiled'],'rb'))
    else:
        sm = pystan.StanModel(file=params['stan_model'])
        pickle.dump(sm, open(params['stan_compiled'],'wb'))
    fit = sm.sampling(data=data, iter=params['mcmc_iters'], chains=params['mcmc_chains'], init=init_pars)
    print fit
    params = fit.extract(permuted=True)
    p_param_samples = params['proj_params']
    sigma_2d_samples = params['sigma_2d']
    sigma_3d_samples = params['sigma_3d']
    pts3d_samples = params['hid3d']
    p_mat_samples = [[] for i in xrange(n_cams)]
    for sample in p_param_samples:
        for ix, x in enumerate(sample):
            x = np.array(x.tolist() + [1.0])
            P = np.reshape(x, (3,4))
            p_mat_samples[ix].append(P.tolist())
    return {'proj_mat_samples': p_mat_samples, 'proj_params': p_param_samples.tolist(),
        'sigma_2d': sigma_2d_samples.tolist(), 'sigma_3d': sigma_3d_samples.tolist(),
        'pts3d_samples': pts3d_samples.tolist()}


def avg_images(image_names, **params):
    im_sum = None
    im_sum_sq = None
    for i_name in image_names:
        if not os.path.isfile(i_name):
            raise Exception('File not found: ' + i_name)
        im = cv2.imread(i_name)
        if 'channel' in params:
            im = im[:,:,params['channel']]
        im = im.astype('float')
        if im_sum is None:
            im_sum = np.zeros(im.shape)
            im_sum_sq = np.zeros(im.shape)
        im_sum += im
        im_sum_sq += im**2
    mean = im_sum / len(image_names)
    var = (im_sum_sq / len(image_names)) - mean**2
    std = np.sqrt(var)
    return mean, std

def find_led_blobs(image, led_thresh, **params):
    params.setdefault('min_inertia', 0.3)
    params = cv2.SimpleBlobDetector_Params()
    # Change thresholds
    params.minThreshold = led_thresh*0.5;
    params.maxThreshold = led_thresh;
    params.filterByArea = True
    params.minArea = 1
    params.maxArea = 15
    # Filter by Inertia
    #params.filterByInertia = True
    #params.minInertiaRatio = params['min_inertia']
    # Create a detector with the parameters
    ver = (cv2.__version__).split('.')
    if int(ver[0]) < 3 :
        detector = cv2.SimpleBlobDetector(params)
    else :
        detector = cv2.SimpleBlobDetector_create(params)
    return detector.detect(image)

def find_green_leds(image_names, **params):
    from matplotlib import pyplot as plt
    params.setdefault('channel', 1) #Green channel
    params.setdefault('rel_thresh', 4.0/5.0)
    params.setdefault('min_std_diff', 5.0)
    params.setdefault('max_led_size', 15) #in pixels
    params.setdefault('plot_summary', False)
    params.setdefault('debug', False)
    img_mean, img_std = avg_images(image_names, **params)
    if params['plot_summary']:
        plt.imshow(img_mean)
        plt.title('Image mean')
        plt.show()
        plt.imshow(img_std)
        plt.title('Image standard deviation')
        plt.show()
    ans = []
    for i_name in image_names:
        if not os.path.isfile(i_name):
            raise Exception('File not found: ' + i_name)
        im = cv2.imread(i_name)
        if params['debug']:
            plt.imshow(im)
            plt.show()
        if 'channel' in params:
            im = im[:,:,params['channel']]
        diff = im - img_mean
        max_pix = np.unravel_index(diff.argmax(), diff.shape)
        if params['debug']:
            print max_pix
            plt.imshow(diff)
            plt.title('Difference with mean image')
            plt.show()
        if diff[max_pix] < params['min_std_diff']*img_std[max_pix]:
            ans.append(None)
            continue
        t_val = np.max(diff) * params['rel_thresh']
        _, mask = cv2.threshold(im, t_val, 255, cv2.THRESH_BINARY)
        if params['debug']:
            plt.imshow(mask)
            plt.title('Thresholded image')
            plt.show()
        _, contours, _ = cv2.findContours( mask, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        if params['debug']:
            print contours
        if len(contours) != 1:
            ans.append(None)
            continue
        led = contours[0]
        if len(led) > params['max_led_size']:
            ans.append(None)
            continue
        #For the moment ignore the eccentricity constraint
        #I will also ignore the bicubic interpolation for subpixel accuracies
        x = np.mean(led, axis=0)
        ans.append(x[0].tolist())
    return ans
