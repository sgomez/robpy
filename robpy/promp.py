""" Probabilistic Movement Primitive module

This module contains a modified version of the old code for probabilistic
movement primitives.
"""

import numpy as np
from scipy.linalg import block_diag
import scipy.optimize as opt

def joint_train_data(tt_data):
    """ Extracts the joint training data for ProMPs

    This method takes a table tennis training set builder object and
    returns the joint angles W and velocities Wdot required by the
    ProMP class.
    """
    data = tt_data.strike_trajectories()
    time = []
    W = []
    Wdot = []
    for instance in data:
        time_n = []
        Wn = []
        Wn_dot = []
        for elem in instance:
            time_n.append(elem["t"])
            Wn.append(elem["q"])
            Wn_dot.append(elem["qd"])
        time.append(time_n)
        W.append(np.array(Wn))
        Wdot.append(np.array(Wn_dot))
    return (time, W, Wdot)

def poly_deriv(poly):
    """ Computes the derivative of a Polynomial
    """
    ans = []
    for i in xrange(1,len(poly)):
        ans.append(poly[i]*i)
    return ans

def make_block_diag(A, num_blocks):
    n,m = A.shape
    assert(n == m and (n % num_blocks)==0)
    block_len = n // num_blocks
    for i in xrange(n):
        for j in xrange(n):
            if (i // block_len) != (j // block_len):
                A[i,j] = 0.0


#One kind of kernel for proMP. All kernel classes should support __call__ and
#time_deriv for evaluation of the kernel and its time derivatives respectively
class proMP_Poly_Basis:
    """ Polynomial basis function for the ProMP class. 
    """
    #Attributes:
    #degree: The degree of the polynomial

    def __init__(self,**params):
        params.setdefault('degree', 3)
        self.degree = params['degree']

    def __call__(self, time):
        """ Returns the basis function vector evaluated on the desired time
        """
        basis_f = map(lambda ix: time**ix, range(self.degree+1))
        return np.array(basis_f)

    def time_deriv(self, time, order=1):
        """ Returns the derivative of the basis function vector at the desired time
        """
        phi = np.zeros(self.degree+1)
        coefs = np.ones(self.degree+1)
        basis = time**np.array(range(self.degree+1))
        for i in xrange(order):
            coefs = poly_deriv(coefs)
        phi[order:self.degree+1] = coefs*basis[0:self.degree+1-order]
        return phi

    def dim(self):
        return self.degree + 1

    def to_stream(self):
        params = {'degree': self.degree}
        ans = {'type': 'poly', 'params': params}
        return ans

    @classmethod
    def build_from_stream(cls, stream):
        obj = cls(**stream)
        return obj

#One kind of kernel for proMP. All kernel classes should support __call__ and
#time_deriv for evaluation of the kernel and its time derivatives respectively
class proMP_Gauss_Basis:
    """ Gaussian basis function for the ProMP class
    """
    #Attributes:
    #centers: The centers (means) of each gaussian kernel
    #sigma_kernel: A single spread (deviation) for all kernels

    def __init__(self,**params):
        params.setdefault('num_basis', 5)
        params.setdefault('centers',np.linspace(0,1,params['num_basis']))
        params.setdefault('sigma',0.25)
        self.centers = np.array(params['centers'])
        self.sigma_kernel = params['sigma']

    def __call__(self, time):
        """ Returns the basis function vector evaluated on the desired time
        """
        sigma_sq = self.sigma_kernel**2
        ans = np.exp(-0.5*(time - self.centers)**2 / sigma_sq)
        return ans

    def time_deriv(self, time, order=1):
        """ Returns the derivative of the basis function vector at the desired time
        """
        sigma_sq = self.sigma_kernel**2
        val = np.exp(-0.5*(time - self.centers)**2 / sigma_sq)
        if order==1:
            return val*((self.centers-time)/(sigma_sq))
        elif order==2:
            return val*(((time-self.centers)/sigma_sq)**2 - 1/sigma_sq)
        else: raise NotImplementedError("Derivatives only implemented up to second order")

    def dim(self):
        return len(self.centers)

    def to_stream(self):
        params = {'centers': self.centers.tolist(), 'sigma': self.sigma_kernel}
        ans = {'type': 'sqexp', 'params': params}
        return ans

    @classmethod
    def build_from_stream(cls, stream):
        obj = cls(**stream)
        return obj


class proMP_Combined_Basis:
    
    def __init__(self, param_list, **params):
        self.basis_list = map(lambda x: KernelBuilder.build_from_stream(x), param_list)
        self.__dim = sum(map(lambda x: x.dim(), self.basis_list))

    def __call__(self, time):
        ans = []
        for b in self.basis_list:
            ans.extend( b(time) )
        return np.array(ans)

    def time_deriv(self, time, order=1):
        ans = []
        for b in self.basis_list:
            ans.extend( b.time_deriv(time, order) )
        return np.array(ans)

    def dim(self):
        return self.__dim

    def to_stream(self):
        params = {'basis_list': map(lambda x: x.to_stream(), self.basis_list)}
        return {'type': 'combined',  'params': params}

    @classmethod
    def build_from_stream(cls, params_list):
        obj = cls(param_list)
        return obj

class KernelBuilder:
    kernel_stream_builder = {'sqexp': proMP_Gauss_Basis.build_from_stream, \
      'poly': proMP_Poly_Basis.build_from_stream, \
      'combined': proMP_Combined_Basis.build_from_stream}

    kernel_builder = {'sqexp': proMP_Gauss_Basis, \
      'poly': proMP_Poly_Basis,
      'combined': proMP_Combined_Basis}

    @classmethod
    def get_builder(cls, key):
        return cls.kernel_builder[key]

    @classmethod
    def get_stream_builder(cls, key):
        return cls.kernel_stream_builder[key]

    @classmethod
    def build_from_stream(cls, stream):
        builder = cls.get_stream_builder(stream["type"])
        return builder(stream["params"])

class proMP:
    """ Basic Probabilistic Movement Primitive as in Alex paper
    Represent a probabilistic movement primitive, each movement y_n is generated as
    y_n(t) = phi(t)*w, where phi(t) are the basis functions and w are some
    parameters generated from a Gaussian prior with mean_w mean and Sigma_w covariace.
    Sigma_y is the noise matrix for the observed value y.
    """
    #Attributes:
    #mean_w: Prior mean for w values
    #Sigma_w: Prior variance for w values
    #Sigma_y: Likelihood variance

    def __init__(self, **params):
        if 'Sigma_y' in params: self.Sigma_y = params['Sigma_y']
        if 'mu_w' in params: self.mean_w = params['mu_w']
        if 'Sigma_w' in params: self.Sigma_w = params['Sigma_w']

    #If passed optional parameter Sigma_q then do conditioning on distribution
    def condition(self, phi_t, mu_q, **params):
        params.setdefault('ignore_Sigma_y', True)
        d,k = np.shape(phi_t)
        if params['ignore_Sigma_y']:
            tmp1 = np.dot(self.Sigma_w, phi_t.T)
            tmp2 = np.dot(phi_t, np.dot(self.Sigma_w, phi_t.T))
            tmp2 = np.linalg.inv(tmp2)
            tmp3 = np.dot(tmp1,tmp2)
            mu_w = self.mean_w + np.dot(tmp3, (mu_q - np.dot(phi_t, self.mean_w)))
            tmp4 = np.eye(d)
            if 'Sigma_q' in params:
                tmp4 -= np.dot(params['Sigma_q'], tmp2)
            Sigma_w = self.Sigma_w - np.dot(tmp3, np.dot(tmp4, tmp1.T))
        else:
            inv_Sig_w = np.linalg.inv(self.Sigma_w)
            inv_Sig_y = np.linalg.inv(self.Sigma_y)
            Sw = np.linalg.inv(inv_Sig_w + np.dot(phi_t.T,np.dot(inv_Sig_y, phi_t)))
            A = np.dot(np.dot(Sw, phi_t.T), inv_Sig_y)
            b = np.dot(Sw, np.dot(inv_Sig_w, self.mean_w))
            mu_w = np.dot(A, mu_q) + b
            if 'Sigma_q' in params:
                Sigma_w = Sw + np.dot(A,np.dot(params['Sigma_q'],A.T))
            else:
                Sigma_w = Sw
        #print "prior: mu_w=", self.mean_w, ", Sig_w=", self.Sigma_w
        #print "phi_t=", phi_t
        #print "mu_q=", mu_q, ", Sig_q=", Sigma_q
        #print "mu_w=", mu_w, "Sw=", Sw, " Sig_w=", Sigma_w
        return proMP(mean_w=mu_w, Sigma_w=Sigma_w, Sigma_y = self.Sigma_y)

    # The marginal distribution (mean and covariance of the resulting Gaussian)
    # for a particular value of t
    def marginal_w(self, Phi_t, **params):
        params.setdefault('ignore_Sigma_y', True)
        res_mean = np.dot(Phi_t, self.mean_w)
        res_cov = np.dot(Phi_t, np.dot(self.Sigma_w, Phi_t.T))
        if not params['ignore_Sigma_y']:
            res_cov += self.Sigma_y
        return res_mean, res_cov

    def _init_EM(self, Phi, y, **params):
        assert len(Phi) == len(y) and len(Phi)>0
        N = len(Phi)
        K, D = np.shape(Phi[0][0])
        params.setdefault('mu_w', np.zeros(D))
        params.setdefault('Sigma_w', np.eye(D))
        params.setdefault('Sigma_y', np.eye(K))
        self.mean_w = params['mu_w']
        self.Sigma_w = params['Sigma_w']
        self.Sigma_y = params['Sigma_y']
        return N,D,K

    def _EM_lowerbound(self, Phi, y, expected_w, variance_w, N, D, **params):
        ans = 0
        inv_sig_w = np.linalg.inv(self.Sigma_w)
        tmp, log_det_sig_w = np.linalg.slogdet(self.Sigma_w)
        inv_sig_y = np.linalg.inv(self.Sigma_y)
        tmp, log_det_sig_y = np.linalg.slogdet(self.Sigma_y)
        for n in xrange(N):
            Tn = len(y[n])
            dif1 = expected_w[n] - self.mean_w
            p1 = log_det_sig_w + np.trace(np.dot(inv_sig_w,variance_w[n])) + np.dot(dif1, np.dot(inv_sig_w,dif1))
            p2 = 0
            for t in xrange(Tn):
                dif2 = y[n][t] - np.dot(Phi[n][t], expected_w[n])
                trace = np.sum(inv_sig_y*np.dot(Phi[n][t], np.dot(variance_w[n], Phi[n][t].T)))
                p2 += log_det_sig_y + trace + np.dot(dif2,np.dot(inv_sig_y,dif2))
            ans += p1 + p2
        return -0.5 * ans

    def _E_step(self, Phi, y, N, **params):
        expected_w = []
        variance_w = []
        inv_sig_w = np.linalg.inv(self.Sigma_w)
        inv_sig_y = np.linalg.inv(self.Sigma_y)
        for n in xrange(N):
            Tn = len(y[n])
            tmp1 = sum(map(lambda t: np.dot(Phi[n][t].T, np.dot(inv_sig_y, y[n][t])), range(Tn)))
            tmp2 = sum(map(lambda t: np.dot(Phi[n][t].T, np.dot(inv_sig_y, Phi[n][t])), range(Tn)))
            tmp2 = (tmp2 + tmp2.T) / 2.0 #For numerical issues (Make positive definite)
            swn = np.linalg.inv(inv_sig_w + tmp2)
            swn = (swn + swn.T) / 2.0 #For numerical issues (Make positive definite)
            wbar = np.dot( swn, tmp1 + np.dot(inv_sig_w,self.mean_w) )
            expected_w.append(wbar)
            variance_w.append(swn)
        return expected_w, variance_w

    def _M_step(self, Phi, y, expected_w, variance_w, N, D, K, **params):
        self.mean_w = sum(expected_w) / N;
        if (params['print_inner_params']): print "mean_w=", self.mean_w
        if (params['print_inner_lb']): print "lb(mean_w)=", self._EM_lowerbound(Phi, y, expected_w, variance_w, N, D, **params)
        sig_w = np.zeros((D,D))
        sig_y = np.zeros((K,K))
        sum_T = 0
        for n in xrange(N):
            Tn = len(y[n])
            dif = expected_w[n] - self.mean_w
            sig_w += variance_w[n] + np.outer(dif,dif)
            for t in xrange(Tn):
                dif2 = y[n][t] - np.dot(Phi[n][t], expected_w[n])
                sig_y += np.outer(dif2,dif2) + np.dot(np.dot(Phi[n][t],variance_w[n]),Phi[n][t].T)
            sum_T += Tn
        self.Sigma_w = sig_w / N
        self.Sigma_w = (self.Sigma_w + self.Sigma_w.T) / 2.0 #for numerical stability (pos-def mat)
        if (params['joint_indep']): make_block_diag(self.Sigma_w, params['num_joints']) 
        if (params['print_inner_params']): print "Sigma_w=", self.Sigma_w
        if (params['print_inner_lb']): print "lb(Sigma_w)=", self._EM_lowerbound(Phi, y, expected_w, variance_w, N, D, **params)
        self.Sigma_y = sig_y / sum_T
        self.Sigma_y = (self.Sigma_y + self.Sigma_y.T) / 2.0 #for numerical stability (pos-def mat)
        if (params['diag_sy']): self.Sigma_y = np.diag(np.diag(self.Sigma_y))
        if (params['print_inner_params']): print "Sigma_y=", self.Sigma_y
        if (params['print_inner_lb']): print "lb(Sigma_y)=", self._EM_lowerbound(Phi, y, expected_w, variance_w, N, D, **params)

    def EM_train(self, Phi, y, **params):
        N,D,K = self._init_EM(Phi,y,**params)
        params.setdefault('max_iter', 10)
        params.setdefault('print_lowerbound', False)
        params.setdefault('print_inner_lb', False)
        params.setdefault('print_params', False)
        params.setdefault('print_inner_params', False)
        params.setdefault('diag_sy', False)
        params.setdefault('joint_indep', False)
        max_iter = params['max_iter']
        for i in xrange(max_iter):
            expected_w, variance_w = self._E_step(Phi,y,N,**params)
            if ('early_quit' in params and params['early_quit']()): break
            if (params['print_lowerbound']):
                print "E-step LB: ", self._EM_lowerbound(Phi,y,expected_w,variance_w,N,D,**params)
            self._M_step(Phi, y, expected_w,variance_w,N,D,K,**params)
            if ('early_quit' in params and params['early_quit']()): break
            if (params['print_lowerbound']):
                print "M-step LB: ", self._EM_lowerbound(Phi,y,expected_w,variance_w,N,D,**params)
        if params['print_params']:
            print "Parameters after optimization:"
            print "mean_w = ", self.mean_w
            print "Sigma_w = ", self.Sigma_w
            print "Sigma_y = ", self.Sigma_y

    @classmethod
    def build_from_stream(cls, stream):
        obj = cls()
        obj.mean_w = stream['mu_w']
        obj.Sigma_w = stream['Sigma_w']
        obj.Sigma_y = stream['Sigma_y']
        return obj

    def to_stream(self):
        ans = {'mu_w': self.mean_w.tolist(), 'Sigma_w': self.Sigma_w.tolist(), 'Sigma_y': self.Sigma_y.tolist()}
        return ans


class proMP_Dependent_Robot_Angles:
    """ A ProMP that considers correlations between different joints (See Alex paper)

    This class uses the basic ProMP implementation as the underlying model, but builds
    the feature matrix Phi_t in a way that considers correlations between the different
    joints. Instead of organizing the matrix as suggested in the original paper, we
    chose a different (but equivalent) organization to make it compatible with the C++
    implementation.
    """
    #Attributes:
    #kernel: A kernel object (like proMP_Gauss_Basis, see comments for more info)
    #model: One single proMP model for all joints
    #num_joints: Number of joints in the robot

    def __init__(self, kernel, **params):
        self.kernel = kernel
        if 'model' in params: self.model = params['model']
        if 'num_joints' in params: self.num_joints = params['num_joints']

    def _get_Phi_t(self, t, pos=True, vel=True, acc=False, **params):
        """ Builds the matrix Phi_t with the same format as the C++ implementation
        """
        assert t>=0 and t<=1
        params.setdefault('vel_fac', 1.0) #velocity factor (Usually depends on exec. vel)
        pos_t = []
        vel_t = []
        acc_t = []
        for d in xrange(self.num_joints):
            if pos: pos_t.append( self.kernel(t) )
            if vel: vel_t.append( params['vel_fac'] * self.kernel.time_deriv(t) )
            if acc: acc_t.append( params['vel_fac']**2 * self.kernel.time_deriv(t,2) )
        ans = []
        if pos: ans.append(block_diag(*pos_t))
        if vel: ans.append(block_diag(*vel_t))
        if acc: ans.append(block_diag(*acc_t))
        return np.concatenate(ans, axis=0)

    def _get_y_t(self, **params):
        """ Builds the vector y_t to be compatible with the matrix Phi_t 

        This method builds a vector y_t with any valid combination of 
        joint position, velocity and acceleration. 
        """
        y_t = []
        if 'q' in params: y_t.extend(params['q'])
        if 'qd' in params: y_t.extend(params['qd'])
        if 'qdd' in params: y_t.extend(params['qdd'])
        return np.array(y_t)

    def __init_training(self,**params):
        assert('time' in params)
        time = params['time']
        W = None
        Wdot = None
        N = len(time)
        if 'W' in params:
            W = params['W']
            assert(len(W) == N)
            self.num_joints = D = np.shape(W[0])[1]
        if 'Wdot' in params:
            Wdot = params['Wdot']
            assert(len(Wdot) == N)
            self.num_joints = D = np.shape(Wdot[0])[1]
        assert N>0
        Phi = []
        y = []
        for n in xrange(N):
            time_n = time[n]
            Phi_n = []
            y_n = []
            t0 = time_n[0]
            T = time_n[-1] - t0
            tfac = 1.0 / T
            for ix, t in enumerate(time_n):
                joint_state = {}
                if W: joint_state['q'] = W[n][ix,:]
                if Wdot: joint_state['qd'] = Wdot[n][ix,:]
                y_nt = self._get_y_t(**joint_state)
                Phi_nt = self._get_Phi_t((t - t0)/T, 'q' in joint_state, 'qd' in joint_state, False, vel_fac=tfac)
                y_n.append(y_nt)
                Phi_n.append(Phi_nt)
            Phi.append(Phi_n)
            y.append(y_n)
        return Phi, y

    def build_train_set(self, **params):
        return self.__init_training(**params)

    def train(self, **params):
        Phi, y = self.__init_training(**params)
        self.model = proMP(**params)
        self.model.EM_train(Phi, y, num_joints=self.num_joints, **params)

    def marginal_w(self, t, **params):
        params.setdefault('use_pos', True)
        params.setdefault('use_vel', False)
        params.setdefault('use_acc', False)
        Phi_t = self._get_Phi_t(t, params['use_pos'], params['use_vel'], params['use_acc'])
        return self.model.marginal_w(Phi_t, **params)

    def condition(self, t, **params):
        """ Returns a conditioned ProMP

        Condition the ProMP to pass be at time t with some desired position, velocity
        and acceleration (q, qd, qdd respectively). If there is
        uncertainty on the conditioned point pass it as the optional matrices Sigma_q,
        Sigma_qd or Sigma_qdd. Use optional parameters cond_pos and cond_vel to decide
        on which combination of position and velocity to condition and set mu_t (
        and possibly Sigma_t accordingly)
        """
        Phi_t = self._get_Phi_t(t, 'q' in params, 'qd' in params, 'qdd' in params)
        mean = self._get_y_t(**params)
        cov_diag = []
        if 'Sigma_q' in params: cov_diag.append(params['Sigma_q'])
        if 'Sigma_qd' in params: cov_diag.append(params['Sigma_qd'])
        if 'Sigma_qdd' in params: cov_diag.append(params['Sigma_qdd'])
        if len(cov_diag) != 0:
            cov = block_diag(*cov_diag)
            model = self.model.condition(Phi_t, mean, Sigma_q=cov)
        else:
            model = self.model.condition(Phi_t, mean)
        return proMP_Dependent_Robot_Angles(self.kernel, model=model, num_joints=self.num_joints)

    def sample_trajectory(self,duration,**params):
        params.setdefault('robot_freq', 500)
        q = []
        qdot = []
        qdotdot = []
        ws = np.random.multivariate_normal(self.model.mean_w, self.model.Sigma_w).T
        tfac = float(params['robot_freq']) / duration
        for t in xrange(duration):
            time = t / float(duration)
            phi_t = self._get_Phi_t(time, True, True, True, vel_fac = tfac)
            yt = np.dot(phi_t, ws)
            qt = yt[0:self.num_joints]
            qdott = yt[self.num_joints:2*self.num_joints]
            qdotdott = yt[2*self.num_joints:]
            q.append(qt)
            qdot.append(qdott)
            qdotdot.append(qdotdott)
        return q, qdot, qdotdot

    def to_stream(self):
        ans = {'kernel': self.kernel.to_stream(), 'model': self.model.to_stream(), 'num_joints': self.num_joints}
        return ans

    @classmethod
    def build_from_stream(cls, stream):
        obj = cls(KernelBuilder.build_from_stream(stream["kernel"]) , model = \
            proMP.build_from_stream(stream['model']), num_joints = stream['num_joints'])
        return obj

class Robot_Ball_Overlap:

    def _canonical_constant(self,lamb,nab,lamb_inv,dims=3):
        lsign, lamb_log_det = np.linalg.slogdet(lamb)
        inner = np.dot(nab,np.dot(lamb_inv,nab))
        return -0.5*(dims*np.log(2*np.pi) - lamb_log_det + inner)

    def _canonical_terms(self, mean, cov):
        lam = np.linalg.inv(cov)
        nab = np.dot(lam, mean)
        c = self._canonical_constant(lam,nab,cov)
        return lam, nab, c

    def __init__(self,kinematics):
        self.kinematics = kinematics

    def trajectory_overlap(self, means_ball, covs_ball, means_robot, covs_robot, ball_offset):
        N = len(means_robot)
        dims = 3
        ans = []
        for n in xrange(N):
            lam_ball, nab_ball, c_ball = self._canonical_terms(means_ball[n+ball_offset], \
                covs_ball[n+ball_offset])
            lam_rob, nab_rob, c_rob = self._canonical_terms(means_robot[n],covs_robot[n])
            lam_hat = lam_ball + lam_rob
            nab_hat = nab_ball + nab_rob
            c_hat = self._canonical_constant(lam_hat,nab_hat,np.linalg.inv(lam_hat))
            ans.append( np.exp(c_ball + c_rob - c_hat) )
        return ans

    def trajectory_cost(self, means_ball, covs_ball, means_robot, covs_robot, ball_offset):
        return sum(self.trajectory_overlap(means_ball,covs_ball,means_robot,covs_robot,ball_offset))

    def compute_robot_trajectory(self,t0,T,ball_times,proMP_function):
        Tball = len(ball_times)
        ball_offset = Tball
        means = []
        covs = []
        orients = []
        for t in xrange(Tball):
            trob = (ball_times[t] - t0) / T
            if trob >= 0 and trob <= 1:
                ball_offset = min(ball_offset, t)
                mean_wt, Sigma_wt = proMP_function(trob)
                mean_rt, jac, ori = self.kinematics.position_and_jac(mean_wt)
                jac = jac[0:3,:]
                Sigma_rt = np.dot(jac, np.dot(Sigma_wt, jac.T))
                means.append(mean_rt)
                covs.append(Sigma_rt)
                orients.append(ori)
        return means, covs, orients, ball_offset

    def start_time_convolution(self, T, mean_balls, covs_ball, ball_times, proMP_function, **params):
        rmeans, rcovs, rorient, ball_offset = self.compute_robot_trajectory(ball_times[0], T, ball_times, proMP_function)
        n = len(ball_times) - len(rmeans)
        init_times = np.linspace(ball_times[0],ball_times[-1]-T,n)
        values = []
        for i in xrange(n):
            val = self.trajectory_cost(mean_balls, covs_ball, rmeans, rcovs, i)
            values.append(val)
        return init_times, values

    def opt_start_time(self, T, mean_balls, covs_ball, ball_times, proMP_function, **params):
        init_times, values = self.start_time_convolution(T,mean_balls,covs_ball,ball_times,proMP_function,**params)
        index = np.argmax(values)
        return init_times[index]


class ProbInvKinematics:
    #params:
    #fwd_k: A forward kinematics object

    def __laplace_cost_and_grad(self, theta, mu_theta, inv_sigma_theta, mu_x, inv_sigma_x):
        f_th, jac_th, ori = self.fwd_k.position_and_jac(theta)
        jac_th = jac_th[0:3,:]
        diff1 = theta - mu_theta
        tmp1 = np.dot(inv_sigma_theta, diff1)
        diff2 = f_th - mu_x
        tmp2 = np.dot(inv_sigma_x, diff2)
        nll = 0.5*(np.dot(diff1,tmp1) + np.dot(diff2,tmp2))
        grad_nll = tmp1 + np.dot(jac_th.T,tmp2)
        return nll, grad_nll

    def __init__(self, fwd_kinematics):
        self.fwd_k = fwd_kinematics

    def inv_kin(self, mu_theta, sig_theta, mu_x, sig_x):
        inv_sig_theta = np.linalg.inv(sig_theta)
        inv_sig_x = np.linalg.inv(sig_x)
        cost_grad = lambda theta: self.__laplace_cost_and_grad(theta, mu_theta, inv_sig_theta, mu_x, inv_sig_x)
        cost = lambda theta: cost_grad(theta)[0]
        grad = lambda theta: cost_grad(theta)[1]
        res = opt.minimize(cost, mu_theta, method='BFGS', jac=grad)
        post_mean = res.x
        post_cov = res.hess_inv
        return post_mean, post_cov
