
import numpy as np

def time_align(t1, t2, epsilon=1e-3):
    """ Computes the time alignment of two time series

    Returns two lists of indices where the respective time series match
    """
    i = 0
    j = 0
    t1_ans = []
    t2_ans = []
    while (i<len(t1) and j<len(t2)):
        d = t1[i] - t2[j]
        if abs(d) < epsilon:
            t1_ans.append(i)
            t2_ans.append(j)
            i += 1
            j += 1
        elif d < 0:
            i+=1
        else:
            j+=1
    return t1_ans, t2_ans


def distance(t1, y1, t2, y2, **params):
    """ Computes the distance between two time series
    
    Takes as input two time series (t1,y1) and (t2,y2) and returns
    a time series (t_ans, y_ans) corresponding to the difference between the
    given time series.
    The optional parameter epsilon can be given to change the default value of
    up to what difference in time between t1 and t2 is considered to be equal.
    """
    params.setdefault('epsilon', 1e-3)
    i = 0
    j = 0
    t_ans = []
    y_ans = []
    while (i<len(t1) and j<len(t2)):
        d = t1[i] - t2[j]
        if abs(d) < params['epsilon']:
            t_ans.append( (t1[i]+t2[j]) / 2.0 )
            y_ans.append(y1[i] - y2[j])
            i += 1
            j += 1
        elif d < 0:
            i+=1
        else:
            j+=1
    return np.array(t_ans), np.array(y_ans)

def summary_stats(t, y):
    """ Computes and returns a dictonary with summary statistics for the given time series
    
    Returns a dictionary with the following summary statistics of the given time series:
    
    * min: Minimum value of the output in each dimension
    * max: Maximum value of the output in each dimension
    * mean: Average vector of the output
    * std: Standard deviation of the output
    * norm: Norm of the time series output
    * norm_mean: Scalar corresponding to the mean of the norm of the output
    """
    norms = np.linalg.norm(y, axis=1)
    ans = {'min': np.min(y, axis=0),\
        'max': np.max(y, axis=0),\
        'mean': np.average(y, axis=0),\
        'std': np.std(y, axis=0),\
        'norm': norms, \
        'norm_mean': np.mean(norms)}
    return ans

def zero_cross_vel(qdot, split_joint=0, zero_tol=0.01, seed_ix=None):
    """Zero crossing velocity algorithm for robot trajectory splitting

    This method returns a pair of integers (a,b) specifying an index range
    that can be used to split the given robot trajctory. The algorithm works
    as follows:

    1) Find the maximum joint velocity
    2) Find an index range (a,b) that contains the maximum joint velocity and
       where the velocity at a and b are close to zero.

    Two optional named parameters can be used with this method:

    * split_joint: Joint index used to split the data
    * zero_tol: Percentaje of the maximum velocity considered zero

    """
    #1) Find the maximum velocity joint velocity
    vel = qdot[:,split_joint]
    max_ix = np.argmax(np.abs(vel)) if seed_ix is None else seed_ix
    if vel[max_ix] < 0: vel = -vel
    zero_vel = zero_tol*vel[max_ix]
    #2) Move from maximum until zero_vel velocity is found
    a = b = max_ix
    while a>0 and vel[a] > zero_vel: a -= 1
    while b<len(vel) and vel[b] > zero_vel: b += 1
    return (a,b)

def mahalanobis_norm(X, P=None):
    """ A norm computed as sqrt(x^T P x) for all rows x in X
    """
    if P is None:
        P = np.eye(np.shape(X)[1])
    return np.sqrt(np.sum(np.dot(X,P)*X, axis=1))

def multiple_zero_cross_vel(qdot, P=None, low_thresh=0.05, high_thresh=0.7, **params):
    """ Zero crossing velocity algorithm using the Mahalanobis norm

    Find multiple time series segments using the zero crossing velocity heuristic
    and the Mahalanobis norm. The threshold parameters are porcentajes w.r.t the
    maximum velocity.
    """
    ans = []
    vel = mahalanobis_norm(qdot, P=P)
    mvel = max(vel)
    cpt = 0
    while cpt < len(vel):
        if vel[cpt] > (mvel*high_thresh):
            a = b = cpt
            while a>0 and vel[a]>(mvel*low_thresh): a -= 1
            while b<len(vel) and vel[b]>(mvel*low_thresh): b += 1
            ans.append((a,b))
            cpt = b
        cpt = cpt + 1
    return ans
