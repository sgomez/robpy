
#import rob_protos.table_tennis_pb2 as ttpb
#import rob_protos.ball_vision_pb2 as bvpb
import struct
import threading
import zmq

'''
def load_trials(fname):
    ssize = struct.Struct('I')
    data = []
    with open(fname,'r') as f:
        while True:
            x = f.read(ssize.size)
            if x=='': 
                break
            trial = ttpb.Trial()
            nb = ssize.unpack(x)[0]
            trial.ParseFromString(f.read(nb))
            data.append(trial)
    return data
'''

class PBFileWriter:

    def __init__(self, f_name, mode='ab'):
        self._fout = open(f_name, mode)

    def append(self, register):
        serialized = register.SerializeToString()
        self._fout.write(struct.pack('I', len(serialized)))
        self._fout.write(serialized)


class PBFileReader:

    def __init__(self, f_name, mode='rb'):
        self._fin = open(f_name, mode)
        self._shead = struct.Struct('I')

    def next(self, register):
        header = self._fin.read( self._shead.size )
        if header=='': return False
        nb = self._shead.unpack(header)[0]
        register.ParseFromString(self._fin.read(nb))
        return True

