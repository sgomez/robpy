data {
  int<lower=0> N; //Total number of observations
  int<lower=0> n_cams; //Number of cameras
  vector[3] obs3d[N]; //3D observations
  vector[2] obs2d[N,n_cams];  //2d pixel observations
  int<lower=0,upper=1> is_obs_2d[N,n_cams]; //was pixel observed?
  int<lower=0,upper=1> is_obs_3d[N]; //was 3d point observed?

  //row and column indices to constrain homogeneous projection matrices
  int<lower=0> proj_ix[11,2];

  //Prior parameters
  cov_matrix[2] prior_sigma_iw; //Inverse wishart matrix for prior on Sigma
  real<lower=0> prior_sigma_dof; //Inverse wishart degrees of freedom from prior on Sigma
  cov_matrix[3] prior_error_3d_iw; //Inverse wishart matrix for prior on 3D error
  real<lower=0> prior_error_3d_dof; //Inverse wishart degrees of freedom from prior on 3D error
}
parameters {
  vector[11] proj_params[n_cams]; //Projection Matrices
  cov_matrix[2] sigma_2d; //Covariance matrix in pixels
  cov_matrix[3] sigma_3d; //Covariance matrix on 3D given points
  vector[3] hid3d[N]; //Hidden 3d points
}
model {
  matrix[3,4] projection[n_cams];
  vector[2] pts2d_mean[N,n_cams];
  vector[3] pt2d_homo;
  vector[4] pt3d_homo;

  for (cam_id in 1:n_cams) {
    for (i in 1:11) {
      projection[cam_id][proj_ix[i,1],proj_ix[i,2]] = proj_params[cam_id][i];
    }
    projection[cam_id][3,4] = 1.0;
  }

  for (n in 1:N) {
    for (i in 1:3) {
      pt3d_homo[i] = hid3d[n][i];
    }
    pt3d_homo[4] = 1.0;
    for (cam_id in 1:n_cams) {
      pt2d_homo = projection[cam_id]*pt3d_homo;
      for (i in 1:2) {
        pts2d_mean[n,cam_id][i] = pt2d_homo[i] / pt2d_homo[3];
      }
    }
  }

  sigma_2d ~ inv_wishart(prior_sigma_dof, prior_sigma_iw);
  sigma_3d ~ inv_wishart(prior_error_3d_dof, prior_error_3d_iw);
 
  for (n in 1:N) {
    if (is_obs_3d[n]) {
      obs3d[n] ~ multi_normal(hid3d[n],sigma_3d);
    }
  }
  for (n in 1:N) {
    for (cam_id in 1:n_cams) {
      if (is_obs_2d[n][cam_id]) {
        obs2d[n,cam_id] ~ multi_normal(pts2d_mean[n,cam_id], sigma_2d);
      }
    }
  }

}
