
import robpy.slpp as slpp
import numpy as np
import matplotlib.pyplot as plt
import sys
import time
import json
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Record ball and joint information in JSON format')
    parser.add_argument('--host', default='localhost', help="Name of the host to connect to")
    parser.add_argument('--port', default=7648, help="Port of the host to connect to")
    parser.add_argument('--url', help="Overrides the host and port setting directly a ZMQ bind URL")
    parser.add_argument('--goto', help="Request the desired joint target where the robot should be moved", action="store_true")
    parser.add_argument('--out', default='saved.out', help="File name where the data is stored")
    parser.add_argument('--time', default=1.0, help="Time where data is recorded")
    args = parser.parse_args()
    if args.goto:
        des_joints = []
        for i in xrange(7):
            des_joints.append( input("Desired joint angle on joint {0}: ".format(i+1)) )

    url = "tcp://{0}:{1}".format(args.host, args.port) if not args.url else args.url
    client = slpp.SLPP( url=url )

    rob_time = client.get_time()
    print "Robot time: ", rob_time
    if args.goto:
        t0 = rob_time + 0.1
        client.go_to(t0, 1.0, des_joints, [0,0,0,0,0,0,0])
        print "Sending go to operation from time t0=", t0
    client.clear_sensor_obs()
    print "Sensor observations cleared"
    client.clear_joint_obs()
    print "Joint measurements cleared"
    T = float(args.time)
    print "Waiting for T=", T
    time.sleep(T)
    print "Obtaining joint and sensor measurements"
    joints = client.get_joint_obs()
    obs = client.flush_sensor_obs()
    to_save = {'joints': joints, 'obs': obs}
    f = open(args.out,'w')
    f.write(json.dumps(to_save))
    print "Sensor information obtained and saved"
