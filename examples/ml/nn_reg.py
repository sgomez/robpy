import numpy as np
import tensorflow as tf
import robpy.neural_process as rnp
import matplotlib.pyplot as plt

import argparse
import os
import json

def main(args):
    with open(args.ball_data,'r') as f:
        data = np.load(f)
        x = np.reshape(data['times'][0], (-1,1))
        y = np.reshape(data['obs'][0:args.ntraj,:,2].T, (-1,args.ntraj))
    print x.shape
    xt = np.linspace(0,1.2,413).reshape((-1,1)) #Pick some other inputs in same range
    with tf.Session() as sess:
        activations = [tf.nn.relu,tf.nn.tanh,tf.nn.relu,tf.nn.tanh,tf.nn.relu,tf.nn.relu,tf.nn.tanh]
        mlp = rnp.MLPReg(hidden_units=[15,20,40,60,40,20,12], in_dims=1, out_dims=args.ntraj, 
                hidden_activation=tf.nn.relu, 
                batch_norm=False)

        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        init_loss = mlp.eval_loss(sess, x, y)
        print("Loss before training: {}".format(init_loss))

        history = mlp.fit(sess, x, y, batch_size=100, epochs=400, lr=1e-2, decay=0.9, decay_epochs=20)
        plt.plot(history['loss'])
        plt.show()

        yt = mlp.predict(sess, xt)

    for i in range(args.ntraj):
        plt.plot(x,y[:,i],'rx')
        plt.plot(xt, yt[:,i])
        plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('ball_data', help="Path to file with the ball data")
    #parser.add_argument('model', help="Path to the model file")
    parser.add_argument('--ntraj', type=int, default=1, help="Number of trajectories")
    args = parser.parse_args()
    main(args)
