
import lib_robotics as lr
import numpy as np
import matplotlib.pyplot as plt

w = np.array([-1, 0.5]) # line is y = 0.5*x - 1
n = 100
t = np.array([np.linspace(-5, 5, n)])

x = np.concatenate((np.ones((n,1)), t.T), axis=1)
y = np.dot(x, w) + np.random.normal(0.0, 0.1, n)
n_outliers = int(0.2*n)
y[ np.random.permutation(n)[0:n_outliers] ] = np.random.normal(0.0, 10, n_outliers)

rans_conf = lr.RansacConf()
rans_conf.tol = 0.5
rans_conf.group_size = 8
rans_conf.num_iter = 100

y_mat = np.array([y]).T
rans_ans = lr.linear_ransac(np.asfortranarray(x), np.asfortranarray(y_mat), rans_conf)
print rans_ans.x

t_line = np.array([np.linspace(-5,5,400)]).T
t_mat = np.concatenate( (np.ones((400,1)), t_line), axis=1 )
y_line = np.dot(t_mat , rans_ans.x )
plt.plot(x[:,1],y,'rx')
plt.plot(t_line[:,0], y_line)
plt.show()
