
import numpy as np
import tensorflow as tf
import robpy.neural_process as rnp
import robpy.utils as utils
import robpy.full_promp as promp
import robpy.deep_promp as deep_promp
import matplotlib.pyplot as plt

def generate_promp_train_data(N,T):
    full_basis = {'conf': [{"type": "poly", "nparams": 0, "conf": {"order": 3}}], 'params': []}
    p = promp.FullProMP(model={'mu_w': np.zeros(4),
        'Sigma_w': np.eye(4),
        'Sigma_y': 0.0001*np.eye(1)}, num_joints=1, basis=full_basis)
    times = [np.linspace(0,1,T) for i in range(N)]
    Phi = []
    X = p.sample(times, Phi=Phi, q=True)
    return times, Phi, X

N = 100
Nv = 10
T = 50
times, Phi, X = generate_promp_train_data(N + Nv,T)

feature_dims = 4
encoder_type = 1
if encoder_type==0:
    encoder = lambda x, is_training: tf.reshape(
            rnp.feature_mlp(x, is_training, hidden_units=[4,6,10,feature_dims], hidden_activation=tf.nn.tanh, batch_norm=False), 
            shape=(tf.shape(x)[0],tf.shape(x)[1],1,feature_dims)
            )
elif encoder_type==1:
    encoder = lambda x, is_training: tf.reshape(
            deep_promp.rbf(x, np.reshape(np.linspace(0,1,feature_dims-1),(-1,1)), np.ones(feature_dims-1)), 
            shape=(tf.shape(x)[0],tf.shape(x)[1],1,feature_dims)
            )
prior_mu_w = {'m0': tf.zeros((feature_dims,1), dtype=tf.float64), 'k0': 1.0}
#prior_Sigma_w = {'v0': feature_dims+1.0, 'mean_cov_mle': lambda v0, Sw_mle: tf.multiply(Sw_mle, tf.eye(feature_dims, dtype=tf.float64))}
prior_Sigma_w = {'v0': feature_dims+1.0, 'mean_cov_mle': lambda v0, Sw_mle: 10*tf.eye(feature_dims, dtype=tf.float64)}
dpmp = deep_promp.DeepProMP(in_dims=1, out_dims=1, feature_dims=feature_dims, phi=encoder, prior_mu_w=prior_mu_w, prior_Sigma_w=prior_Sigma_w)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    sess.run(tf.local_variables_initializer())
    x = np.array(times[0:N]).reshape((N,T,1))
    y = np.array(X[0:N]).reshape((N,T,1))
    w_means, w_covs = dpmp.comp_w_dist(sess,x,y)
    avg_elbo = dpmp.elbo(sess, x, y, w_means, w_covs)
    print("Untrained prior avg. marginal likelihood: {}".format(avg_elbo))
    y_means, y_covs = dpmp.output_dist(sess, x, w_dist=(w_means, w_covs))
    #plt.title('Example of fit before training')
    #plt.plot(x[0,:,0],y[0,:,0],'rx')
    #plt.plot(x[0,:,0],y_means[0,:,0,0])
    #plt.show()

    history = dpmp.fit(sess, x, y, lr=1e-4, init_phi_lr=1e-2, opt_basis_steps=100, em_batch_size=100, basis_batch_size=25, iterations=20, init_phi_iter=200)
    if 'init_phi_loss' in history:
        phi_loss = history['init_phi_loss']
        plt.plot(phi_loss)
        plt.title('Loss of the basis function initialization procedure')
        plt.show()
    ob_loss = np.mean(history['opt_basis_losses'], axis=0)
    plt.plot(ob_loss)
    plt.show()
    plt.plot(history['loss'])
    plt.show()
    w_means, w_covs = dpmp.comp_w_dist(sess,x[:,0:T/2],y[:,0:T/2])
    y_means, y_covs = dpmp.output_dist(sess, x, w_dist=(w_means, w_covs))
    for i in range(10):
        plt.title('Example of after training')
        plt.plot(x[i,:,0],y[i,:,0],'rx')
        plt.plot(x[i,:,0],y_means[i,:,0,0])
    plt.show()

    trained_avg_elbo = dpmp.elbo(sess, x, y, w_means, w_covs)
    print("Trained prior avg. marginal likelihood: {}".format(trained_avg_elbo))
    print(sess.run(dpmp.Sigma_y))
    print(sess.run(dpmp.Sigma_w))


