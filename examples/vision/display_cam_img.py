"""Script to plot and analyze recorded vision data

This script receives as parameter a PB file with the recorded
data and has the ability to present different plots or scores. 
"""

import frame_pb2 as pb_frame

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import argparse
import os
import time
import cv2
import zmq

def main(args):
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect("tcp://{}:{}".format(args.host, args.port))
    topicfilter = ""
    socket.setsockopt(zmq.SUBSCRIBE, topicfilter)

    frm = pb_frame.Frame()
    max_num = -1
    while True:
        msg = socket.recv()
        frm.ParseFromString(msg)
        im_data = np.frombuffer(frm.img.data, dtype=np.uint8)
        im = np.reshape(im_data, (frm.img.height, frm.img.width, frm.img.channels))
        if frm.num < max_num:
            print("Caution. Images are not stored in order")
        if frm.num > max_num:
            max_num = frm.num
            if cv2.waitKey(20) & 0xFF == ord('q'):
                break
        cv2.putText(im, "Im: {}".format(frm.num), (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255), 2, cv2.LINE_AA)
        cv2.imshow('cam{}'.format(frm.cam_id), im)
    cv2.destroyAllWindows()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    rel_path = lambda fname: os.path.join(os.path.dirname(__file__), fname)
    parser.add_argument('--host', default='localhost', help="Name of the host to connect to")
    parser.add_argument('--port', default="7655", help="Port to connect to")
    args = parser.parse_args()
    main(args)
