
"""Example Script to capture the 3D position of the end effector of the robot

This example script produces a JSON file with the position of the end effector of the
robot every time the user presses Enter. To finish type exit followed by enter.

"""

import robpy.slpp as slpp
import robpy.kinematics.forward as fwd_kin
import numpy as np
import sys
import time
import json
import argparse

def save(to_save, file_name):
    f = open(file_name, 'w')
    f.write(json.dumps(to_save))
    f.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Record the state of the robot after receiving user input')
    parser.add_argument('--host', default='localhost', help="Name of the host to connect to")
    parser.add_argument('--port', default=7648, help="Port of the host to connect to")
    parser.add_argument('--out', default='saved.out', help="File name where the data is stored")
    parser.add_argument('--start', type=int, default=1, help="First index of the recorded data")
    args = parser.parse_args()

    url = "tcp://{0}:{1}".format(args.host, args.port)
    client = slpp.SLPP( url=url )
    barrett_kin = fwd_kin.BarrettKinematics()

    ans = []
    index = args.start
    client.clear_joint_obs()
    while True:
        user_in = raw_input('--> ')

        if user_in == "exit":
            break
        elif user_in == "print":
            print ans
            continue

        joint = client.get_joint_obs()[-1]['q']
        x, ori = barrett_kin.end_effector(joint)
        elem = {'index': index, 'pos': x.tolist(), 'joints': joint}
        print "Captured: ", elem
        ans.append(elem)
        save(ans, args.out)
        index += 1
