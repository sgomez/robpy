
"""Example Script to test the 3D position estimation with calibrated cameras

This example script takes as input a file with the parameters of the calibrated
cameras. Then you can use it to estimate the 3D position of a point given the
projection of that point in at least two cameras.

This script can also be used to extend a captured JSON file with a new 3D sensor 
(or replace a existing one). The new 3D sensor information is produced by the 
stereo algorithm provided that the captured file has 2D information from the
calibrated cameras. For instance, a command like:

    python stereo.py calib_matrices.txt --cap_file input.json --new3d 20 --out_file output.json 

would produce a new JSON file called "output.json" equal to the input but with a new
3D sensor with index 20. The information is produced by triangulating the 2D positions
read from the input file and with the calibration parameters in "calib_matrices.json"
"""

import robpy.slpp as slpp
import robpy.kinematics.forward as fwd_kin
import robpy.vision as vision
import numpy as np
import sys
import time
import json
import argparse
import os

def load_proj_mat(fname):
    C = np.loadtxt(fname)
    proj_mats = []
    for i in xrange(4):
        x = C[3*i:3*i+3,:]
        x = (x / x[2,3]) #normalize to have one as last parameter
        proj_mats.append(x)
    return proj_mats

def stereo(calib, obs2d):
    c_mat = []
    pt2d = []
    for elem in obs2d:
        c_mat.append(calib[elem['id']])
        pt2d.append(elem['obs'])
    return vision.stereo_vision(c_mat, pt2d)

def sl_stereo(calib, client):
    all_obs = client.flush_sensor_obs(ids=[0,1,10,11,12,13])
    obs2d = []
    for cam_obs in all_obs:
        idx = cam_obs['id']
        if 'values' in cam_obs:
            point = cam_obs['values'][-1]['obs']
            if idx == 0 or idx==1: 
                print 'SL Camera {0} produced observation {1}'.format(idx,point)
            else:
                obs2d.append({'id': idx-10, 'obs': point})
    print "\nObservations in 2D:"
    print obs2d
    pt3d = stereo(calib, obs2d)
    print "\nMy 3D estimate: ", pt3d

def flatten(obs_data, ix2d):
    """Returns a list of tuples with time, camera id, and observation

    Given the data in the common JSON format and a set of camera indices to
    take into account, this method returns a unique list of tuples of observations.
    """
    ans = []
    for elem in obs_data:
        if elem['id'] in ix2d and 'values' in elem:
            for x in elem['values']:
                ans.append((x['time'], elem['id'], x['obs']))
    return ans

def get_all_3d(flattened, calib_dict, stereo_method):
    ans = []
    i = 0
    time_eps = 1e-3
    while i<len(flattened):
        j = i
        proj_mat = []
        pts2d = []
        while j<len(flattened) and abs(flattened[i][0] - flattened[j][0]) < time_eps:
            proj_mat.append(calib_dict[flattened[j][1]])
            pts2d.append(flattened[j][2])
            j += 1
        i = j
        if len(pts2d) >= 2:
            ans.append({'time': flattened[i][0], 'obs': stereo_method(proj_mat, pts2d)})
    return ans         

def process_stored_data(data, calib, args):
    ix2d = [10, 11, 12, 13]
    calib_dict = {ix: calib[i] for i, ix in enumerate(ix2d)}
    obs2d = flatten(data['obs'], ix2d)
    obs2d.sort()
    pts3d = get_all_3d(obs2d, calib_dict, vision.stereo_vision)
    if args.new3d is not None:
        data['obs'].append({'id': args.new3d, 'values': pts3d})

    if args.out_file is not None:
        slpp.save_json(args.out_file, data)

def main(args):
    if args.calib_file is None or not os.path.isfile(args.calib_file):
        raise Exception("The calibration file was not given or non existent")
    else:
        calib = load_proj_mat(args.calib_file)
    if args.host is not None:
        url = "tcp://{0}:{1}".format(args.host, args.port)
        client = slpp.SLPP( url=url )
    else: 
        client = None
    
    if args.cap_file is not None:
        data = slpp.load_json(args.cap_file)
        process_stored_data(data, calib, args)
    else:
        sl_stereo(calib, client)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('calib_file', help="Path to the camera calibration parameters file")
    parser.add_argument('--host', help="Name of the host to connect to")
    parser.add_argument('--port', default=7648, help="Port of the host to connect to")
    parser.add_argument('--cap_file', help="If instead of gathering data from a remote host it should be read from a file")
    parser.add_argument('--new3d', type=int, help="The new index to fill out the 3D estimates")
    parser.add_argument('--out_file', help="JSON file where the results should be stored")
    args = parser.parse_args()
    main(args)
