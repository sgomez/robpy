
"""Find a green led in dark pictures

This example script produces a JSON file with the pixel position of the green led in every
picture (or null if the led is not found in the image). In this Script we assume that all
the images are located in the same folder, and the name of the images follow the format::

  <camera_name>_<image_number>.<extension>

The extension is by default ``jpg``, and the image number should be zero padded to have
exactly 8 digits. For example, lets suppose that we have only two cameras, named ``cam1``
and ``cam2``. And that we take 2 pictures per camera. The images should be named::

  cam1_00000001.jpg
  cam2_00000001.jpg
  cam1_00000002.jpg
  cam2_00000002.jpg

When you call the script, you should pass with ``--path`` the path to the image folder,
with ``--cam_prefixes`` a list with the camera names, and with ``--num_pics`` the number
of pictures per camera. For the previous example the Script has to be called as::

  python find_green_led.py --path /path/to/images --cam_prefixes cam1 cam2 --num_pics 2

As a result, this Script should create a file named ``obs2d.json`` in the same folder
where the images where stored.

"""

import robpy.vision as vision
import numpy as np
import os
import json
import argparse

def get_img_names(path, cam_name, num_pics, img_ext):
    img_names = []
    for i in xrange(1,num_pics+1):
        fname = "{0}_{1:08d}.{2}".format(cam_name, i, img_ext)
        url = os.path.join(path, fname)
        img_names.append(url)
    return img_names

def main(args):
    pts2d = []
    for cam_name in args.cam_prefixes:
        img_names = get_img_names(args.path, cam_name, args.num_pics, args.img_ext)
        pts2d.append( vision.find_green_leds(img_names, max_led_size=args.max_led_size) )
    ans = {'obs2d': pts2d}
    f = open(args.path + '/obs2d.json', 'w')
    json.dump(ans, f)
    f.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--path', help="Path to the image folder")
    parser.add_argument('--cam_prefixes', nargs='+', help="List with the camera name prefixes for the images")
    parser.add_argument('--num_pics', type=int, help="Number of pictures taken per camera")
    parser.add_argument('--img_ext', default='jpg', help="Extension of the image file name")
    parser.add_argument('--max_led_size', type=int, default=15, help="Maximum area of the led in pixels")
    args = parser.parse_args()
    main(args)
