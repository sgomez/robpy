""" Finds the transformation matrix between the robot and the vision frames

Assuming that a data set was captured with the ball on top of the table tennis racket,
finds the transformation between the robot frame and the vision frame.
"""

import robpy.time_series as ts
import robpy.slpp as slpp
import robpy.kinematics.forward as fwd_kin
import numpy as np
import json
import argparse
from matplotlib import pyplot as plt

def rigid_transform_3D(A, B):
    assert len(A) == len(B)

    N = A.shape[0]; # total points

    centroid_A = np.mean(A, axis=0)
    centroid_B = np.mean(B, axis=0)
    
    # centre the points
    AA = A - np.tile(centroid_A, (N, 1))
    BB = B - np.tile(centroid_B, (N, 1))

    # dot is matrix multiplication for array
    H = np.dot(np.transpose(AA),  BB)

    U, S, Vt = np.linalg.svd(H)

    R = np.dot(Vt.T, U.T)

    # special reflection case
    if np.linalg.det(R) < 0:
       #print "Reflection detected"
       Vt[2,:] *= -1
       R = np.dot(Vt.T, U.T)

    t = -np.dot(R,centroid_A.T) + centroid_B.T
    return R, t

def main(args):
    data = slpp.load_json(args.data)
    q, qd, tr = slpp.joints_to_numpy(data['joints'])
    tb, xb = slpp.obs_to_numpy(data['obs'])

    barrett_kin = fwd_kin.BarrettKinematics(endeff = [0.0,args.ball_offset,0.3,0.0,0.0,0.0])
    xr,_ = barrett_kin.end_eff_trajectory(q)

    ix_r, ix_b = ts.time_align(tr, tb[1])
    R, t = rigid_transform_3D(xb[1][ix_b], xr[ix_r])

    H = np.eye(4)
    H[0:3,0:3] = R
    H[0:3,3] = t

    # Computing the final error
    t_xb = np.dot(R, xb[1][ix_b].T).T + t
    diff = t_xb - xr[ix_r]
    err = np.sqrt(np.sum(diff*diff, axis=1))

    print("Avg error: {}".format(np.mean(err)))
    plt.hist(err, 30)
    plt.title("Error after the axis alignment")
    plt.show()
   
    print("Resulting transformation matrix:")
    print(H)
    if args.out:
        json.dump(H.tolist(), open(args.out,'w'))   

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('data', help="File with the robot and vision information")
    parser.add_argument('--ball_offset', type=float, default=0.03, 
            help="Distance between the center of the racket and the ball")
    parser.add_argument('--out', help="JSON file where the transformation matrix is stored")
    args = parser.parse_args()
    main(args)
