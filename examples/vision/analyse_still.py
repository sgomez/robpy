
import robpy.slpp as slpp
import robpy.kinematics.forward as fwd_kin
import robpy.time_series as time_series
import numpy as np
import matplotlib.pyplot as plt
import sys
import time
import json
import argparse

def load(fname):
    f = open(fname, 'r')
    jstr = f.read()
    return json.loads(jstr)

def display_stats(cam_id, stats):
    print "Stats for cam ", cam_id, ": "
    print "Min: ", stats['min']
    print "Max: ", stats['max']
    print "Mean: ", stats['mean']
    print "Norm mean: ", stats['norm_mean']

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Analyse captured data with the ball attached to the robot end effector')
    parser.add_argument('in_file', help="Input file where the captured data is read from")
    parser.add_argument('--plot', help="Should the data be plotted", action="store_true")
    parser.add_argument('--offset', default='0,0,0,0,0,0', \
        help="Six real values separated by comma containing the desired offset to add to the racket center")
    #parser.add_argument('--time', default=1.0)
    args = parser.parse_args()

    data = load(args.in_file)
    q, qd, t_kin = slpp.joints_to_numpy(data['joints'])

    offset = np.array(map(float, args.offset.split(",")))
    endeff = np.array([0,0,0.3,0,0,0]) + offset
    kin = fwd_kin.BarrettKinematics(endeff)
    x_test, ori_test = kin.end_eff_trajectory(np.zeros((1,7)))
    print x_test
    x_kin, ori_kin = kin.end_eff_trajectory(q)

    t_cam, x_cam = slpp.obs_to_numpy(data['obs'])

    print "Q: ", q[0,:]
    if (0 in x_cam):
        t0_diff, x0_diff = time_series.distance(t_kin, x_kin, t_cam[0], x_cam[0])
        stats0 = time_series.summary_stats(t0_diff, x0_diff)
        display_stats(0,stats0)
    if (1 in x_cam):
        t1_diff, x1_diff = time_series.distance(t_kin, x_kin, t_cam[1], x_cam[1])
        stats1 = time_series.summary_stats(t1_diff, x1_diff)
        display_stats(1, stats1)
    print "FwdKin: ",x_kin[0,:]

    if args.plot:
        dim_names = "XYZ"
        for i in xrange(3):
            plt.subplot(3,1,i+1)
            plt.plot(t_kin, x_kin[:,i])
            if 0 in x_cam:
                plt.plot(t_cam[0], x_cam[0][:,i], 'rx')
            if 1 in x_cam:
                plt.plot(t_cam[1], x_cam[1][:,i], 'bo')
            plt.ylabel(dim_names[i])
        plt.xlabel('Time')
        plt.show()
