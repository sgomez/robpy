"""Compute (or recompute) the position of the end effector from joint angles

This example script produces a JSON file with the position of the end effector of the
robot given a JSON file with the joint angles. This script can be used in our calibration
method, for instance, to add an additional lenght between the LED and the racket center.

"""

import robpy.slpp as slpp
import robpy.kinematics.forward as fwd_kin
import numpy as np
import sys
import time
import json
import argparse

def save_json(to_save, file_name):
    f = open(file_name, 'w')
    f.write(json.dumps(to_save))
    f.close()

def load_json(f_name):
    f = file(f_name,'r')
    data = json.load(f)
    f.close()
    return data

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Recompute the position of the end effector from joint angles')
    parser.add_argument('--input', help="Original JSON file with joint angles")
    parser.add_argument('--out', default='out.json', help="Output file to store recomputed positions")
    parser.add_argument('--offset', nargs=3, type=float, help="X, Y and Z offset with respect to racket center")
    args = parser.parse_args()

    offset = np.array([0,0,0.3,0,0,0]) + np.array(args.offset + [0,0,0])
    barrett_kin = fwd_kin.BarrettKinematics(offset)
    data = load_json(args.input)
    for elem in data:
        if not 'joints' in elem:
            raise Exception("Dictionary {0} does not contain keyword `joints`".format(str(elem)))
        q = elem['joints']
        x, _ = barrett_kin.end_effector(q)
        elem['pos'] = x.tolist()
    save_json(data, args.out)
