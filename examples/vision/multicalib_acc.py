
import numpy as np
import robpy.vision as vision
import robpy.kinematics.forward as fwd_kin
import json
import os

def load_proj_mats(**params):
    params.setdefault('fname', os.path.join(os.path.dirname(__file__), '../config/calib_mats.json'))
    return json.load(open(params['fname'],'r'))

def check_pic_limits(Y, **params):
    params.setdefault('W', 500)
    params.setdefault('H', 500)
    obs_Y = []
    for cam_obs in Y:
        good_obs = []
        for obs in cam_obs:
            if obs[0]>=0.0 and obs[0]<=params['W'] and obs[1]>=0.0 and obs[1]<=params['H']:
                good_obs.append(np.array(obs).tolist())
            else:
                good_obs.append(None)
        obs_Y.append(good_obs)
    return obs_Y

def generate_sim_robot_data(proj_mats, **params):
    params.setdefault('n_robot_obs', 50)
    params.setdefault('n_table_obs', 200)

    if 'file_name' in params:
        if os.path.isfile(params['file_name']):
            f = open(params['file_name'],'r')
            data = json.load(f)
            f.close()
            return data
        else:
            np.random.seed(0)
    barr_kin = fwd_kin.BarrettKinematics()
    Q = np.random.multivariate_normal(np.zeros(7), 0.7*np.eye(7), params['n_robot_obs'])
    rob_X, _ = barr_kin.end_eff_trajectory(Q)
    noisy_rob_X = rob_X + np.random.multivariate_normal(np.zeros(3), 1e-4*np.eye(3), params['n_robot_obs'])
    tab_X = np.random.multivariate_normal([0.0,-1.45,-0.5],np.diag([0.25**2,0.4**2,0.15**2]), params['n_table_obs'])

    full_X = rob_X.tolist() + tab_X.tolist()
    obs_X = noisy_rob_X.tolist() + ([None]*params['n_table_obs'])

    full_Y = []
    full_noisy_Y = []
    for P in proj_mats:
        full_Y.append( vision.project_points(full_X, P) )
        pix_noise = np.random.multivariate_normal(np.zeros(2), 1*np.eye(2), len(full_Y[-1]))
        full_noisy_Y.append( full_Y[-1] + pix_noise )
    obs_Y = check_pic_limits(full_noisy_Y)
    data = {'full_3d': full_X, 'obs_3d': obs_X, 'full_2d': np.array(full_Y).tolist(),
        'obs_2d': obs_Y}
    if 'file_name' in params:
        f = open(params['file_name'],'w')
        json.dump(data, f)
        f.close()
    return data

def test_calib(**params):
    params.setdefault('data_fname', 'calib_data.json')
    proj_mats = load_proj_mats()
    data = generate_sim_robot_data(proj_mats,file_name=params['data_fname'])
    good_data = vision.good_calib_data(data['obs_2d'], data['obs_3d'], min_cam_obs=4)
    print "Number of good data points: ", len(good_data['good_ix'])
    est_proj_mats = vision.lin_multicam_calib(good_data['obs2d'], good_data['obs3d'])
    np.set_printoptions(precision=4, suppress=True)
    for ix in xrange(len(proj_mats)):
        print "Camera ", ix
        print "Real: ", np.array(proj_mats[ix])
        print "Estimated: ", est_proj_mats[ix]
        if est_proj_mats[ix] is not None:
            print "Diff: ", est_proj_mats[ix] - proj_mats[ix]
        print "\n"
    return est_proj_mats

def test_calib_bayesian(**params):
    params.setdefault('fname', 'calib_samples.json')
    params.setdefault('stan_compiled', 'calib_stan_compiled.out')
    params.setdefault('data_fname', 'calib_data.json')
    proj_mats = load_proj_mats()
    data = generate_sim_robot_data(proj_mats,file_name=params['data_fname'])
    good_data = vision.good_calib_data(data['obs_2d'], data['obs_3d'], min_cam_obs=4)
    P_samples = vision.bayesian_multicam_cam_calib(good_data['obs2d'], good_data['obs3d'], mcmc_iter=500,
        stan_compiled=params['stan_compiled'])
    json.dump(P_samples, open(params['fname'],'w'))

def mc_stereo_vision(P_mat_samples, pts2d, **params):
    params.setdefault('n_samples', len(P_mat_samples[0]))
    n_cams = len(pts2d)
    pts3d = []
    for ix in xrange(params['n_samples']):
        P = []
        x = []
        for cam_id in xrange(n_cams):
            if pts2d[cam_id] is not None:
                P.append(P_mat_samples[cam_id][ix])
                x.append(pts2d[cam_id])
        pts3d.append(vision.stereo_vision(P, x))
    return pts3d

def analyse_calib(fname, **params):
    proj_mats = load_proj_mats()
    data = json.load(open(fname,'r'))
    init_mats = test_calib(data_fname=params['data_fname'])
    init_mats = [[P] for P in init_mats]
    P_mats = data['proj_mat_samples']
    #P_mats_avg = map(lambda x: np.mean(np.array(x),axis=0).tolist(), P_mats)
    np.set_printoptions(precision=4, suppress=True)
    #print "Average from samples:"
    #for P in P_mats_avg:
    #  print np.array(P)
    #print "A sample: "
    #P_mats = np.array(P_mats)
    #ix = np.random.randint(0,P_mats.shape[1])
    #for P in P_mats[:,ix,:,:]:
    #  print P
    data = generate_sim_robot_data(proj_mats, n_robot_obs=50, n_table_obs=100)
    good_data = vision.good_calib_data(data['obs_2d'], data['full_3d'])
    obs2d = np.array(good_data['obs2d'])
    dists_mcmc = []
    dists_init = []
    for ix, pt3d in enumerate(good_data['obs3d']):
        samples3d = mc_stereo_vision(P_mats, obs2d[:,ix].tolist(), n_samples=25)
        X = np.array(samples3d)
        mu = np.mean(X, axis=0)
        cov = np.cov(X, rowvar=0)
        init_est = mc_stereo_vision(init_mats, obs2d[:,ix].tolist())
        diff_mcmc = pt3d - mu
        if np.linalg.norm(diff_mcmc)>0.09:
            print "real: ", pt3d
            print "est. mean: ", mu
            print "est. init: ", init_est
            print "est. std: ", np.sqrt(np.diag(cov))
            print "dist: ", np.linalg.norm(diff_mcmc), "\n"
        dists_mcmc.append(np.linalg.norm(diff_mcmc))
        diff_init = pt3d - np.array(init_est[0])
        dists_init.append(np.linalg.norm(diff_init))
        #print "dist: ", np.linalg.norm(diff)
        #print "est. cov: ", cov
        #print "est. std: ", np.sqrt(np.diag(cov))
    print "Avg. dist mcmc: ", np.mean(dists_mcmc)
    print "Avg. dist linear: ", np.mean(dists_init)


data_fname = '/local_data/sgomez/calibration/test_calib_data.json'
#test_calib(data_fname=data_fname)
#test_calib_bayesian(fname='/local_data/sgomez/calibration/test_calib_samples.json',
#    stan_compiled='/local_data/sgomez/calibration/stan_compiled.out', data_fname=data_fname)
analyse_calib('/local_data/sgomez/calibration/test_calib_samples_1.json', data_fname=data_fname)
