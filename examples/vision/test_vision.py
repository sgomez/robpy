"""Script to plot and analyze recorded vision data

This script receives as parameter a PB file with the recorded
data and has the ability to present different plots or scores. 
"""

import robpy.slpp as slpp
import robpy.kinematics.forward as fwd_kin
import robpy.time_series as time_series
import robpy.protos.utils as pb_util
import frame_pb2 as pb_frame

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import argparse
import os
import time
import cv2

def main(args):
    pbr = pb_util.PBFileReader(args.file)
    frm = pb_frame.Frame()
    max_num = -1
    while pbr.next(frm):
        im_data = np.frombuffer(frm.img.data, dtype=np.uint8)
        im = np.reshape(im_data, (frm.img.height, frm.img.width, frm.img.channels))
        if frm.num < max_num:
            print("Caution. Images are not stored in order")
        if frm.num > max_num:
            max_num = frm.num
            if cv2.waitKey(args.delay) & 0xFF == ord('q'):
                break
        cv2.putText(im, "Im: {}".format(frm.num), (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255), 2, cv2.LINE_AA)
        cv2.imshow('cam{}'.format(frm.cam_id), im)
    cv2.destroyAllWindows()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    rel_path = lambda fname: os.path.join(os.path.dirname(__file__), fname)
    parser.add_argument('file', help="File name where the vision data is stored")
    parser.add_argument('--delay', help="Delay between pictures", type=int, default=20)
    parser.add_argument('--obs2d', nargs='+', type=int, help="Plot the 2d ball observations passed as parameter w.r.t time")
    parser.add_argument('--proj3d', nargs='+', type=int, help="Given indices of 3D observations, plot each of the axis w.r.t time")
    parser.add_argument('--task_traj', nargs='+', type=int, 
            help="Plot the end effector trajectory along with the observations of the camera passed as parameter")
    parser.add_argument('--joints', nargs='+', type=int, help="Indices of the joints to plot")
    parser.add_argument('--time', type=float, help="Limit time span to at most the given number of seconds")
    parser.add_argument('--racket_dists', type=int, default=range(3), nargs='+', help="Indices of the racket distributions to plot")
#  parser.add_argument('--host', default='localhost', help="Name of the host to connect to")
    args = parser.parse_args()
    main(args)
