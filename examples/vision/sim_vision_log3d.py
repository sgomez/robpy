"""Loads a 3D vision log acts like the vision server with that data

"""

import zmq
import robpy.protos.ball_vision_pb2 as bvpb
import robpy.protos.slpp_pb2 as slpp_pb2
import robpy.protos.utils as utils
import struct
import robpy.time_series as time_series

import numpy as np
import argparse
import os
import json
import time

def load_obs_3d(f_name):
    rd = utils.PBFileReader(f_name)
    ans = []
    while True:
        reg = bvpb.obs3d()
        if not rd.next(reg): break
        ans.append(reg)
    return ans

def main(args):
    context = zmq.Context()
    socket = context.socket(zmq.PUB)
    socket.bind("tcp://*:7660")

    rd = utils.PBFileReader(args.log3d)
    while True:
        reg = bvpb.obs3d()
        if not rd.next(reg): break
        msg = {'time': reg.abs_time, 'obs': list(reg.obs) if len(reg.obs)==3 else None, 'num': reg.num}
        socket.send(json.dumps(msg))
        if args.delay:
            time.sleep(args.delay / 1000.0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('log3d', help="Path to the vision log 3d file")
    parser.add_argument('--delay', type=float, help="Number of milliseconds to wait between ball obs")
    args = parser.parse_args()
    main(args)
