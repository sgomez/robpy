
import robpy.slpp as slpp
import numpy as np
import matplotlib.pyplot as plt
import robpy.table_tennis.ball_model as ball_model
import sys
import time
import json
import argparse
import os

def configure_table_tennis(client, args):
    '''
    Sends the parameters required by the table tennis task via JSON
    '''
    methods = [{'file': args.ballmodel, 'method': 'table_tennis/load_ball_model'}, \
      {'file': args.ballfilter, 'method': 'table_tennis/load_ball_filter'}, \
      {'file': args.barrettkin, 'method': 'table_tennis/load_barrett_kin'}, \
      {'file': args.promp, 'method': 'table_tennis/load_prior_promp'}]
    ans = []
    for method in methods:
        f = open(method['file'], 'r')
        body = json.load(f)
        print "Configuring on ", method['method']
        ans.append( client.query(method['method'], body) )
    return ans

def get_sim_data(sensor_id, obs, times):
    N = len(obs)
    ans = []
    for i in xrange(N):
        ans.append({'sensor_id': sensor_id, 'time': times[i], 'obs': obs[i].tolist()})
    return ans

def send_sim_ball_traj(client, kf, deltaT, args):
    #ball_obs = table_tennis.sample_ball_traj_obs(ball_model, args.trajlen, args.deltaT)
    #print kf.mu0
    #print np.sqrt(np.diag(kf.P0))
    ball_obs, hidden = kf.generate_sample(args.trajlen, actions=np.ones((args.trajlen,1)))
    #print hidden[0]
    sim_times = np.array(range(1,args.trajlen+1)) * deltaT
    data = get_sim_data(args.sensorid, ball_obs, sim_times)
    client.query('obs/add_sim_obs', data)


def main(args):
    url = "tcp://{0}:{1}".format(args.host, args.port) if not args.url else args.url
    print "Connecting to ", url
    client = slpp.SLPP( url=url )

    configure_table_tennis(client, args)
    if args.start:
        rtime = client.query('table_tennis/start',"")
        print "Table tennis local task time: ", rtime
        time.sleep(1.0)
    client.clear_joint_obs()
    client.clear_sensor_obs()

    kf, deltaT = ball_model.load_ball_model(args.simball)
    send_sim_ball_traj(client, kf, deltaT, args)

    time.sleep(deltaT * args.trajlen + 0.5)
    ball_obs = client.query('obs/flush',{'ids': [args.sensorid]})
    joint_obs = client.get_joint_obs()
    log = client.query('table_tennis/get_log',{})
    data = {'obs': ball_obs, 'joints': joint_obs, 'log': log}
    f_out = open(args.out, "w")
    json.dump(data, f_out)
    f_out.close()
    if args.stop:
        rtime = client.query('table_tennis/stop',"")
        print "Table tennis stopped at t=", rtime


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Record ball and joint information in JSON format')
    parser.add_argument('--host', default='localhost', help="Name of the host to connect to")
    parser.add_argument('--port', default=7647, help="Port of the host to connect to")
    parser.add_argument('--url', help="Overrides the host and port setting directly a ZMQ bind URL")
    parser.add_argument('--out', default='saved.out', help="File name where the data is stored")
    parser.add_argument('--sensorid', default=1, help="Id of the sensor that provides the ball observations")
    parser.add_argument('--trajlen', default=60, type=int, help="Ball trajectory length")
    parser.add_argument('--start', action='store_true', help="Start the table tennis task server")
    parser.add_argument('--stop', action='store_true', help="Stop the table tennis task server")

    rel_path = lambda fname: os.path.join(os.path.dirname(__file__), fname)
    parser.add_argument('--ballmodel', default = rel_path('../config/ball_model.json'), \
            help="File name where the ball model is stored")
    parser.add_argument('--ballfilter', default = rel_path('../config/ball_filter.json'), \
            help="File name where the ball filter configuration is stored")
    parser.add_argument('--barrettkin', default = rel_path('../config/barrett_kin.json'), \
            help="File name where the Barrett forward kinematics configuration is stored")
    parser.add_argument('--promp', default = rel_path('../config/indep_promp.json'), \
            help="File name where the prior ProMP for a hitting movement is stored")
    parser.add_argument('--simball', default = rel_path('../config/sim_ball_model.json'), \
            help = "File name where the simulation ball model is stored")

    args = parser.parse_args()
    main(args)
