"""Script to create a ball trajectory dataset from a consolidated raw file

This script receives as parameter a JSON file with the consolidated recorded
data and optional parameters about decired processing steps and produce a simple
ball trajectory file.
"""

import robpy.slpp as slpp
import robpy.table_tennis.load_data as load_data
import numpy as np
import sys
import time
import argparse
import os
import json

def main(args):
    with open(args.in_file, 'r') as f:
        if args.in_file.endswith('json'):
            trial_data = json.load(f)
        else:
            trial_data = np.load(f)
        ds = load_data.PlayDatasetBuilder()
        ds.data = trial_data['raw']
    #X, Xavg, U, Hidden, Times = load_data.fw_ball_traj(trial_data)
    Times, X = ds.fw_ball_traj()
    print("{} ball trajectories extracted".format(len(Times)))
    #stream = {'X': X, 'Xavg': Xavg, 'Hidden': Hidden, 'Times': Times}
    stream = {'X': X, 'Times': Times}
    with open(args.out_file, 'w') as f:
        if args.out_file.endswith('json'):
            json.dump(stream, f)
        elif args.out_file.endswith('npz'):
            np.savez(f, **stream)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('in_file', help="File where the consolidated information is stored")
    parser.add_argument('out_file', help="File where the ball processed information is stored")
    parser.add_argument('--outliers', help="Plot the prior ball to racket overlap", action="store_true")
    args = parser.parse_args()
    main(args)
