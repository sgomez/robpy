"""Record Robot Table Tennis Data

This script uses a simple interface to record data in JSON format from
the robot and the cameras. The user can specify the host name and TCP port
of the machine where the SL++ task is running, and the folder where the
data should be stored. By default this script connects to localhost and
the output folder is the current folder on the terminal where the script
was called.

Once the program starts running, it will show a screen with a circle. If
the color is red, the program is not recording data. If it is green then 
it is recording. This program records the data initially in the RAM memory.
Therefore, it is advisable not to record very long data sequences. The
data is flushed to a JSON file when a recording is stoped or when it is
restarted. The trajectories are stored in the path given by the user with
a time stamp corresponding to the moment when the data was saved.

* To start recording press the key 's'
* To end a recording press the key 'e'
* To end the current recording and start a new recording right away press 'l'
* To quit the program either press 'q' or close the window

It is also possible to start recording by pressing (and holding) the
PgUp or PgDn keys and releasing the pressed key to stop recording.

"""

import robpy.slpp as slpp
import numpy as np
import pygame
import sys
import time
import argparse



def redraw(screen, record):
    colors = {'white': (255,255,255), 'black': (0,0,0), 'red': (255,0,0), 
            'green': (0,255,0), 'blue': (0,0,255), 'yellow': (255,255,0)}
    screen.fill(colors['white'])
    
    scolor = colors['green'] if record.is_recording else colors['red']
    pygame.draw.circle(screen, scolor, (200,200), 150)

def main(args):
    url = "tcp://{0}:{1}".format(args.host, args.port)
    client = slpp.SLPP( url=url )
    
    pygame.init()
    screen = pygame.display.set_mode((400,400))
    running = True
    record = slpp.Recorder(client, sensor_ids=[0,1,2,10,11,12,13], path = args.path, no_joints = args.no_joints)
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_s:
                    record.start_recording()
                elif event.key == pygame.K_e:
                    record.stop_recording()
                elif event.key == pygame.K_l:
                    record.segment_recording()
                elif event.key == pygame.K_q:
                    running = False
                elif event.key == pygame.K_PAGEUP or event.key == pygame.K_PAGEDOWN:
                    record.start_recording()
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_PAGEUP or event.key == pygame.K_PAGEDOWN:
                    record.stop_recording()
        record.update()
        redraw(screen, record)
        pygame.display.flip()
        pygame.time.wait(20)
    record.stop_recording()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--host', default='localhost', help="Name of the host to connect to")
    parser.add_argument('--port', default=7648, help="Port of the host to connect to")
    parser.add_argument('--path', default='./', help="Folder where the data will be stored")
    parser.add_argument('--no_joints', action='store_true', help="Use this when the joint measures should not be recorded")
    args = parser.parse_args()
    main(args)
