
import robpy.protos.ball_vision_pb2 as bvpb
import robpy.protos.slpp_pb2 as slpp_pb2
import robpy.protos.utils as utils
import struct
import threading
import zmq
import argparse
import json
import time

class Log2d(threading.Thread):

    def __init__(self, logger, urls):
        super(Log2d,self).__init__()
        self._urls = urls
        self._logger = logger
        self._stop_event = threading.Event()

    def stop(self):
        self._stop_event.set()

    def run(self):
        context = zmq.Context()
        socket = context.socket(zmq.SUB)
        for url in self._urls: 
            socket.connect(url)
        topicfilter = ""
        socket.setsockopt(zmq.SUBSCRIBE, topicfilter)
        while (not self._stop_event.is_set()):
            msg = json.loads(socket.recv())
            obs2d = bvpb.obs2d()
            if msg['obs'] is not None:
                for x in msg['obs']:
                    obs2d.obs.append(x)
            obs2d.cam_id = msg['cam_id']
            obs2d.num = msg['num']
            if 'time' in msg: obs2d.abs_time = msg['time']
            obs2d.time = time.time()
            self._logger.append(obs2d)

class Log3d(threading.Thread):

    def __init__(self, logger, urls):
        super(Log3d,self).__init__()
        self._urls = urls
        self._logger = logger
        self._stop_event = threading.Event()

    def stop(self):
        self._stop_event.set()

    def run(self):
        context = zmq.Context()
        socket = context.socket(zmq.SUB)
        for url in self._urls: 
            socket.connect(url)
        topicfilter = ""
        socket.setsockopt(zmq.SUBSCRIBE, topicfilter)
        while (not self._stop_event.is_set()):
            msg = json.loads(socket.recv())
            obs = bvpb.obs3d()
            if msg['obs'] is not None:
                for x in msg['obs']:
                    obs.obs.append(x)
            obs.num = msg['num']
            if 'time' in msg: obs.abs_time = msg['time']
            obs.time = time.time()
            self._logger.append(obs)

class LogJoints(threading.Thread):

    def __init__(self, logger, urls):
        super(LogJoints,self).__init__()
        self._urls = urls
        self._logger = logger
        self._stop_event = threading.Event()

    def stop(self):
        self._stop_event.set()

    def run(self):
        context = zmq.Context()
        socket = context.socket(zmq.SUB)
        for url in self._urls: 
            socket.connect(url)
        topicfilter = ""
        socket.setsockopt(zmq.SUBSCRIBE, topicfilter)
        while (not self._stop_event.is_set()):
            msg = socket.recv()
            obs = slpp_pb2.JointObs()
            obs.ParseFromString(msg)
            obs.time = time.time()
            self._logger.append(obs)

def main(args):
    l2d = Log2d(utils.PBFileWriter("/tmp/obs2d.pb"), urls=[args.v2d])
    if ((args.mask & 1)): l2d.start()
    l3d = Log3d(utils.PBFileWriter("/tmp/obs3d.pb"), urls=[args.v3d])
    if ((args.mask & 2)): l3d.start()
    joints = LogJoints(utils.PBFileWriter("/tmp/joints.pb"), urls=[args.joints])
    if ((args.mask & 4)): joints.start()
    while True:
        x = raw_input("Press q to quit: ")
        if x == 'q': break
    l2d.stop()     
    l3d.stop()
    joints.stop()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--v3d', default='tcp://helbe:7660', help="Host to connect to for 3D ball positions")
    parser.add_argument('--v2d', default='tcp://helbe:7650', help="Host for 2D ball positions")
    parser.add_argument('--joints', default='tcp://lein:7641', help="Host for joint positions")
    parser.add_argument('--mask', type=int, default=7, help="Binary mask (joints|v3d|v2d) specifying what to connect to")
    args = parser.parse_args()
    main(args)
