
import robpy.kalman as kalman
import robpy.table_tennis.ball_model as ball_model
import robpy.table_tennis.load_data as loader
import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
import json

def obs_unfold(time_n, Xn):
    t_ans = []
    x_ans = []
    for ix, t in enumerate(time_n):
        for obs in Xn[ix]:
            t_ans.append(t)
            x_ans.append(obs)
    return np.array(t_ans), np.array(x_ans)

def plot_ball_E_step(em_obj, X, U, means, covs, inst_id, time):
    positions = X[inst_id]
    Tn = len(positions)
    D = np.shape(em_obj.Sigma)[0]
    time_obs, x_obs = obs_unfold(time, positions)
    print x_obs.shape
    filter_means = np.zeros((Tn,D))
    filter_dev = np.zeros((Tn,D))
    noise_dev = np.zeros((Tn,D))
    axis = "XYZ"
    for t in xrange(Tn):
        for d in xrange(D):
            filter_means[t][d] = means[inst_id][t][2*d]
            filter_dev[t][d] = np.sqrt(covs[inst_id][t][2*d,2*d])
            noise_dev[t][d] = np.sqrt(em_obj.Sigma[d,d])
    plt.figure(1)
    for d in xrange(D):
        plt.subplot(D,1,d+1)
        plt.plot(time_obs,x_obs[:,d],'g.')
        plt.plot(time,filter_means[:,d],'r')
        plt.fill_between(time, filter_means[:,d] - noise_dev[:,d], filter_means[:,d] + noise_dev[:,d], facecolor='gray')
        plt.fill_between(time, filter_means[:,d] - filter_dev[:,d], filter_means[:,d] + filter_dev[:,d], facecolor='blue')
        plt.ylabel(axis[d])
    plt.title('Instance id={0}'.format(inst_id))
    plt.xlabel('Time')
    plt.show()

def E_step_callback(emobj, X, U, means, covs, do_plot = True):
    inst_id = np.random.randint(0, high=len(X))
    if (do_plot):
        plot_ball_E_step(emobj,X,U,means,covs, inst_id, 0.016*np.array(range(len(X[inst_id]))) ) 
    emobj.B.bounceFac = emobj.A.params[5]

def compIsBounce(X):
    isBounce = []
    for id_x, x in enumerate(X):
        ix = np.argmin(x[:,2])
        is_bounce = [i == (ix+1) for i in xrange(len(x)+1)]
        isBounce.append(is_bounce)
    return isBounce

def train_kalman(fw_ball_traj, **params):
    params.setdefault('model_fname', '/tmp/ball_model.json')
    params.setdefault('max_iter', 10)
    params.setdefault('optimize_A', True)
    params.setdefault('print_lowerbound', True)
    params.setdefault('debug_LB', True)
    params.setdefault('check_grad', False)
    params.setdefault('a_mat', 'linear')
    params.setdefault('prior_P0', kalman.get_cov_prior([0.5,1,1,1,0.5,1], 100))
    params.setdefault('prior_Sigma', kalman.get_cov_prior([0.01,0.01,0.01], 20))
    params.setdefault('prior_Gamma', kalman.get_cov_prior([1e-3 for i in xrange(6)], 6))
    X, Xavg, U, Hidden, Times = fw_ball_traj
    #n = 1
    #X = X[0:n]
    #U = U[0:n]
    #Hidden = Hidden[0:n]
    #Times = Times[0:n]
    isBounce = compIsBounce(Xavg)
    if os.path.isfile(params['model_fname']):
        f = file(params['model_fname'],'r')
        data = json.load(f)
        model = data["ball_model"]
        beta = model["airDrag"]
        bounceFac = model["bounceFac"]
        deltaT = model['deltaT']
        params.setdefault('mu0', np.array(model['mu_0']))
        params.setdefault('P0', np.array(model['P0']))
        params.setdefault('init_Sigma', np.array(model['fly_obs_noise']))
        params.setdefault('init_Gamma', np.array(model['fly_trans_noise']))
    else:
        params.setdefault('mu0', np.zeros(6))
        params.setdefault('P0', np.eye(6))
        params.setdefault('init_Sigma', 4e-4*np.eye(3))
        params.setdefault('init_Gamma', 1e-3*np.eye(6))
        beta = [0.9,0.9,0.2] #air drag
        bounceFac = [0.6,0.6,0.6] #bounce factors
        #airDrag = 0.14
        #deltaT = 0.016
        deltaT = 1.0 / 180.0
    bounceZ = -0.85 #-0.99 + 0.02
    A = ball_model.Ball_A_Mat(beta, bounceFac, deltaT, bounceZ)
    A.optional['isBounce'] = isBounce
    #A = ball_model.Yanlong_A_Mat(airDrag, bounceFac, deltaT, bounceZ)
    B = ball_model.Ball_B_Mat(deltaT, bounceZ, bounceFac[2])
    C = kalman.ConstMat(np.array([[1,0,0,0,0,0],[0,0,1,0,0,0],[0,0,0,0,1,0]]))
    kf = kalman.LinearKF(A, C, B=B)
    init_vals = {'init_state_mean': np.zeros(6), 'init_state_cov': np.eye(6)*10,
                'init_Sigma': params['init_Sigma'], 'init_Gamma': params['init_Gamma']}
    params.update(init_vals)
    my_callback = lambda emobj, X, U, means, covs: E_step_callback(emobj, X, U, means, covs, params['do_plot'])
    kf.EM_train(X, U, Abounds=[(0,10),(0,10),(0,10),(0,1),(0,1),(0,1)], \
        hidden = Hidden, callback_after_E_step = my_callback, Time=Times, **params)
    print "mu0=", kf.mu0
    print "P0=", kf.P0
    print "Init state stdev = ", np.sqrt(np.diag(kf.P0))
    print "Sigma=", kf.Sigma
    print "Noise stdev=", np.sqrt(np.diag(kf.Sigma))
    print "Gamma=", kf.Gamma
    print "Model Stdev=", np.sqrt(np.diag(kf.Gamma))
    A_theta = A.params
    print "air_drag=", A_theta[0:3]
    print "bouncing=", A_theta[3:6]
    ball_model.save_ball_model(params['model_fname'], deltaT, kf, A, **params)


def main(args):
    l = loader.DatasetBuilder()
    if not args.cons:
        assert(len(args.data) == len(args.meta))
        for i, d in enumerate(args.data):
            meta = args.meta[i]
            if args.path:
                meta = os.path.join(args.path, meta)
                d = os.path.join(args.path, d)
            l.load_json(d, meta=meta)
        fw_ball_traj = l.fw_ball_traj()
    else:
        with open(args.cons,'r') as f:
            if args.cons.endswith('json'):
                fw_ball_traj = loader.fw_ball_traj( json.load(f) )
            elif args.cons.endswith('npz'):
                data = np.load(f)
                fw_ball_traj = (data['X'], data['Xavg'], data['U'], data['Hidden'], data['Times'])
            
    opt_pars = {'optimize_A': args.opt_A, 'debug_LB': args.debug_lb, 'do_plot': args.plot_e}
    if args.iter: opt_pars['max_iter'] = args.iter
    if args.model: opt_pars['model_fname'] = args.model
    train_kalman(fw_ball_traj, **opt_pars)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--path', default='localhost', help="Path where training files are stored")
    parser.add_argument('--data', nargs="+", help="List of data files")
    parser.add_argument('--meta', nargs="+", help="List of segmentation meta data files")
    parser.add_argument('--cons', help="Path to file with consolidated data")
    parser.add_argument('--opt_A', action="store_true", help="Should the parameters of A be optimized")
    parser.add_argument('--iter', type=int, help="Number of iteration of the training algorithm")
    parser.add_argument('--model', help="File where the model should be stored to (or read from)")
    parser.add_argument('--debug_lb', action="store_true", help="Verbosity for EM lower bound")
    parser.add_argument('--plot_e', action="store_true", help="Plot the E-step of every iteration of EM")
    args = parser.parse_args()
    main(args)
