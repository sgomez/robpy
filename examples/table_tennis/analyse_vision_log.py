"""Script to plot and analyze recorded vision data

This script receives as parameter the files with the recorded
data and has the ability to present different plots or scores. As
you may want to use this script for different purposes, you must
pass on different parameters to decide which kind of plot or analysis
you want.
"""

import robpy.protos.ball_vision_pb2 as bvpb
import robpy.protos.slpp_pb2 as slpp_pb2
import robpy.protos.utils as utils
import struct
import robpy.time_series as time_series

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import argparse
import os
import json
import time

def load_obs_2d(f_name):
    rd = utils.PBFileReader(f_name)
    ans = []
    while True:
        reg = bvpb.obs2d()
        if not rd.next(reg): break
        ans.append(reg)
    return ans

def load_obs_3d(f_name):
    rd = utils.PBFileReader(f_name)
    ans = []
    while True:
        reg = bvpb.obs3d()
        if not rd.next(reg): break
        ans.append(reg)
    return ans

def load_joint_obs(f_name):
    rd = utils.PBFileReader(f_name)
    ans = []
    while True:
        reg = slpp_pb2.JointObs()
        if not rd.next(reg): break
        ans.append(reg)
    return ans

def reproject(obs3d, calib):
    h3d = list(obs3d)
    h3d.append(1)
    h2d = np.dot(calib, h3d)
    return [h2d[0] / h2d[2], h2d[1] / h2d[2]]

def plot2d(obs2d, cam_id=0, obs3d=None, calib=None):
    X = np.array([x.obs for x in obs2d if x.cam_id==cam_id and len(x.obs)==2])
    if obs3d is not None and calib is not None:
        Xpr = np.array([reproject(x.obs, calib) for x in obs3d if len(x.obs)==3])
    else:
        Xpr = None
    print(X.shape)
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111, aspect='equal')
    if len(X) > 0: ax1.plot(X[:,0], X[:,1], 'ro')
    if Xpr is not None and len(Xpr)>0: ax1.plot(Xpr[:,0],Xpr[:,1],'bx')
    ax1.add_patch( patches.Rectangle((0,0),640,480,fill=False) )
    plt.ylim( (-100,600) )
    plt.xlim( (-200,800) )
    plt.gca().invert_yaxis()
    plt.show()

def plot3d(obs3d):
    #t_dist, x_dist = time_series.distance(tr, xr, tb, xb)
    #dist_summ = time_series.summary_stats(t_dist,x_dist)
    X = np.array([x.obs for x in obs3d if len(x.obs)==3])
    if len(X) > 0:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ball_colors = "rgbykmc"
        ax.scatter(X[:,0], X[:,1], zs=X[:,2], c='r')
        ax.set_title('Ball Trajectory')
        plt.show()

def time_sync(obs3d, joints):
    t_vision = np.array([x.abs_time for x in obs3d]) / 1e9
    tv_local = np.array([x.time for x in obs3d])
    t_joints = np.array([x.abs_time for x in joints]) / 1e9
    tj_local = np.array([x.time for x in joints])
    diff_vt = tv_local - t_vision
    diff_jt = tj_local - t_joints
    print("Time difference: Obs {} +/- {}. Joints {} +/- {}".format(np.mean(diff_vt), 
        np.std(diff_vt), np.mean(diff_jt), np.std(diff_jt)))

def main(args):
    cfile = args.conf if args.conf else os.path.join(os.getenv("ROBPY_CONF",""),"analyse_vision_log.json")
    if not os.path.isfile(cfile):
        print("Error: A valid configuration file must be passed as argument or exist in a default folder."
                " To set the default folder use the ROBPY_CONF environment variable. File name: {}".format(cfile))
        return
    conf = json.load(file(cfile,'r'))
    obs2d = load_obs_2d(conf['obs2d'])
    obs3d = load_obs_3d(conf['obs3d'])
    #joints = load_joint_obs(conf['joints'])
    #time_sync(obs3d, joints)
    calib = {x["ID"]: x["val"] for x in conf['stereo']['calib']}
    for i in range(4): plot2d(obs2d,cam_id=i,obs3d=obs3d,calib=calib[i])
    plot3d(obs3d)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--conf', help="Path to a JSON config file")
    args = parser.parse_args()
    main(args)
