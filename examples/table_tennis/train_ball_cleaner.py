
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import random
import robpy.table_tennis.load_data as load_data
import argparse
import tensorflow as tf

def time_to_dt(sdata):
    for inst in sdata:
        X = inst['ball']
        for i in reversed(range(1,len(X))): X[i,3] = X[i,3] - X[i-1,3]
        X[0,3] = 0.0

def dt_to_time(sdata, t_offset=0.0):
    for inst in sdata:
        X = inst['ball']
        X[0,3] = t_offset
        for i in range(1,len(X)): X[i,3] = X[i,3] + X[i-1,3]

def predict_cleaner_model(sdata, outputs, model):
    model_pred = []
    for i in range(len(sdata)):
        curr_pred = {'ball': sdata[i]['ball']}
        feed_dict = {model['input']: [sdata[i]['ball']]}
        for k in outputs:
            kpred = model['pred_{}'.format(k)].eval(feed_dict=feed_dict)
            curr_pred[k] = np.reshape(kpred, [-1])
        #print(len(sdata[i]['ball']), len(sdata[i]['ball_direction']))
        model_pred.append(curr_pred)
    return model_pred

def augment_training_data(sdata, outlier_prob=0.01, ntimes=10):
    N = len(sdata)
    for i in range(N):
        inst = sdata[i]
        X = inst['ball']
        T = len(X)
        for j in range(ntimes):
            noise = np.random.uniform(low=-2,high=2,size=(T,3))
            outliers = np.random.binomial(n=1,p=outlier_prob,size=(T,1))
            tmp = np.linalg.norm(outliers,axis=1) > 0.01
            outliers = outliers*np.reshape(tmp,[-1,1])
            noise = noise*outliers
            add_noise = np.concatenate((noise, np.zeros((T,1))), axis=1)
            nX = X + add_noise
            new_inst = inst.copy()
            new_inst['ball'] = nX
            is_reliable = (1 - np.reshape(outliers,[-1])).astype(np.int32)
            old_reliable = np.array(inst['ball_reliable']).astype(np.int32)
            new_inst['ball_reliable'] = np.bitwise_and(old_reliable, is_reliable)
            sdata.append(new_inst)

def train_cleaner_model(sdata, outputs = {'ball_cut': 2, 'ball_direction': 3, 'ball_bounce': 2, 'ball_reliable': 2},
        class_weight = {'ball_cut': [0.1,0.9], 'ball_bounce': [0.1,0.9]}):
    tf.reset_default_graph()
    x = tf.placeholder(tf.float32, shape=[None,None,4])
    model = {}
    tf.add_to_collection("input",x)
    model["input"] = x

    conv1 = tf.layers.conv1d(inputs=x,
            filters=16,
            kernel_size=3,
            padding="same",
            activation=tf.nn.tanh,
            name="conv1")
    conv2 = tf.layers.conv1d(inputs=conv1,
            filters=16,
            kernel_size=3,
            activation=tf.nn.tanh,
            dilation_rate=2,
            padding="same",
            name="conv2")
    conv3 = tf.layers.conv1d(inputs=conv2,
            filters=16,
            kernel_size=3,
            activation = tf.nn.tanh,
            dilation_rate=4,
            padding="same",
            name="conv3")
    conv4 = tf.layers.conv1d(inputs=conv3,
            filters=16,
            kernel_size=3,
            activation = tf.nn.tanh,
            dilation_rate=8,
            padding="same",
            name="conv4")
    conv5 = tf.layers.conv1d(inputs=conv3,
            filters=16,
            kernel_size=3,
            activation = tf.nn.tanh,
            dilation_rate=16,
            padding="same",
            name="conv5")

    features = conv5

    for k in outputs:
        logname = "logit_{}".format(k)
        predname = "pred_{}".format(k)
        probname = "prob_{}".format(k)
        lossname = "loss_{}".format(k)
        outname = "out_{}".format(k)
        curr_out = tf.placeholder(tf.int32, shape=[None, None], name=outname)
        flat_curr_out = tf.reshape(curr_out, shape=[-1])
        logits = tf.layers.conv1d(inputs = features,
                filters = outputs[k],
                kernel_size = 3,
                padding = "same",
                name = logname)
        flat_logits = tf.reshape(logits, [-1,outputs[k]])
        one_hot_labels = tf.one_hot(indices=tf.cast(flat_curr_out, tf.int32), depth=outputs[k])
        if k in class_weight:
            npw = np.reshape(class_weight[k],[outputs[k],1])
            cw = tf.constant(npw, dtype=tf.float32)
            weights = tf.reshape(tf.matmul(one_hot_labels , cw), [-1])
        else:
            weights = 1.0
        pred = tf.argmax(input=logits, axis=2)
        prob = tf.nn.softmax(logits, name=probname)
        loss = tf.losses.softmax_cross_entropy(onehot_labels=one_hot_labels, logits=flat_logits, weights=weights)
        layers = {logname: logits, predname: pred, probname: prob, lossname: loss, outname: curr_out}
        for lname in layers:
            tf.add_to_collection(lname, layers[lname])
            model[lname] = layers[lname]
        tf.losses.add_loss(loss)
    total_loss = tf.losses.get_total_loss(name="total_loss")
    tf.add_to_collection("total_loss", total_loss)
    model["total_loss"] = total_loss
    train_step = tf.train.AdamOptimizer(1e-3).minimize(total_loss)

    saver = tf.train.Saver()
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        for opt_iter in range(100000):
            ixs = [opt_iter%len(sdata)]
            feed_dict = {x: [sdata[i]['ball'] for i in ixs]}
            for k in outputs:
                feed_dict[model["out_{}".format(k)]] = [sdata[i][k] for i in ixs]
            if (opt_iter%200) == 0:
                print(opt_iter)
                l = total_loss.eval(feed_dict=feed_dict)
                mp = predict_cleaner_model(sdata,outputs,model)
                #load_data.plot_time_cleaner_instance(mp[0])
                #load_data.plot_time_cleaner_instance(sdata[0])
                print("loss: {}".format(l))
                derr = []
                for j in range(len(sdata)):
                    dir_err = mp[j]["ball_direction"] - np.array(sdata[j]["ball_direction"])
                    derr.append(sum(abs(dir_err)))
                print("dir_err: {}".format(derr))
                saver.save(sess, '/tmp/trained_cleaner/model', global_step=opt_iter)
            train_step.run(feed_dict=feed_dict)
        saver.save(sess, '/tmp/trained_cleaner/model')
        model_pred = []
        for i in range(len(sdata)):
            curr_pred = {'ball': sdata[i]['ball']}
            feed_dict = {x: [sdata[i]['ball']]}
            for k in outputs:
                kpred = model['pred_{}'.format(k)].eval(feed_dict=feed_dict)
                print k, kpred.shape
                curr_pred[k] = np.reshape(kpred, [-1])
            model_pred.append(curr_pred)
            dt_to_time([curr_pred])
            load_data.plot_time_cleaner_instance(curr_pred)
    return model

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('segm_data', nargs='+', help='file where the segmented data is stored')
    parser.add_argument('--out_file', help="Path to an npz file to contain the consolidated data", default="out.npz")
    args = parser.parse_args()

    sdata = []
    for sfile in args.segm_data:
        data = load_data.load_cleaner_dataset(sfile)
        tmp_data = load_data.split_time_cleaner_dataset(data, split_time=0.08, min_obs_traj=180)
        sdata.extend(tmp_data)
    augment_training_data(sdata,ntimes=5,outlier_prob=0.05)
    time_to_dt(sdata)
    model = train_cleaner_model(sdata)
    dt_to_time(sdata)
    #for inst in sdata:
    #    load_data.plot_time_cleaner_instance(inst)
