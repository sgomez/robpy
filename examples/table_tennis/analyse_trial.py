"""Script to plot and analyze recorded data

This script receives as parameter a JSON file with the recorded
data and has the ability to present different plots or scores. As
you may want to use this script for different purposes, you must
pass on different parameters to decide which kind of plot or analysis
you want.

The JSON file is expected to be a dictionary with at least a "joints" and "obs" 
field representing the joints and cameras observations respectively.
"""

import robpy.slpp as slpp
import robpy.kinematics.forward as fwd_kin
import robpy.time_series as time_series

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import argparse
import os
import json
import time
import bisect

def decode_racket_dist(racket_dist):
    t = map(lambda x: x['t'], racket_dist)
    x = map(lambda x: x['mean'], racket_dist)
    covs = map(lambda x: np.reshape(np.array(x['cov']), (3,3)), racket_dist)
    devs = map(lambda x: np.sqrt(np.diag(x)), covs)
    return np.array(t), np.array(x), np.array(devs)

def time_trim(lists, **params):
    if 'tf' in params:
        final_ix = bisect.bisect_right(lists[0], params['tf'])
    else:
        final_ix = len(lists[0])
    if 't0' in params:
        start_ix = bisect.bisect_left(lists[0], params['t0'])
    else:
        start_ix = 0
    ans = []
    for l in lists:
        ans.append(l[start_ix:final_ix])
    return ans

def plot_ball_and_rob(tr, xr, tb, xb, logs, args):
    #t_dist, x_dist = time_series.distance(tr, xr, tb, xb)
    #dist_summ = time_series.summary_stats(t_dist,x_dist)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ball_colors = "rgbykmc"
    t0 = 1e100
    for i,ix in enumerate(args.task_traj):
        ax.scatter(xb[ix][:,0], xb[ix][:,1], zs=xb[ix][:,2], c=ball_colors[i])
        t0 = min(t0, tb[ix][0])
    if args.time:
        tf = t0 + args.time
    if xr is not None and np.shape(xr)[0]>0:
        if args.time:
            tr, xr = time_trim((tr, xr), t0=t0, tf=tf)
        ax.plot(xr[:,0], xr[:,1], zs=xr[:,2], c='r')
    for log in logs:
        if "predict_ball_traj" in log:
            tpb = np.array(log['predict_ball_traj']['times'])
            xpb = np.array(log['predict_ball_traj']['means'])
            if args.time:
                tpb, xpb = time_trim((tpb,xpb), tf=tf)
            ax.plot(xpb[:,0], xpb[:,1], zs=xpb[:,2], c='g')
    ax.set_title('Ball and Racket Trajectory')
    plt.show()

def plot_overlap(log, args):
    deltaT = 1.0 / 60.0
    t0 = log['t0']
    x = log['log_overlap']
    x = [elem for elem in x if elem>-1e99]
    t = t0 + deltaT*np.array(range(len(x)))
    plt.plot(t, x)
    plt.xlabel('time')
    plt.ylabel('overlap')
    plt.show()

def plot_obs_2d(tb, xb, indices):
    for ix in indices:
        fig = plt.figure()
        for i in xrange(2):
            ax = fig.add_subplot(3,1,i+1)
            x = xb[ix][:,i]
            if i==1: x = 500-x #flip the y axis
            ax.set_xlabel('Time')
            ax.set_ylabel("XY"[i])
            ax.plot(tb[ix],x,'rx')
        ax=fig.add_subplot(3,1,3)
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.plot(xb[ix][:,0],500-xb[ix][:,1],'bo')
        plt.show()

def proj_obs_3d(tb, xb, tr, xr, indices, logs, args, playmeta=None):
    racket_dist_names = ["racket_prior", "racket_post", "racket_post_init_cond"] 
    fig = plt.figure()
    ball_colors = "rgbykmc"
    racket_dists = []
    for log in logs:
        racket_dist = {}
        for i,dname in enumerate(racket_dist_names):
            if dname in log and isinstance(log[dname], list):
                trp, xrp, drp = decode_racket_dist(log[dname])
                racket_dist[dname] = {"t": trp, "x": xrp, "dev": drp, "color": ball_colors[i]}
        racket_dists.append(racket_dist)

    for d in xrange(3):
        ax = fig.add_subplot(3,1,d+1)
        ax.set_ylabel("XYZ"[d])
        for ix in indices:
            ax.scatter(tb[ix], xb[ix][:,d], c=ball_colors[ix])
        for log in logs:
            if 'predict_ball_traj' in log:
                tpb = np.array(log['predict_ball_traj']['times'])
                xpb = np.array(log['predict_ball_traj']['means'])
                covs = log['predict_ball_traj']['covs']
                if args.time:
                    t1 = tpb[0]
                    t2 = t1 + args.time
                    tpb, xpb, covs = time_trim((tpb,xpb,covs), t0=t1, tf=t2)
                dpb = np.array( map(lambda x: np.sqrt(np.diag(np.reshape(np.array(x),(3,3)))), covs) )
                ax.plot(tpb, xpb[:,d], c='y')
                ax.fill_between(tpb, xpb[:,d] - dpb[:,d], xpb[:,d] + dpb[:,d], color='y', alpha=0.3)
        for i in args.racket_dists:
            for log_ix,log in enumerate(logs):
                if racket_dist_names[i] in racket_dists[log_ix]:
                    rdist = racket_dists[log_ix][racket_dist_names[i]]
                    ax.plot(rdist['t'], rdist['x'][:,d], c=rdist['color'])
                    ax.fill_between(rdist['t'], rdist['x'][:,d] - rdist['dev'][:,d], rdist['x'][:,d] + rdist['dev'][:,d], color=rdist['color'], alpha=0.3)
        for log in logs:
            if 'cond_ix' in log and 'predict_ball_traj' in log:
                t_hit = log['predict_ball_traj']['times'][log['cond_ix']]
                ax.axvline(t_hit)
            if 'predict_ball_traj' in log and 'last_time' in log['predict_ball_traj']:
                tlast = log['predict_ball_traj']['last_time']
                ax.axvline(tlast, color='yellow')
        if xr is not None:
            ax.plot(tr, xr[:,d], color='black')
        if playmeta is not None:
            for game in playmeta:
                ax.axvline(game["game_start_time"], color='green')
                ax.axvline(game["game_end_time"], color='green')
                for trial in game["trials"]:
                    #Only display for the moment for player 1
                    if trial["player"] != 1: continue
                    trial_color = "red"
                    if "success" in trial and trial["success"]: trial_color = 'blue'
                    ax.axvspan(trial["start_time"], trial["end_time"], alpha=0.2, color=trial_color)
                    if "ball_fw_time" in trial:
                        ax.axvline(trial["ball_fw_time"], color='black')
    ax.set_xlabel("Time")
    plt.show()

def joint_space_plot(t, q, qd, logs, args):
    planned_trajs = []
    planned_vels = []
    for log in logs:
        if 'joint_mean' in log:
            q_mean, qd_mean, t_mean = slpp.joints_to_numpy(log['joint_mean'])
            planned_trajs.append([t_mean, q_mean, 'red'])
            planned_vels.append([t_mean, qd_mean, 'red'])
        if 'joint_mean_post' in log:
            q_post, qd_post, t_post = slpp.joints_to_numpy(log['joint_mean_post'])
            planned_trajs.append([t_post, q_post, 'green'])
            planned_vels.append([t_post, qd_post, 'green'])
    fig = plt.figure()
    for i,ix in enumerate(args.joints):
        ax = fig.add_subplot(len(args.joints),2,1+2*i)
        ax.set_ylabel("q_{0}".format(ix))
        ax.plot(t, q[:,ix], color="black")
        for q_traj in planned_trajs:
            ax.plot(q_traj[0], q_traj[1][:,ix], color=q_traj[2])
        ax = fig.add_subplot(len(args.joints),2,2+2*i)
        ax.set_ylabel("qd_{0}".format(ix))
        ax.plot(t, qd[:,ix], color="black")
        for qdtraj in planned_vels:
            ax.plot(qdtraj[0], qdtraj[1][:,ix], color=qdtraj[2])
    ax.set_xlabel("Time")
    plt.show()


def main(args):
    trial_data = slpp.load_json(args.file)
    bk = fwd_kin.BarrettKinematics()

    tb, xb = slpp.obs_to_numpy(trial_data['obs'])
    qr, qdr, tr = slpp.joints_to_numpy(trial_data['joints'])
    if qr is not None:
        xr, ori_racket = bk.end_eff_trajectory(qr)
    else:
        xr = None

    if 'success' in trial_data:
        if trial_data['success']: print "Trajectory seems successful"
        else: print "Unsuccessful trajectory"
    
    #log = {}
    logs = []
    if 'logs' in trial_data: logs = trial_data['logs']
    #if 'log' in trial_data and trial_data['log']: log = trial_data['log']

    #if args.plot_overlap:
    #    plot_overlap(log, args)
    playmeta = None
    if args.playmeta:
        with open(args.playmeta,'r') as f:
            playmeta = json.load(f)

    if args.task_traj is not None:
        plot_ball_and_rob(tr, xr, tb, xb, logs, args)

    if args.obs2d is not None:
        plot_obs_2d(tb, xb, args.obs2d)

    if args.proj3d is not None:
        proj_obs_3d(tb, xb, tr, xr, args.proj3d, logs, args, playmeta=playmeta)

    if args.joints is not None:
        joint_space_plot(tr, qr, qdr, logs, args)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    rel_path = lambda fname: os.path.join(os.path.dirname(__file__), fname)
    parser.add_argument('file', help="File name where the trial data is stored")
    parser.add_argument('--playmeta', help="Display also the play meta deta from this file")
    parser.add_argument('--plot_overlap', help="Plot the prior ball to racket overlap", action="store_true")
    parser.add_argument('--obs2d', nargs='+', type=int, help="Plot the 2d ball observations passed as parameter w.r.t time")
    parser.add_argument('--proj3d', nargs='+', type=int, help="Given indices of 3D observations, plot each of the axis w.r.t time")
    parser.add_argument('--task_traj', nargs='+', type=int, 
            help="Plot the end effector trajectory along with the observations of the camera passed as parameter")
    parser.add_argument('--joints', nargs='+', type=int, help="Indices of the joints to plot")
    parser.add_argument('--time', type=float, help="Limit time span to at most the given number of seconds")
    parser.add_argument('--racket_dists', type=int, default=range(3), nargs='+', help="Indices of the racket distributions to plot")
#  parser.add_argument('--host', default='localhost', help="Name of the host to connect to")
    args = parser.parse_args()
    main(args)
