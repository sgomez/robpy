
import robpy.slpp as slpp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import table_tennis as tt
import numpy as np
import argparse
import os
import json
import time

def plot_ball_traj(pos):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(pos[:,0], pos[:,1], zs=pos[:,2], c='b')
    ax.set_title('Ball Trajectory')
    plt.show()

def get_sim_data(sensor_id, obs, times):
    N = len(obs)
    ans = []
    for i in xrange(N):
        ans.append({'sensor_id': sensor_id, 'time': times[i], 'obs': obs[i]})
    return ans

def main(args):
    f = open(args.ballmodel, "r")
    conf = f.read()
    ball_model = tt.load_ball_model_obj(conf)

    url = "tcp://{0}:{1}".format(args.host, args.port) if not args.url else args.url
    client = slpp.SLPP( url=url )

    ball_obs = tt.sample_ball_traj_obs(ball_model, args.trajlen, args.deltaT)
    sim_times = np.array(range(1,args.trajlen+1)) * args.deltaT
    data = get_sim_data(args.sensorid, ball_obs.tolist(), sim_times)
    client.query('obs/add_sim_obs', data)
    time.sleep(args.deltaT * args.trajlen + 0.3)
    robot_obs = client.query('obs/flush',{'ids': [args.sensorid]})
    t, x = slpp.obs_to_numpy(robot_obs)
    plot_ball_traj(x[1]) #plot observations of sensor 1

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Python example for ball trajectory code')
    rel_path = lambda fname: os.path.join(os.path.dirname(__file__), fname)
    parser.add_argument('--ballmodel', default = rel_path('../config/ball_model.json'), \
        help="File name where the ball model is stored")
    parser.add_argument('--host', default='localhost', help="Name of the host to connect to")
    parser.add_argument('--port', default=7647, help="Port of the host to connect to")
    parser.add_argument('--url', help="Overrides the host and port setting directly a ZMQ bind URL")
    parser.add_argument('--sensorid', default=1, help="Id of the sensor that provides the ball observations")
    parser.add_argument('--trajlen', default=50, help="Ball trajectory length")
    parser.add_argument('--deltaT', default=1.0/60.0, help="Time distance between ball observations")
    args = parser.parse_args()
    main(args)
