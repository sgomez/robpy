"""Script to plot ball trajectory data

"""

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

import numpy as np
import argparse
import json

def plot_ball_and_rob(tb, xb):
    #t_dist, x_dist = time_series.distance(tr, xr, tb, xb)
    #dist_summ = time_series.summary_stats(t_dist,x_dist)

    #fig = plt.figure()
    #ax = fig.add_subplot(111, projection='3d')
    ball_colors = "rgbykmc"
    t0 = 1e100
    print len(xb), " trajectories"
    for i in xrange(10):
        x = np.array(xb[i])
        plt.plot(x[:,1], x[:,2], 'bo')
        plt.show()


def main(args):
    with open(args.file, 'r') as f:
        if args.file.endswith('json'):
            stream = json.load(f)
        elif args.file.endswith('npz'):
            stream = np.load(f)
        plot_ball_and_rob(np.array(stream['Times']), np.array(stream['Xavg']))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('file', help="File name where the trial data is stored")
    args = parser.parse_args()
    main(args)
