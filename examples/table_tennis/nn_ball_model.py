
import robpy.table_tennis.nn_ball_model as ball_model
import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
import keras.models
from mpl_toolkits.mplot3d import Axes3D
import tensorflow as tf

import robpy.kalman as kalman
import robpy.table_tennis.ball_model as kf_ball_model

def train(obs, args):
    model = ball_model.train_rtsp(obs)
    if args.model:
        model.save(args.model)
    return model

def train_kf(obs, args, is_bounce=None):
    beta = [0.5,0.5,0.1] #air drag
    bounceFac = [0.9,0.9,0.8] #bounce factors
    deltaT = 0.016
    bounceZ = 0 #-0.99 + 0.02
    A = kf_ball_model.Ball_A_Mat(beta, bounceFac, deltaT, bounceZ)
    A.optional['isBounce'] = is_bounce
    #A = ball_model.Yanlong_A_Mat(airDrag, bounceFac, deltaT, bounceZ)
    B = kf_ball_model.Ball_B_Mat(deltaT, bounceZ, bounceFac[2])
    C = kalman.ConstMat(np.array([[1,0,0,0,0,0],[0,0,1,0,0,0],[0,0,0,0,1,0]]))
    kf = kalman.LinearKF(A, C, B=B)
    init_vals = {'init_state_mean': np.zeros(6), 'init_state_cov': np.eye(6)*10,
                'init_Sigma': 1e-2*np.eye(3), 'init_Gamma': 1e-2*np.eye(6),
                'prior_P0': kalman.get_cov_prior([0.5,1,1,1,0.5,1], 10),
                'prior_Sigma': kalman.get_cov_prior([0.0001,0.0001,0.0001], 6),
                'prior_Gamma': kalman.get_cov_prior([1e-6 for i in xrange(6)], 10)}
    U = np.ones((obs.shape[0],obs.shape[1],1))
    X = [[np.array([x]) for x in traj] for traj in obs.tolist()]
    Times = 0.016*np.tile(np.array(range(obs.shape[1])),(obs.shape[0],1))
    kf.EM_train(X, list(U), Abounds=[(0,10),(0,10),(0,10),(0,1),(0,1),(0,1)], Time=list(Times), 
            debug_LB=False, em_print_params=False, max_iter=20, **init_vals)
    #kf.mu0 = np.zeros(6)
    #kf.P0 = np.eye(6)
    #kf.Sigma = init_vals['init_Sigma']
    #kf.Gamma = init_vals['init_Gamma']
    print "mu0=", kf.mu0
    print "P0=", kf.P0
    print "Init state stdev = ", np.sqrt(np.diag(kf.P0))
    print "Sigma=", kf.Sigma
    print "Noise stdev=", np.sqrt(np.diag(kf.Sigma))
    print "Gamma=", kf.Gamma
    print "Model Stdev=", np.sqrt(np.diag(kf.Gamma))
    A_theta = A.params
    print "air_drag=", A_theta[0:3]
    print "bouncing=", A_theta[3:6]
    return kf

def pref_kf(kf, given_obs, steps, is_bounce=None):
    kf.A.optional['isBounce'] = is_bounce
    s = given_obs.shape
    x = np.concatenate((given_obs, np.zeros((s[0],steps,s[2]))), axis=1)
    u = np.ones((s[1]+steps,1))
    for n in xrange(s[0]):
        X = [[np.array(o)] for o in given_obs[n,:,:].tolist()]
        means, covs, P_vals, As, Bs, Cs, Ds = kf.forward_recursion(X,u,n=0)
        for t in xrange(steps):
            po = np.dot(Cs[0], means[s[1]+t])
            x[n,s[1]+t,:] = po
    return x

def get_obs(N=100, T=50, deltaT=0.006, noise=None, max_bounces=None):
    time, obs, is_bounce = ball_model.sim_data(N=N, T=T, deltaT=deltaT, max_bounces=max_bounces,
            quad_air_drag=np.array([0.15,0.0,0.1]), bounce_fac=np.array([1.0,1.0,1.0]))
    if noise is not None:
        obs += np.random.normal(loc=0.0,scale=noise,size=obs.shape)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for i in xrange(10):
        ax.scatter(obs[i,:,0], obs[i,:,1], zs=obs[i,:,2])
    plt.show()
    return time, obs, is_bounce

def eval_model(model, kf_model, obs, burn_out=20, steps=30, is_bounce=None):
    given_obs = obs[:,0:burn_out,:]
    pred = ball_model.predict_rtsp(model, given_obs, steps)
    pred_kf = pref_kf(kf_model, given_obs, steps, is_bounce=is_bounce)
    pred_toy = ball_model.predict_sim_data(given_obs, steps)
    real = obs[:,0:steps+burn_out,:]
    diff = real-pred
    #print diff
    print np.concatenate((real, pred, pred_kf), axis=2)

def main(args):
    if args.data:
        #print args.data
        if os.path.isfile(args.data):
            with open(args.data,'r') as f:
                data = np.load(f)
                times = data['times']
                obs = data['obs']
                is_bounce = data['is_bounce']
        else:
            times, obs, is_bounce = get_obs(N=5000,T=200,noise=0.005,max_bounces=1)
            with open(args.data,'w') as f:
                data = {'times': times, 'obs': obs, 'is_bounce': is_bounce}
                np.savez(f, **data)
    else:
        times, obs, is_bounce = get_obs(N=2048,T=300,noise=0.01)
    
    N,T = times.shape
    print("Loading data-set of {} instances and {} time samples".format(N,T))
    #print times
    ext_obs = np.concatenate((np.reshape(times, (N,T,1)),obs),axis=2)
    #model = ball_model.train_cnn(ext_obs[0:4000,:,:], k=32, num_iter=100000, val_data=ext_obs[4000:,:,:])
    k = 32
    with tf.Session() as sess:
        model_path = '/tmp/cnn_ball_model/cnn_ball_model' if not args.cnn else args.cnn
        model = ball_model.load_cnn(sess, model_path)
        err = ball_model.cnn_predict_error(sess, model, ext_obs[4000:,:,:], k)
        pred = ball_model.cnn_predict_many(sess, model, ext_obs[4000:,0:k,:], ext_obs[4000:,k:,0])
        #plt.plot(err)
        #plt.show()
        
        for ix in range(5):
            fig = plt.figure()
            axes = [plt.subplot(3,1,i+1) for i in range(3)]
            time = ext_obs[4000+ix,:,0]
            real = ext_obs[4000+ix,:,1:]
            curr_pred = pred[ix,:,:]
            for i in range(3):
                axes[i].scatter(time,real[:,i],c='r')
                axes[i].plot(time[k:], curr_pred[:,i])
            plt.show()
    
    #if os.path.exists(args.model):
    #    model = keras.models.load_model(args.model)
    #else :
    #    model = train(obs, args)

    #kf_model = train_kf(obs, args, is_bounce=is_bounce)    
    #obs_test, bounce_test = get_obs(N=1)
    #eval_model(model, kf_model, np.array([obs[0]]), is_bounce=bounce_test)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--model', help="File name of the model")
    parser.add_argument('--cnn', help="Path to the cnn model")
    parser.add_argument('--data', help="File where the ball data is stored")
    args = parser.parse_args()
    main(args)
