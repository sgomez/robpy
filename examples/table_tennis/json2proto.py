""" Converts a recorded JSON file to a protocol buffer binary
"""

import robpy.protos.table_tennis_pb2 as ttpb
from google.protobuf import json_format
import argparse
import json
import os
import re
import struct

def main(args):
    fexp = re.compile(r'([\w]+)\.(json)')
    if args.path:
        json_files = filter(fexp.search, os.listdir(args.path))
        json_files.sort()
    else:
        json_files = []
    if args.json_file: json_files.append(args.json_file)

    with open(args.pb_file,'ab') as fout:
        for fname in json_files:
            with open(os.path.join(args.path,fname), 'r') as fin:
                data = json.load(fin)
            trial = ttpb.Trial()
            json_format.ParseDict(data, trial)
            serialized = trial.SerializeToString()
            fout.write(struct.pack('I', len(serialized)))
            fout.write(serialized)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('pb_file', help="Path to the protobuf output file")
    parser.add_argument('--json_file', help="Path to a json file")
    parser.add_argument('--path', help="Path to a folder where every JSON file should be converted")
    args = parser.parse_args()
    main(args)
