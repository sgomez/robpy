""" Loads a ball model and tests the prediction accuracy
"""


import robpy.kalman as kalman
import robpy.table_tennis.ball_model as ball_model
import robpy.table_tennis.load_data as loader
import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
import json

def test_pred(fw_ball_traj, kf, nobs=20):
    X, Xavg, U, Hidden, Times = fw_ball_traj
    N = len(X)
    for n,Xn in enumerate(X):
        Tn = len(Xn)
        Un = np.ones((Tn,1))
        means, covs, P_vals, As, Bs, Cs, Ds = kf.forward_recursion(Xn[0:nobs],Un)
        time_obs = Times[n]
        x_obs = np.array(Xavg[n])
        pred = np.array(means)[:,[0,2,4]]
        plt.figure(1)
        for d in xrange(3):
            plt.subplot(3,1,d+1)
            plt.plot(time_obs,x_obs[:,d],'g.')
            plt.plot(time_obs,pred[:,d],'r')
        plt.show()
        diff = Xavg[n] - pred
        dist = np.linalg.norm(diff, axis=1)
        plt.plot(range(Tn), dist)
        plt.show()
        #if n==3: break


def main(args):
    with open(args.data,'r') as f:
        if args.data.endswith('json'):
            fw_ball_traj = loader.fw_ball_traj( json.load(f) )
        elif args.data.endswith('npz'):
            data = np.load(f)
            fw_ball_traj = (data['X'], data['Xavg'], data['U'], data['Hidden'], data['Times'])
        else:
            print("Unrecognized format file: {}".format(args.data))
            return 1
    kf, deltaT = ball_model.load_ball_model(args.ball_model)

    test_pred(fw_ball_traj, kf, nobs=args.nobs)




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('ball_model', help="Path to the ball model")
    parser.add_argument('data', help="Path to the consolidated data of ball trajectories")
    parser.add_argument('--nobs', type=int, default=20, help="List of segmentation meta data files")
    args = parser.parse_args()
    main(args)
