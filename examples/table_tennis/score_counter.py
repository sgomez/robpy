""" Connects to the vision server to score a table tennis match

"""

import zmq
import robpy.slpp as slpp
import robpy.utils as utils
import robpy.table_tennis.load_data as load_data
import numpy as np
import time
import datetime
import json
import argparse
import os
import threading
import pygame
import logging

def game_tracker(url, callbacks, time_bw_games=0.5):
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    logging.info("Connecting to {}".format(url))
    socket.connect(url)
    topicfilter = ""
    socket.setsockopt(zmq.SUBSCRIBE, topicfilter)
    ball_obs = []
    running = True

    logging.info("Waiting for messages...")
    while running:
        body = socket.recv()
        msg = json.loads(body)
        msg['time'] = msg['time'] / 1e9
        if msg['obs'] is not None:
            logging.debug("Appending obs {}".format(msg['obs']))
            ball_obs.append(msg)
        elif len(ball_obs)>0 and (msg['time'] - ball_obs[-1]["time"])>time_bw_games:
            logging.info("Calling callbacks with {} obs".format(len(ball_obs)))
            for cb in callbacks:
                ans = cb(ball_obs)
                if ans: running = False
            logging.debug("Cleaning ball obs cache")
            ball_obs = []

def ball_obs_to_slpp_log(ball_obs):
    return {'joints': [], 'obs': [{'id': 1, 'values': ball_obs}]}

def game_meta(traj_cleaner, slpp_log):
    return traj_cleaner.process_trial(slpp_log)

def game_winner(trials):
    player = -1
    for trial in trials:
        if trial['valid']:
            player = trial['player']
            opponent = -player
            if not trial['opponent_success']:
                return player
            if not 'success' in trial or not trial['success']:
                return opponent
        else:
            logging.warning('Invalid trial from {} to {}'.format(
                trial['start_ball_ix'],trial['end_ball_ix']))
    return player

class ScoreCounter:

    def __init__(self, traj_cleaner, logdir = '/tmp/tt_games'):
        self.traj_cleaner = traj_cleaner
        self.logdir = logdir
        self.score = {-1: 0, 1: 0}

    def __call__(self, obs):
        logging.debug("Score counter callback called with {} obs".format(len(obs)))
        if len(obs) < 100:
            logging.debug("Ignoring {} isolated observations")
            return False # return but do not quit
        slpp_log = ball_obs_to_slpp_log(obs)
        t_stamp = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
        data_file = os.path.join(self.logdir, "data_{}.json".format(t_stamp))
        meta_file = os.path.join(self.logdir, "meta_{}.json".format(t_stamp))
        logging.info("Saving game data in {}".format(data_file))
        slpp.save_json(data_file, slpp_log)
        logging.info("Obtaining game meta-data")
        meta = game_meta(self.traj_cleaner, slpp_log)
        logging.info("Saving game meta-data in {}".format(meta_file))
        slpp.save_json(meta_file, meta)
        #Extract the number of trials
        if len(meta)!=1:
            logging.warning("Game {} was split in {} games. Ignoring it".format(t_stamp,len(meta)))
            return False
        game = meta[0]
        trials = game['trials']
        if trials is None:
            logging.debug("Game {} has no trials. Ignoring".format(t_stamp))
            return False
        logging.info("Game {} consisted of {} balls".format(t_stamp, len(trials)))
        #Decide who won based on the first mistake 
        #last_player = -trials[-1]['player']
        #last_success = trials[-1]['opponent_success']
        #winner = last_player if last_success else -last_player
        if len(trials)<2: return False #Ignore service only trials
        trials_without_service = trials[1:]
        winner = game_winner(trials_without_service)
        logging.info("Winner: {}".format(winner))
        self.score[winner] += 1
        logging.info("Current score: {}".format(self.score))
        return False


def main(args):
    url = "tcp://{}:{}".format(args.host, args.port)
    traj_cleaner = slpp.TrajCleaner(buff_size=50000000,segm_type="play")
    traj_cleaner.set_config({'min_subseq_obs': 20})
    if args.conf:
        additional_conf = slpp.load_json(args.conf)
        traj_cleaner.set_config(additional_conf)
    score_counter = ScoreCounter(traj_cleaner, logdir=args.log)
    game_tracker(url, [score_counter])

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--host', help="Name of the host running the vision", default="localhost")
    parser.add_argument('--port', default=7660, help="Port of the vision 3D server")
    parser.add_argument('--log', default="/tmp", help="Directory where the hitting log information is stored")
    parser.add_argument('--conf', help="Additional configuration parameters for the segmentation server")

#    rel_path = lambda fname: os.path.join(os.path.dirname(__file__), fname)
    args = parser.parse_args()
    main(args)
