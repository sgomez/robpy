
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import random
import robpy.table_tennis.load_data as load_data
import robpy.slpp as slpp
import argparse
import tensorflow as tf

def time_to_dt(X):
    for i in reversed(range(1,len(X))): X[i,3] = X[i,3] - X[i-1,3]
    X[0,3] = 0.0

def dt_to_time(X, t_offset=0.0):
    X[0,3] = t_offset
    for i in range(1,len(X)): X[i,3] = X[i,3] + X[i-1,3]

def predict_cleaner_model(bdata, outputs, model):
    model_pred = []
    for i in range(len(bdata)):
        curr_pred = {'ball': bdata[i]}
        feed_dict = {model['input']: [bdata[i]]}
        for k in outputs:
            kpred = model['pred_{}'.format(k)].eval(feed_dict=feed_dict)
            curr_pred[k] = np.reshape(kpred, [-1])
        #print(len(sdata[i]['ball']), len(sdata[i]['ball_direction']))
        model_pred.append(curr_pred)
    return model_pred

def load_raw_data(fname, split_time=0.2, min_obs_traj=180):
    trial_data = slpp.load_json(fname)
    tb, xb = slpp.obs_to_numpy(trial_data['obs'])
    l = tb[1]
    assert(all(l[i] <= l[i+1] for i in range(len(l)-1)))
    split_ix = [i for i in range(1,len(l)) if (l[i]-l[i-1])>split_time]
    split_ix.append(len(l))
    bobs = np.array(xb[1])
    btime = np.reshape(np.array(tb[1]),(-1,1))
    bdata = []
    last_ix = 0
    for ix in split_ix:
        if ix - last_ix > min_obs_traj:
            inst = np.concatenate((bobs[last_ix:ix,:],btime[last_ix:ix,:]), axis=1)
            bdata.append(inst)
        last_ix = ix
    return bdata

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('model', help='Path to the tensorflow saved model')
    parser.add_argument('obs', help="Path to a raw data file to label")
    parser.add_argument('--outfile', help="A npz file where the labeled data should be stored")
    parser.add_argument('--split_time', default=0.2, type=float, help="Split by duration between ball observation")
    parser.add_argument('--min_obs_traj', default=100, type=int, help="Minimum number of observations per trial")
    args = parser.parse_args()

    outputs = {'ball_cut': 2, 'ball_direction': 3, 'ball_bounce': 2, 'ball_reliable': 2}
    with tf.Session() as sess:
        new_saver = tf.train.import_meta_graph('{}.meta'.format(args.model))
        new_saver.restore(sess, '{}'.format(args.model))
        model = {'input': tf.get_collection("input")[0]}
        pref = ['pred', 'prob', 'logit', 'loss', 'out']
        for k in outputs:
            for cpref in pref:
                name = "{}_{}".format(cpref,k)
                model[name] = tf.get_collection(name)[0]
        bdata = load_raw_data(args.obs, split_time = args.split_time, min_obs_traj=args.min_obs_traj)
        for x in bdata: time_to_dt(x)
        model_pred = predict_cleaner_model(bdata, outputs, model)
        for x in bdata: dt_to_time(x)
        if args.outfile:
            out_data = {'ball': []}
            #with open(args.outfile, 'w') as f:
            #    np.savez(f, **model_pred)
        for inst in model_pred:
            load_data.plot_time_cleaner_instance(inst)
