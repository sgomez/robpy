"""Creates a consolidated data set from scattered files

Given a path, this script scans recursively all folders searching for
file names that match the expected name pattern. All the data is then
processed and consolidated in a single file.
"""

import robpy.slpp as slpp
import numpy as np
import sys
import time
import argparse
import os
import re
import json

def list_files(path, pattern = 'rec_\d+_\d+.json'):
    ans = []
    for folder, subs, files in os.walk(path):
        for f in files:
            if re.match(pattern, f):
                ans.append(os.path.join(folder, f))
    return ans

def load_data(files, traj_clean):
    ans = []
    for fname in files:
        with open(fname,'r') as f:
            try:
                data = json.load(f)
            except:
                print("Error loading the file {}. Skipping it.".format(fname))
                continue
            meta = traj_clean.process_trial(data)
            ans.append({'meta': meta, 'data': data})
    return ans

def main(args):
    files = list_files(args.path) #glob.glob('{0}/**/{1}'.format(args.path, pattern), recursive=True)
    traj_clean = slpp.TrajCleaner(buff_size=100000000, segm_type="play")
    data = load_data(files, traj_clean)
    if args.raw_out:
        with open(args.raw_out, 'w') as f:
            if args.raw_out.endswith('json'):
                json.dump(data, f)
            elif args.raw_out.endswith('npz'):
                np.savez(f, raw=data)
    traj_clean.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('path', help="Path to scan recursively for scattered files")
    parser.add_argument('--raw_out', help="File to store the consolidated raw output")
    args = parser.parse_args()
    main(args)
