""" Runs a task that trains and uses a single ProMP

This task has a Kin. theaching mode and a playing mode. The
ball gun is supposed to be fixed since the experiment starts
execution. The obtained trajectories are cleaned up with some
external code (A C++ code I wrote in the table_tennis project).
The cleaned trajectories are used to keep training the ProMP to
improve performance.
"""

import robpy.slpp as slpp
import robpy.utils as utils
import robpy.full_promp as promp
import robpy.table_tennis.load_data as load_data
import robpy.table_tennis.ball_model as ball_model
import numpy as np
import sys
import time
from time import sleep
import json
import argparse
import os
import threading
import pygame
import logging

class SinglePrompTask:

    def start_kin_teach(self, **params):
        self.__state = "kin_teach"
        print("Starting teaching mode")
        self.recorder.stop_recording(do_save=False)
        self.recorder.start_recording()
        self.__last_process_trial = time.time()
        self.client.query("tt_single_promp/stop", "")

    def start_table_tennis(self, **params):
        self.__state = "table_tennis"
        self.recorder.stop_recording(do_save=False)
        self.recorder.start_recording()
        self.__last_process_trial = time.time()
        self.client.query("tt_single_promp/start", "")

    def stop_task(self):
        if self.__last_process_trial:
            self.process_trials()
        self.__state = "stopped"
        if "stop" in self.__retrain: self.__retrain["stop"].set()
        print("Stoping table tennis mode")
        time = self.client.query("tt_single_promp/stop", "")
        self.recorder.stop_recording(do_save=False)
        self.__last_process_trial = None

    def conf_trial_cleaner(self, conf):
        self.trial_clean.set_config(conf)

    def __init__(self, **params):
        self.client = slpp.SLPP(url = params['url'])
        self.recorder = slpp.Recorder(self.client, **params['recorder'])
        self.loader = load_data.PlayDatasetBuilder()
        self.trial_clean = slpp.TrajCleaner(**params['trial_clean_srv'])
        self.__retrain = params['retrain']
        self.__retrain["thread"] = None
        self.__retrain["data_lock"] = threading.Lock()
        self.__retrain["tot_success"] = 0
        self.__promp_params = params["promp_params"]
        self.trial_ready_callbacks = []
        self.__last_process_trial = None
        self.stop_task()

    def configure_table_tennis(self, **params):
        ''' Sends the parameters required by the table tennis task via JSON
        '''
        methods = [{'file': params["ballmodel"], 'method': 'tt_single_promp/load_ball_model'}, \
          {'file': params["barrettkin"], 'method': 'tt_single_promp/load_barrett_kin'}, \
          {'file': params["promp"], 'method': 'tt_single_promp/load_prior_promp'}]
        ans = []
        for method in methods:
            f = open(method['file'], 'r')
            body = json.load(f)
            print "Configuring on ", method['method']
            ans.append( self.client.query(method['method'], body) )
        return ans

    def configure_task(self, conf):
        self.client.query("tt_single_promp/set_config", conf)

    def close(self):
        t = self.__retrain["thread"]
        self.stop_task()
        self.trial_clean.close()
        if t:
            print "Waiting for the ProMP thread to finish"
            t.join()

    def send_sim_ball_traj(self, ball_sim_promp, deltaT, **params):
        params.setdefault("trajlen", 180)
        params.setdefault("sensor_id", 1)
        trajlen = params['trajlen']
        sim_times = np.array(range(1,trajlen+1)) * deltaT
        ball_obs = ball_sim_promp.sample([sim_times])[0]
        data = []
        for i, obs in enumerate(ball_obs):
            data.append({"sensor_id": params["sensor_id"], "time": sim_times[i], "obs": obs.tolist()})
        self.client.query('obs/add_sim_obs', data)

    def send_sim_ball_data(self, ix, t, x, max_time_gap=0.1, sensor_id=1, min_traj_len=150):
        data = []
        while ix<len(t) and len(data) < min_traj_len:
            t0 = t[ix]
            data = [{"sensor_id": sensor_id, "time": t[ix]-t0, "obs": x[ix].tolist()}]
            ix += 1
            while ix<len(t) and (t[ix]-t[ix-1]) < max_time_gap:
                data.append({"sensor_id": sensor_id, "time": t[ix]-t0, "obs": x[ix].tolist()})
                ix += 1
        if len(data) > min_traj_len: self.client.query('obs/add_sim_obs', data)
        return ix

    def retrain_ball_model(self):
        #NOTE: For the moment only works for the ball ProMP model and the parameters are hard coded
        with self.__retrain["data_lock"]:
            ball_time, ball_positions = self.loader.fw_ball_traj()
        fw_ball_traj = (ball_time, ball_positions)
        duration = 1.2
        min_z = -0.75
        min_duration = 0.6
        ball_promp = ball_model.train_ball_promp(fw_ball_traj, duration, min_z=min_z, min_duration=min_duration)
        print "Ball model trained"
        stream = ball_promp.to_stream()
        data = {'promp': stream, 'duration': duration, 'min_z': min_z}

        slpp.save_json("/tmp/ball_model.json", data)
        self.client.query("tt_single_promp/load_ball_model", data)


    def retrain_promp(self):
        #NOTE: Need mutex for gathering the data only. At the end pass learned ProMP by network to SL
        while not self.__retrain["stop"].isSet():
            with self.__retrain["data_lock"]:
                time, Q, Qdot = promp.joint_train_data( self.loader )
            if self.__state == "table_tennis" and not self.__retrain['retrain_while_playing']:
                sleep(5.0)
                continue
            pp = self.__promp_params #NOTE: Might be thread unsafe
            dim_basis_fun = promp.dim_comb_basis(**pp['basis'])
            inv_whis_mean = lambda v, Sigma: (1-pp['pdiag_sw'])*utils.make_block_diag(Sigma, pp['ndof']) + pp['pdiag_sw']*np.eye(dim_basis_fun*pp['ndof'])
            inv_wish_prior = {'v':dim_basis_fun*pp['ndof'], 'mean_cov_mle': inv_whis_mean}
            robot_promp = promp.FullProMP(basis=pp['basis'])
            quit_fn = lambda: self.__retrain["stop"].isSet() 
            print "Training ProMP with {0} instances".format(len(time))
            if pp["use_velocity"]:
                robot_promp.train(time, q=Q, qd=Qdot, early_quit=quit_fn, prior_Sigma_w=inv_wish_prior, **pp)
            else:
                robot_promp.train(time, q=Q, early_quit=quit_fn, prior_Sigma_w=inv_wish_prior, **pp)
            print "ProMP trained"
            stream = robot_promp.to_stream()
            self.client.query("tt_single_promp/load_prior_promp", stream)
            slpp.save_json(pp["file"], stream)
            if "retrain_ball_model" in self.__retrain and self.__retrain["retrain_ball_model"]:
                self.retrain_ball_model()
            sleep(1.0)
        self.__retrain["thread"] = None

    def process_trials(self):
        logging.info("Gathering information and processing trials")
        data = self.recorder.to_stream()
        meta = self.trial_clean.process_trial(data)
        for method in self.trial_ready_callbacks:
            method(data, meta, self)
        # Create a temporal data loader object
        ldata = load_data.PlayDatasetBuilder()
        ldata.data = [{'data': data, 'meta': meta}]
        strike_traj = ldata.strike_trajectories(player=1, split_joint=0, zero_tol=0.01)
        logging.info("{} successful strike robot trajectories found".format(len(strike_traj)))
        if len(strike_traj)>0:
            num_success = len(strike_traj)
            with self.__retrain["data_lock"]:
                self.loader.data.append({"data": data, "meta": meta})
            self.__retrain["tot_success"] += num_success
            logging.info("Total number of successful trials: {}".format( self.__retrain["tot_success"] ))
            if self.__retrain["tot_success"] > self.__retrain["min_traj"] and not self.__retrain["thread"]:
                logging.info("About to launch a thread to train ProMPs")
                t = threading.Thread(target = lambda : self.retrain_promp())
                self.__retrain["thread"] = t
                self.__retrain["stop"] = threading.Event()
                t.start()
        else:
            logging.info("Malformed trial")
        self.recorder.save()
        self.recorder.clear_internal_state()

    def update(self):
        self.recorder.update()
        if self.__state != "stopped":
            if time.time() > (self.__last_process_trial + self.__retrain["process_trial_every"]):
                self.__last_process_trial = time.time()
                self.process_trials()
            
    def get_state(self): return self.__state

def redraw(screen, task, **params):
    colors = {'white': (255,255,255), 'black': (0,0,0), 'red': (255,0,0), 
            'green': (0,255,0), 'blue': (0,0,255), 'yellow': (255,255,0)}
    screen.fill(colors['white'])

    state = task.get_state()
    state_color = {'stopped': 'green', 'kin_teach': 'yellow', 'table_tennis': 'red'}
    scolor = colors[state_color[state]]
    pygame.draw.circle(screen, scolor, (200,200), 100)

def main(args):
    logging.basicConfig(filename="/tmp/tt_single_promp.log",level=logging.DEBUG)
    task = None
    try:
        conf = json.load(open(args.conf, 'r'))
        if args.url or args.port:
            if not args.url:
                args.url = "tcp://{0}:{1}".format(args.host, args.port)
            conf['url'] = args.url
        task = SinglePrompTask(**conf)
        task.configure_table_tennis(ballmodel=args.ballmodel,
                promp=args.promp, barrettkin=args.barrettkin)
        task.configure_task(conf["task_params"])
        #simkf, deltaT = ball_model.load_ball_model(args.simball)
        with open(args.simball, 'r') as sb:
            data = json.load(sb)
        ball_sim_promp = promp.FullProMP(**data['promp'])
        if args.simdata:
            data = slpp.load_json(args.simdata)
            tb, xb = slpp.obs_to_numpy(data['obs'])
            sim_time_ball = tb[1]
            sim_pos_ball = xb[1]
            sim_data_ix = 0
        else:
            sim_data_ix = None
        pygame.init()
        screen = pygame.display.set_mode((400,400))
        running = True
        state = "kin_teach"
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_k:
                        task.start_kin_teach()
                        print "Starting human demonstration mode"
                    elif event.key == pygame.K_t:
                        task.start_table_tennis()
                        print "Starting table tennis playing mode"
                    elif event.key == pygame.K_q:
                        running = False
                    elif event.key == pygame.K_s:
                        task.stop_task()
                        print "Stopping both table tennis and recording modes"
                    elif event.key == pygame.K_b:
                        if sim_data_ix is not None and sim_data_ix<len(sim_time_ball):
                            sim_data_ix = task.send_sim_ball_data(sim_data_ix, sim_time_ball, sim_pos_ball, 
                                    max_time_gap=conf['sim_params']['max_time_gap'], min_traj_len=conf['sim_params']['trajlen'])
                            print("Sending simulated obs from recorded data ix={}".format(sim_data_ix))
                        else:
                            deltaT = conf['task_params']['deltaT']
                            task.send_sim_ball_traj(ball_sim_promp, deltaT, **conf['sim_params'])
                            print "Sending some simulated ball observations"
            task.update()
            redraw(screen, task)
            pygame.display.flip()
            pygame.time.wait(20)
    finally:
        if task:
            task.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Record ball and joint information in JSON format')
    parser.add_argument('--host', help="Name of the host to connect to")
    parser.add_argument('--port', default=7647, help="Port of the host to connect to")
    parser.add_argument('--url', help="Overrides the host and port setting directly a ZMQ bind URL")
    parser.add_argument('--log', help="Directory where the hitting log information is stored")

    rel_path = lambda fname: os.path.join(os.path.dirname(__file__), fname)
    parser.add_argument('--promp', default = rel_path('../config/prior_promp.json'), \
            help="File name where the initial ProMP model is stored")
    parser.add_argument('--ballmodel', default = rel_path('../config/promp_ball_model.json'), \
            help="File name where the ball model is stored")
    parser.add_argument('--barrettkin', default = rel_path('../config/barrett_kin.json'), \
            help="File name where the Barrett forward kinematics configuration is stored")
    parser.add_argument('--conf', default = rel_path('../config/tt_single_promp.json'), \
            help="File where the general configuration of the task is stored")
    parser.add_argument('--simball', default = rel_path('../config/promp_ball_model.json'), \
            help = "File name where the simulation ball model is stored")
    parser.add_argument('--simdata', help="A file containing the ball observation to use in simulation")

    args = parser.parse_args()
    main(args)
