""" Runs a task that trains and uses a single ProMP

This task has a Kin. theaching mode and a playing mode. The
ball gun is supposed to be fixed since the experiment starts
execution. The obtained trajectories are cleaned up with some
external code (A C++ code I wrote in the table_tennis project).
The cleaned trajectories are used to keep training the ProMP to
improve performance.
"""

import robpy.slpp as slpp
import robpy.utils as utils
import robpy.full_promp as promp
import robpy.table_tennis.load_data as load_data
import robpy.table_tennis.ball_model as ball_model
import numpy as np
import sys
import time
import json
import argparse
import os
import threading
import pygame

class SinglePrompTask:

    def start_kin_teach(self, **params):
        self.__state = "kin_teach"
        self.recorder.stop_recording(do_save=False)
        self.recorder.start_recording()
        self.__last_process_trial = time.time()
        self.client.query("table_tennis/stop", "")

    def start_table_tennis(self, **params):
        self.__state = "table_tennis"
        self.recorder.stop_recording(do_save=False)
        self.recorder.start_recording()
        self.__last_process_trial = time.time()
        self.client.query("table_tennis/start", "")

    def stop_task(self):
        if self.__last_process_trial:
            self.process_trials()
        self.__state = "stopped"
        if "stop" in self.__retrain: self.__retrain["stop"].set()
        time = self.client.query("table_tennis/stop", "")
        self.recorder.stop_recording(do_save=False)
        self.__last_process_trial = None

    def conf_trial_cleaner(self, conf):
        self.trial_clean.set_config(conf)

    def __init__(self, **params):
        self.client = slpp.SLPP(url = params['url'])
        self.recorder = slpp.Recorder(self.client, **params['recorder'])
        self.loader = load_data.DatasetBuilder()
        self.trial_clean = slpp.TrajCleaner(**params['trial_clean_srv'])
        self.__retrain = params['retrain']
        self.__retrain["thread"] = None
        self.__retrain["data_lock"] = threading.Lock()
        self.__retrain["tot_success"] = 0
        self.__promp_params = params["promp_params"]
        self.trial_ready_callbacks = []
        self.__last_process_trial = None
        self.stop_task()

    def configure_table_tennis(self, **params):
        ''' Sends the parameters required by the table tennis task via JSON
        '''
        methods = [{'file': params["ballmodel"], 'method': 'table_tennis/load_ball_model'}, \
          {'file': params["ballfilter"], 'method': 'table_tennis/load_ball_filter'}, \
          {'file': params["barrettkin"], 'method': 'table_tennis/load_barrett_kin'}, \
          {'file': params["promp"], 'method': 'table_tennis/load_prior_promp'}]
        ans = []
        for method in methods:
            f = open(method['file'], 'r')
            body = json.load(f)
            print "Configuring on ", method['method']
            ans.append( self.client.query(method['method'], body) )
        return ans

    def configure_task(self, conf):
        self.client.query("table_tennis/set_config", conf)

    def close(self):
        t = self.__retrain["thread"]
        self.stop_task()
        self.trial_clean.close()
        if t:
            print "Waiting for the ProMP thread to finish"
            t.join()

    def send_sim_ball_traj(self, kf, deltaT, **params):
        params.setdefault("trajlen", 180)
        params.setdefault("sensor_id", 1)
        trajlen = params['trajlen']
        ball_obs, hidden = kf.generate_sample(trajlen, actions=np.ones((trajlen,1)))
        sim_times = np.array(range(1,trajlen+1)) * deltaT
        data = []
        for i, obs in enumerate(ball_obs):
            data.append({"sensor_id": params["sensor_id"], "time": sim_times[i], "obs": obs.tolist()[0]})
        self.client.query('obs/add_sim_obs', data)

    def retrain_promp(self):
        #NOTE: Need mutex for gathering the data only. At the end pass learned ProMP by network to SL
        while not self.__retrain["stop"].isSet():
            with self.__retrain["data_lock"]:
                time, Q, Qdot = promp.joint_train_data( self.loader )
            pp = self.__promp_params #NOTE: Might be thread unsafe
            dim_basis_fun = promp.dim_comb_basis(**pp['basis'])
            inv_whis_mean = lambda v, Sigma: utils.make_block_diag(Sigma, pp['ndof']) + 1e-4*np.eye(dim_basis_fun*pp['ndof'])
            inv_wish_prior = {'v':dim_basis_fun*pp['ndof'], 'mean_cov_mle': inv_whis_mean}
            robot_promp = promp.FullProMP(basis=pp['basis'])
            quit_fn = lambda: self.__retrain["stop"].isSet() 
            print "Training ProMP with {0} instances".format(len(time))
            if pp["use_velocity"]:
                robot_promp.train(time, q=Q, qd=Qdot, early_quit=quit_fn, prior_Sigma_w=inv_wish_prior, **pp)
            else:
                robot_promp.train(time, q=Q, early_quit=quit_fn, prior_Sigma_w=inv_wish_prior, **pp)
            print "ProMP trained"
            stream = robot_promp.to_stream()
            self.client.query("table_tennis/load_prior_promp", stream)
            slpp.save_json(pp["file"], stream)
        self.__retrain["thread"] = None

    def process_trials(self):
        print "Gathering information and processing trials"
        data = self.recorder.to_stream()
        meta = self.trial_clean.process_trial(data)
        for method in self.trial_ready_callbacks:
            method(data, meta, self)
        if meta['segments'] and len(meta['segments'])>0:
            if len(meta['segments']) > 1:
                print "Warning: Receiving more that one segment for the same trial"
            num_success = 0
            for segm in meta['segments']:
                if segm['success']:
                    num_success += 1
                    print "Successful trial"
                else:
                    print "Unsuccessful trial"

            if num_success > 0:
                with self.__retrain["data_lock"]:
                    self.loader.data.append({"data": data, "meta": meta})
                self.__retrain["tot_success"] += num_success
                print "Total number of successful trials: ", self.__retrain["tot_success"]
                if self.__retrain["tot_success"] > self.__retrain["min_traj"] and not self.__retrain["thread"]:
                    print "About to launch a thread to train ProMPs"
                    t = threading.Thread(target = lambda : self.retrain_promp())
                    self.__retrain["thread"] = t
                    self.__retrain["stop"] = threading.Event()
                    t.start()
        else:
            print "Malformed trial"
        self.recorder.save()
        self.recorder.clear_internal_state()

    def update(self):
        self.recorder.update()
        if self.__state != "stopped":
            if time.time() > (self.__last_process_trial + self.__retrain["process_trial_every"]):
                self.__last_process_trial = time.time()
                self.process_trials()
            
    def get_state(self): return self.__state

def redraw(screen, task, **params):
    colors = {'white': (255,255,255), 'black': (0,0,0), 'red': (255,0,0), 
            'green': (0,255,0), 'blue': (0,0,255), 'yellow': (255,255,0)}
    screen.fill(colors['white'])

    state = task.get_state()
    state_color = {'stopped': 'green', 'kin_teach': 'yellow', 'table_tennis': 'red'}
    scolor = colors[state_color[state]]
    pygame.draw.circle(screen, scolor, (200,200), 100)

def main(args):
    task = None
    try:
        conf = json.load(open(args.conf, 'r'))
        if args.url or args.port:
            if not args.url:
                args.url = "tcp://{0}:{1}".format(args.host, args.port)
            conf['url'] = args.url
        task = SinglePrompTask(**conf)
        task.configure_table_tennis(ballmodel=args.ballmodel, ballfilter=args.ballfilter,
                promp=args.promp, barrettkin=args.barrettkin)
        task.configure_task(conf["task_params"])
        simkf, deltaT = ball_model.load_ball_model(args.simball)
        pygame.init()
        screen = pygame.display.set_mode((400,400))
        running = True
        state = "kin_teach"
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_k:
                        task.start_kin_teach()
                        print "Starting human demonstration mode"
                    elif event.key == pygame.K_t:
                        task.start_table_tennis()
                        print "Starting table tennis playing mode"
                    elif event.key == pygame.K_q:
                        running = False
                    elif event.key == pygame.K_s:
                        task.stop_task()
                        print "Stopping both table tennis and recording modes"
                    elif event.key == pygame.K_b:
                        task.send_sim_ball_traj(simkf, deltaT, **conf['sim_params'])
                        print "Sending some simulated ball observations"
            task.update()
            redraw(screen, task)
            pygame.display.flip()
            pygame.time.wait(20)
    finally:
        if task:
            task.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Record ball and joint information in JSON format')
    parser.add_argument('--host', help="Name of the host to connect to")
    parser.add_argument('--port', default=7647, help="Port of the host to connect to")
    parser.add_argument('--url', help="Overrides the host and port setting directly a ZMQ bind URL")
    parser.add_argument('--log', help="Directory where the hitting log information is stored")

    rel_path = lambda fname: os.path.join(os.path.dirname(__file__), fname)
    parser.add_argument('--promp', default = rel_path('../config/prior_promp.json'), \
            help="File name where the initial ProMP model is stored")
    parser.add_argument('--ballmodel', default = rel_path('../config/ball_model.json'), \
            help="File name where the ball model is stored")
    parser.add_argument('--ballfilter', default = rel_path('../config/ball_filter.json'), \
            help="File name where the ball filter configuration is stored")
    parser.add_argument('--barrettkin', default = rel_path('../config/barrett_kin.json'), \
            help="File name where the Barrett forward kinematics configuration is stored")
    parser.add_argument('--conf', default = rel_path('../config/single_promp_task.json'), \
            help="File where the general configuration of the task is stored")
    parser.add_argument('--simball', default = rel_path('../config/sim_ball_model.json'), \
            help = "File name where the simulation ball model is stored")

    args = parser.parse_args()
    main(args)
