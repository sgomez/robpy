"""Script to create a strike movement trajectory dataset from a consolidated raw file

This script receives as parameter a file with the consolidated recorded
data and optional parameters about decired processing steps and produce a single
robot trajectory dataset.
"""

import robpy.slpp as slpp
import robpy.full_promp as promp
import robpy.table_tennis.load_data as load_data
import numpy as np
import sys
import time
import argparse
import os
import json

def main(args):
    with open(args.in_file, 'r') as f:
        if args.in_file.endswith('json'):
            trial_data = json.load(f)
        else:
            trial_data = np.load(f)
        ds = load_data.PlayDatasetBuilder()
        ds.data = trial_data['raw']
    time, Q, Qdot = promp.joint_train_data(ds)
    if args.permute:
        p = np.random.permutation(len(time))
        time = [time[ix] for ix in p]
        Q = [Q[ix] for ix in p]
        Qdot = [Qdot[ix] for ix in p]
    print("{} robot trajectories extracted".format(len(time)))
    stream = {'time': time, 'Q': Q, 'Qdot': Qdot}
    with open(args.out_file, 'w') as f:
        if args.out_file.endswith('json'):
            json.dump(stream, f)
        elif args.out_file.endswith('npz'):
            np.savez(f, **stream)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('in_file', help="File where the consolidated information is stored")
    parser.add_argument('out_file', help="File where the ball processed information is stored")
    parser.add_argument('--permute', action="store_true", help="Randomly permute dataset")
    args = parser.parse_args()
    main(args)
