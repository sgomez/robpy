
import numpy as np
import tensorflow as tf
import unittest
import robpy.neural_process as rnp
import robpy.utils as utils
import robpy.full_promp as promp
import matplotlib.pyplot as plt

def generate_promp_train_data(N,T):
    full_basis = {'conf': [{"type": "poly", "nparams": 0, "conf": {"order": 2}}], 'params': []}
    p = promp.FullProMP(model={'mu_w': np.zeros(3),
        'Sigma_w': np.eye(3),
        'Sigma_y': 0.0001*np.eye(1)}, num_joints=1, basis=full_basis)
    times = [np.linspace(0,1,T) for i in range(N)]
    Phi = []
    X = p.sample(times, Phi=Phi, q=True)
    return times, Phi, X

class TestCNP(unittest.TestCase):

    def test_untrained_pred_shape(self):
        conf = {'in_dim': 1, 'out_dim': 1}
        encoder = lambda x, is_training: tf.layers.conv1d(x, filters=1, kernel_size=1)
        decoder = lambda x, is_training: tf.layers.conv1d(x, filters=1, kernel_size=1)
        cnp = rnp.CondNeuralProcess(conf, encoder, decoder)
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())
            cs = np.array([[[0,0],[1,1]]])
            inputs = np.array([[[0],[1],[2]]])
            pred = cnp.predict(sess,  cs, inputs)
            loss_zero = cnp.eval_loss(sess, cs, inputs, pred)
            #print(loss_zero)
            other_loss = cnp.eval_loss(sess, cs, inputs, inputs)
            exp_other_loss = np.mean((pred-inputs)**2)
            self.assertAlmostEqual(loss_zero, 0.0, places=2)
            self.assertAlmostEqual(other_loss, exp_other_loss, places=2)
            #print(other_loss, exp_other_loss)
            print(pred)
            obt_shape = np.array(pred).shape
            #print(obt_shape)
            exp_shape = [1,3,1]
            for d in range(3): self.assertEqual(obt_shape[d],exp_shape[d])

    def test_training_comp_promp(self):
        return True # It is not working anyways :(
        conf = {'in_dim': 1, 'out_dim': 1, 'lr': 1e-4}
        encoder = rnp.create_mlp_regressor([20,40,60,120,60],40,hidden_activation=tf.nn.relu, batch_norm=False)
        decoder = rnp.create_mlp_regressor([60,120,200,120,80,40,20],1,hidden_activation=tf.nn.relu, batch_norm=False)
        cnp = rnp.CondNeuralProcess(conf, encoder, decoder)
        N = 100
        Nv = 10
        T = 50
        times, Phi, X = generate_promp_train_data(N + Nv,T)
        #tv = np.array(times[N:]).reshape((Nv,T,1))
        #Xv = np.array(X[N:]).reshape((Nv,T,1))
        tv = np.array(times[0:Nv]).reshape((Nv,T,1))
        Xv = np.array(X[0:Nv]).reshape((Nv,T,1))
        times = np.array(times[0:N]).reshape((N,T,1))
        X = np.array(X[0:N]).reshape((N,T,1))

        tcs = tv[:,[0,5,15,48],:]
        xcs = Xv[:,[0,5,15,48],:]
        cs = np.concatenate([tcs,xcs], axis=2)
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())
            pred_v = cnp.predict(sess, cs, tv)
            self.assertAlmostEqual( cnp.eval_loss(sess, cs, tv, pred_v), 0.0 )
            val_loss_before_train = cnp.eval_loss(sess, cs, tv, Xv)
            #for i in range(Nv):
            #    plt.plot(tv[i,:,0], Xv[i,:,0], 'rx')
            #    plt.plot(tv[i,:,0], pred_v[i,:,0])
            #    plt.show()

            batch_sampler = rnp.RandomBatchSampler(batch_size=32)
            history = cnp.fit(sess, times, X, batch_sampler, 5000, lr=1e-2, decay=0.9, decay_epochs=20)
            loss = np.mean(np.reshape(history['loss'],(-1,10)),axis=1)
            val_loss_after_train = cnp.eval_loss(sess, cs, tv, Xv)
            print("Validation loss before: {} and after: {} training".format(val_loss_before_train, val_loss_after_train))
            #print(loss)
            plt.plot(loss)
            plt.show()
            sample_in, sample_out, sample_cs = batch_sampler(times, X, 0)
            sample_loss = cnp.eval_loss(sess, cond_set=sample_cs, inputs=sample_in, outputs=sample_out)
            print(sample_loss)
            sample_pred = cnp.predict(sess, sample_cs, sample_in)
            for i in range(Nv):
                plt.plot(sample_in[i,:,0], sample_out[i,:,0], 'rx')
                plt.plot(sample_in[i,:,0], sample_pred[i,:,0])
                plt.show()

            pred_v = cnp.predict(sess, cs, tv)
            for i in range(Nv):
                plt.plot(tv[i,:,0], Xv[i,:,0], 'rx')
                plt.plot(tv[i,:,0], pred_v[i,:,0])
                plt.show()
            self.assertLess(val_loss_after_train, val_loss_before_train)




if __name__ == '__main__':
    unittest.main()
