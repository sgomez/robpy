
import numpy as np
import unittest
import robpy.kalman as kalman
import robpy.table_tennis.ball_model as ball_model

class TestKalman(unittest.TestCase):

    def setUp(self):
        np.random.seed(0)
        kf_params = {'deltaT': 0.016, 'bounceZ': -0.99,
                'airDrag': [0.6, 0.6, 0.1], 'bounceFac': [0.9, 0.9, 0.8]}
        A = ball_model.Ball_A_Mat(kf_params['airDrag'], kf_params['bounceFac'], 
                kf_params['deltaT'], kf_params['bounceZ'])
        B = ball_model.Ball_B_Mat(kf_params['deltaT'], kf_params['bounceZ'], 
                kf_params['bounceFac'][2])
        C = kalman.ConstMat(np.array([[1,0,0,0,0,0],[0,0,1,0,0,0],[0,0,0,0,1,0]]))
        self.mu0 = np.array([0.6, -1.8, -3.8, 4.3, -0.6, 1.5])
        self.P0 = np.eye(6)*1e-3
        self.Sigma = 1e-5*np.eye(3)
        self.Gamma = 1e-6*np.eye(6)
        kf = kalman.LinearKF(A, C, B=B)
        kf.init_params(init_state_mean=self.mu0,init_state_cov=self.P0,
                init_Sigma=self.Sigma,init_Gamma=self.Gamma)
        self.kf = kf
        self.kf_params = kf_params
        self.X = []
        self.U = []
        self.Z = []
        self.isBounce = []
        self.times = []
        for i in xrange(50):
            Tn = 100
            Un = np.ones((Tn,1))
            T = kf_params['deltaT']*np.array(range(Tn))
            x,z = kf.generate_sample(Tn, actions=Un)
            isBounce = []
            for t in xrange(Tn):
                state = {'t': t, 'prev_z': z[t], 'action': Un[t,:], 'A': self.kf.A, 'B': self.kf.B, 'C': self.kf.C, 'D': self.kf.D}
                isBounce.append( self.kf.A.isBounce(state) )
            self.X.append(x)
            self.U.append(Un)
            self.Z.append(z)
            self.times.append(T)
            self.isBounce.append(isBounce)

    def test_A_lowerbound(self):
        A_params = [ [0.6,0.6,0.1,0.9,0.9,0.8],
                [0.3,0.3,0.3,0.6,0.6,0.6],
                [0.5,0.25,0.15,0.71,0.71,0.18],
                [0.1,0.1,0.1,0.85,0.85,0.85]]
        A_lb = []
        for a_th in A_params:
            self.kf.A.params = a_th
            means, covs, Js, As, Bs, Cs, Ds, fwd_means = self.kf._E_step(self.X, self.U, Time=self.times)
            A_lb.append( self.kf._EM_lowerbound(means, covs, Js, As, Bs, Cs, Ds, self.X, self.U) )
        print A_lb
        for i in xrange(1,len(A_lb)):
            self.assertGreater(A_lb[0], A_lb[i])


    def test_EM_no_A(self):
        init_vals = {'init_state_mean': np.zeros(6), 'init_state_cov': np.eye(6)*10,
                'init_Sigma': np.eye(3)*1e-4, 'init_Gamma': np.eye(6)*1e-2,
                'prior_Sigma': kalman.get_cov_prior([0.01,0.01,0.01], 20),
                'prior_Gamma': kalman.get_cov_prior([1e-3 for i in xrange(6)], 20)}
        A = ball_model.Ball_A_Mat([0.6,0.6,0.1], [0.9,0.9,0.8], 
                self.kf_params['deltaT'], self.kf_params['bounceZ'])
        A.optional['isBounce'] = self.isBounce
        kf = kalman.LinearKF(A, self.kf.C, B=self.kf.B)
        kf.EM_train(self.X, self.U, Time=self.times, max_iter=10, optimize_A=False,
                print_lowerbound=True, debug_LB=True, check_grad=False, **init_vals)
        print "A_params = ", kf.A.params
        #self.assertTrue( np.allclose(kf.A.params, self.kf.A.params, rtol=0.1) )
        print "mu0=", kf.mu0
        self.assertTrue( np.allclose(self.mu0, kf.mu0, rtol=0.1) )
        print "P0 = ", kf.P0
        self.assertTrue( np.allclose(self.P0, kf.P0, atol=1e-1) )
        print "Sigma = ", kf.Sigma
        self.assertTrue( np.allclose(self.Sigma, kf.Sigma, atol=1e-2) )
        print "Gamma = ", kf.Gamma
        self.assertTrue( np.allclose(self.Gamma, kf.Gamma, atol=1e-2) )

if __name__ == '__main__':
    unittest.main()
                
