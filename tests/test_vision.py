
import robpy.vision as vision
import numpy as np
import unittest
import os
import json

class TestCameraProj(unittest.TestCase):

    def setUp(self):
        np.random.seed(0)
        #Load Projection Matrices
        proj_mat_file = os.path.join(os.path.dirname(__file__), 'data/proj_matrices.txt')
        C = np.loadtxt(proj_mat_file)
        self.proj_mats = []
        for i in xrange(4):
            x = C[3*i:3*i+3,:]
            x = (x / x[2,3]) #normalize to have one as last parameter
            self.proj_mats.append(x)
        #Load test calibration points or create them
        calib_test_fn = os.path.join(os.path.dirname(__file__), 'data/calib_test.json')
        if os.path.isfile(calib_test_fn):
            calib_f = open(calib_test_fn,'r')
            tmp = json.load(calib_f)
            self.pts2d = map(lambda x: np.array(x), tmp['pts2d'])
            self.pts3d = np.array(tmp['pts3d'])
            self.pts2d_noisy = map(lambda x: np.array(x), tmp['pts2d_noisy'])
        else:
            calib_f = open(calib_test_fn,'w')
            N = 500
            self.pts3d = np.random.multivariate_normal([0.0,-2.13,-0.9],np.diag([0.3,1,0.2]), N)
            self.pts2d = [vision.project_points(self.pts3d, P) for P in self.proj_mats]
            pix_std = 2 #noise std in pixels
            self.pts2d_noisy = []
            for i in xrange(len(self.proj_mats)):
                self.pts2d_noisy.append( self.pts2d[i] + np.random.multivariate_normal([0,0],pix_std**2*np.eye(2), N) )
            stream = {'pts3d': self.pts3d.tolist(), 'pts2d': np.array(self.pts2d).tolist(),
                'pts2d_noisy': np.array(self.pts2d_noisy).tolist()}
            json.dump(stream, calib_f)

    def test_simple_calibration(self):
        np.random.seed(0)
        N = 50
        for i in xrange(len(self.proj_mats)):
            C = self.proj_mats[i]
            pts3d = self.pts3d[0:N]
            pts2d = self.pts2d[i][0:N]
            pts2d_noisy = self.pts2d_noisy[i][0:N]
            C_hat = vision.lin_calib_cam(pts2d, pts3d)
            C_hat_noisy = vision.lin_calib_cam(pts2d, pts3d)
            self.assertTrue( np.allclose(C, C_hat, rtol=0.01) )
            self.assertTrue( np.allclose(C, C_hat_noisy, rtol=0.1) )

    def test_stereo_vision(self):
        N = 50
        for i in xrange(N):
            pt3d = self.pts3d[i]
            pts2d = []
            for j in xrange(len(self.proj_mats)): pts2d.append(self.pts2d_noisy[j][i])
            st3d = vision.stereo_vision(self.proj_mats, pts2d)
            self.assertTrue( np.allclose(st3d, pt3d, atol=0.05) ) #5cm error is acceptable

    def test_multi_cam_calib(self):
        np.random.seed(0)
        N = 70
        n_cams = len(self.proj_mats)
        p_obs_2d = 0.3 #On average every camera observe 30% of the 3D points
        p_obs_3d = 0.3 #And only around 30% of the obs have corresponding 3D position
        for tc in xrange(10):
            is_obs_3d = np.random.binomial(1, p_obs_3d, size=N)
            pts3d = map(lambda ix: self.pts3d[ix].tolist() if is_obs_3d[ix]==1 else None, xrange(N))
            pts2d = []
            for i in xrange(n_cams):
                is_obs_2d = np.random.binomial(1, p_obs_2d, size=N)
                pts2d.append(map(lambda ix: self.pts2d[i][ix].tolist() if is_obs_2d[ix]==1 else None, xrange(N)))
            find_cal = vision.lin_multicam_calib(pts2d, pts3d)
            for i in xrange(n_cams):
                C = find_cal[i]
                P = self.proj_mats[i]
                if C is not None:
                    self.assertTrue( np.allclose(C, P, rtol=0.01) )

if __name__ == '__main__':
    unittest.main()
