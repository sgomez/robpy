
import robpy.time_series
import numpy as np
import unittest
import os
import json

class TestTimeSeries(unittest.TestCase):

    def test_simple_zero_cross_vel(self):
        qdot_t = np.array([[0,1,2,3,4,3,1,2,1,0,-1],
            [5,4,3,2,1,0,1,2,3,4,0],
            [2,1,0,2,3,1,0,1,2,1,0],
            [-4,-3,-4,-5,-4,-3,-2,-1,0,4,3],
            [3,0,3,4,5,4,3,2,1,0,3]])
        all_ans = [(0,9),(0,5),(2,6),(0,8),(1,9)]
        qdot = qdot_t.T
        for i, ans in enumerate(all_ans):
            a,b = robpy.time_series.zero_cross_vel(qdot, split_joint=i)
            self.assertEqual(a, ans[0])
            self.assertEqual(b, ans[1])

