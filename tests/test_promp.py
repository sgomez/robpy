
import numpy as np
import unittest
import robpy.full_promp as mp
import robpy.utils as utils
import matplotlib.pyplot as plt

def plot_time_series(times, q):
    N = len(times)
    for n in xrange(N):
        plt.plot(times[n], q[n][:,0], 'r')
    plt.show()

class TestProMP(unittest.TestCase):

    def setUp(self):
        np.random.seed(0)
        self.ndof = 2
        self.full_basis = {
            'conf': [
                    {"type": "sqexp", "nparams": 5, "conf": {"dim": 4}},
                    {"type": "poly", "nparams": 0, "conf": {"order": 0}}
                ],
            'params': np.array([np.log(0.25),0.0,1.0/3.0,2.0/3.0,1.0])
            }
        self.org_promp = mp.FullProMP(basis=self.full_basis, num_joints=self.ndof)
        self.org_promp.mu_w = np.array([ 0.116 ,  0.0942, -0.0817, -0.1188,  1.2   ,  0.04  , -0.0223,  0.0783,  0.1794, -0.0346])
        self.org_promp.Sigma_w = np.array([
            [  9.0398e-03,   4.2124e-03,   4.2814e-03,   9.2394e-03,  -1.4033e-02,  -9.5272e-04,  -7.4309e-04,   1.7346e-04,  -6.2996e-04,   1.5053e-03],
            [  4.2124e-03,   2.3477e-03,   1.9011e-03,   4.3953e-03,  -8.4537e-03,  -4.6006e-04,  -3.1803e-04,   1.9104e-04,  -1.2093e-04,   7.9450e-04],
            [  4.2814e-03,   1.9011e-03,   2.5860e-03,   5.0943e-03,  -9.6803e-03,  -4.9598e-04,  -4.0214e-04,   2.4711e-08,  -4.4742e-04,   5.5029e-04],
            [  9.2394e-03,   4.3953e-03,   5.0943e-03,   1.1120e-02,  -2.1934e-02,  -1.1241e-03,  -8.2700e-04,   1.6739e-04,  -7.4704e-04,   1.3910e-03],
            [ -1.4033e-02,  -8.4537e-03,  -9.6803e-03,  -2.1934e-02,   9.9367e-02,   2.4965e-03,   1.3703e-03,  -1.3987e-03,  -1.1278e-03,  -1.8915e-03],
            [ -9.5272e-04,  -4.6006e-04,  -4.9598e-04,  -1.1241e-03,   2.4965e-03,   4.7076e-03,   2.3391e-03,  -7.4124e-04,   5.6327e-04,  -4.6041e-03],
            [ -7.4309e-04,  -3.1803e-04,  -4.0214e-04,  -8.2700e-04,   1.3703e-03,   2.3391e-03,   1.9175e-03,  -4.9661e-05,   1.5557e-03,  -2.4757e-03],
            [  1.7346e-04,   1.9104e-04,   2.4711e-08,   1.6739e-04,  -1.3987e-03,  -7.4124e-04,  -4.9661e-05,   1.9552e-03,   2.4610e-03,   2.0543e-03],
            [ -6.2996e-04,  -1.2093e-04,  -4.4742e-04,  -7.4704e-04,  -1.1278e-03,   5.6327e-04,   1.5557e-03,   2.4610e-03,   7.5791e-03,  -7.5670e-04],
            [  1.5053e-03,   7.9450e-04,   5.5029e-04,   1.3910e-03,  -1.8915e-03,  -4.6041e-03,  -2.4757e-03,   2.0543e-03,  -7.5670e-04,   9.6304e-03]
            ])
        N = 100
        deltaT = 0.002
        self.times = map(lambda Tn: np.array(range(Tn))*deltaT, np.random.randint(100,400,size=N))
        self.weights = np.random.multivariate_normal(self.org_promp.mu_w, self.org_promp.Sigma_w,N)
        self.noise = np.array([0.01,0.005])
        self.q = self.org_promp.sample(self.times, weights=self.weights, noise=self.noise, q=True)
        self.dim_basis_fun = mp.dim_comb_basis(**self.full_basis)
        self.inv_whis_mean = lambda v, Sigma: utils.make_block_diag(Sigma, self.ndof) + 1e-4*np.eye(self.dim_basis_fun*self.ndof)

    def test_em_training(self):
        promp = mp.FullProMP(basis=self.full_basis, num_joints=self.ndof)
        promp.train(self.times, q=self.q, max_iter=15)
        cond_ml = np.linalg.cond(promp.Sigma_w)
        rank_ml = np.linalg.matrix_rank(promp.Sigma_w, tol=1e-5)
        print "ML estimate of Sigma_w with all data: cond={0}, rank={1}".format(cond_ml, rank_ml)
        #print promp.mu_w
        #print promp.Sigma_w
        #print promp.Sigma_y
        #print promp.Sigma_w - self.org_promp.Sigma_w
        self.assertTrue(np.allclose(promp.mu_w, self.org_promp.mu_w, rtol=0.15, atol=1e-2))
        self.assertTrue(np.allclose(np.sqrt(np.diag(promp.Sigma_w)), np.sqrt(np.diag(self.org_promp.Sigma_w)), rtol=0.15))
        self.assertTrue(np.allclose(promp.Sigma_w, self.org_promp.Sigma_w, rtol=0.1, atol=1e-2))
        self.assertTrue(np.allclose(promp.Sigma_y, np.diag(self.noise**2), rtol=0.2))

    def test_em_with_priors(self):
        promp = mp.FullProMP(basis=self.full_basis, num_joints=self.ndof)
        promp.train(self.times, q=self.q, max_iter=15, prior_Sigma_w={'v':self.dim_basis_fun*self.ndof, 'mean_cov_mle': self.inv_whis_mean})
        cond_ml = np.linalg.cond(promp.Sigma_w)
        rank_ml = np.linalg.matrix_rank(promp.Sigma_w, tol=1e-5)
        print "MAP estimate of Sigma_w with all data: cond={0}, rank={1}".format(cond_ml, rank_ml)
        #print promp.mu_w
        #print promp.Sigma_w
        #print promp.Sigma_y
        #print promp.Sigma_w - self.org_promp.Sigma_w
        self.assertTrue(np.allclose(promp.mu_w, self.org_promp.mu_w, rtol=0.15, atol=1e-2))
        self.assertTrue(np.allclose(np.sqrt(np.diag(promp.Sigma_w)), np.sqrt(np.diag(self.org_promp.Sigma_w)), rtol=0.15))
        self.assertTrue(np.allclose(promp.Sigma_w, self.org_promp.Sigma_w, rtol=0.1, atol=1e-2))
        self.assertTrue(np.allclose(promp.Sigma_y, np.diag(self.noise**2), rtol=0.2))

    def test_em_little_data(self):
        N = 6
        promp = mp.FullProMP(basis=self.full_basis, num_joints=self.ndof)
        promp.train(self.times[0:N], q=self.q[0:N], max_iter=15)
        cond_ml = np.linalg.cond(promp.Sigma_w)
        rank_ml = np.linalg.matrix_rank(promp.Sigma_w, tol=1e-5)
        print "ML estimate of Sigma_w: cond={0}, rank={1}".format(cond_ml, rank_ml)

        promp = mp.FullProMP(basis=self.full_basis, num_joints=self.ndof)
        promp.train(self.times[0:N], q=self.q[0:N], max_iter=15, prior_Sigma_w={'v':self.dim_basis_fun*self.ndof, 'mean_cov_mle': self.inv_whis_mean})
        cond_map = np.linalg.cond(promp.Sigma_w)
        rank_map = np.linalg.matrix_rank(promp.Sigma_w, tol=1e-5)
        print "MAP estimate of Sigma_w: cond={0}, rank={1}".format(cond_map, rank_map)
       
        self.assertGreater(cond_ml, cond_map) # Actually cond_ml should be much bigger than cond_map
        self.assertLessEqual(rank_ml, N)
        self.assertGreater(rank_map, N)
        self.assertEqual(rank_map, self.dim_basis_fun*self.ndof)
        

if __name__ == '__main__':
    unittest.main()
