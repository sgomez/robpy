from setuptools import setup, find_packages

setup(name='robpy',
      version='0.0.1',
      description='A collection of Python libraries for robotics',
      url='https://gitlab.tuebingen.mpg.de/sgomez/robpy',
      author='Sebastian Gomez-Gonzalez',
      author_email='sgomez@tue.mpg.de',
      packages=find_packages(),
      setup_requires=['pytest-runner'],
      tests_require=['pytest'],
      install_requires = [
        'numpy==1.13.1',
        'scipy==0.19.0',
        'matplotlib==2.0.2',
        'pyzmq==16.0.2',
        'autograd',
        'pygame==1.9.3',
        'pyparsing==2.4.2',
        'python-dateutil==2.6.1',
        ],
      license='Do not know')
