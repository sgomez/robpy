Robotics Python Librares
========================

This repository contains a collection of Python libraries for robotics. This library is also used to interact
with the C++ robotics API on [robcpp](https://gitlab.tuebingen.mpg.de/sgomez/robcpp). This means
that in to have all the functionality of this package you may need to install the C++ API first and
compiled with the Python extensions. Fortunately, most of the functionality is written
in Python.

## Examples

This project contains a big API for many different purposes. Examples can be found in the
example folder distributed with the code. 

## Motivation

This project was created with the aim of making the coding efforts for robotic projects reusable.

## Installation

The first step to install this library is to install the pre-requisits. Start by installing
[robcpp](https://gitlab.tuebingen.mpg.de/sgomez/robcpp) with Python extensions. Then install
[OpenCV](http://opencv.org/) with Python extensions and Sphinx if you want to compile the
documentation. In Ubuntu 16.04 this can be done with

```
sudo apt-get install python-opencv python-sphinx libsdl1.2-dev libfreetype6-dev libsdl-image1.2-dev libjpeg-dev libpng-dev libportmidi-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev gfortran
```

For the functionality that requires the use of Protocol Buffers, you need to install
the Google Protocol Buffer library from [protobuff](https://github.com/google/protobuf/)
and compile the required Python bindings:

```
# Run the following command on the robpy/protos subfolder
protoc --python_out=./ *.proto
```

You might get an error if you version is too old. We recommend to use the newest version,
but it needs to be at least a version 3.x.

Subsequently, you can install this package as a regular Python package using

```
python setup.py install
python setup.py test           #If you want to run the tests
```

## API Reference

To compile the API documentation you require Sphinx. In Linux, you can compile
the documentation like this:

```
cd doc
make html
```

## License

I don't know yet what license can be used for this code. Once I know I will update this file.
